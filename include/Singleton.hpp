#if !defined(SINGLETON_HPP)
#define SINGLETON_HPP

#include "Prerequisites.hpp"

namespace bge
{

template <typename T>
class Singleton
{
public:
	static T& getInst()
	{
		// TODO: handle error when p_staticInstance=0
		return (*p_staticInstance);
	}
	static T* getInstPtr()
	{
		return p_staticInstance;
	}
public:
	Singleton()
	{
		// TODO: handle error when p_staticInstance=0
		p_staticInstance = static_cast<T*>(this);
	}
	~Singleton()
	{
		// TODO: handle error when p_staticInstance=0
		p_staticInstance = 0;
	}
protected:
	static T* p_staticInstance;
private:
	Singleton(const Singleton<T> &);
	Singleton& operator=(const Singleton<T> &);
};

}

#endif // SINGLETON_HPP

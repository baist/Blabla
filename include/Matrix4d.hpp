#if !defined (MATRIX4D_HPP)
#define MATRIX4D_HPP

#include "Prerequisites.hpp"
#include "Vector4d.hpp"

namespace bge
{

class Matrix4d
{
public:
	static const Matrix4d ZERO;
	static const Matrix4d ZEROAFFINE;
	static const Matrix4d IDENTITY;
	/** Useful little matrix which takes 2D clipspace {-1, 1} to {0,1}
			and inverts the Y. */
	static const Matrix4d CLIPSPACE2DTOIMAGESPACE;
public:
	static Matrix4d getTrans(const Vector3d &vec);
	static Matrix4d getTrans(real x, real y, real z);
	static Matrix4d getScale(const Vector3d &vec);
	static Matrix4d getScale(real x, real y, real z);
public:
	friend Vector4d operator * (const Vector4d &v, const Matrix4d &mat)
	{
		return Vector4d(
				v.x * mat[0][0] + v.y * mat[1][0] + v.z * mat[2][0] + v.w * mat[3][0],
				v.x * mat[0][1] + v.y * mat[1][1] + v.z * mat[2][1] + v.w * mat[3][1],
				v.x * mat[0][2] + v.y * mat[1][2] + v.z * mat[2][2] + v.w * mat[3][2],
				v.x * mat[0][3] + v.y * mat[1][3] + v.z * mat[2][3] + v.w * mat[3][3]
			);
	}
public:
	Matrix4d();
	Matrix4d(
			real m00, real m01, real m02, real m03,
			real m10, real m11, real m12, real m13,
			real m20, real m21, real m22, real m23,
			real m30, real m31, real m32, real m33 );
	Matrix4d(const Matrix3d &matr);
	Matrix4d(const Quaternion &rot);
	real *operator [] (size_t row);
	const real *operator [] (size_t row) const;
	Matrix4d concatenate(const Matrix4d &matr) const;
	Matrix4d operator * (const Matrix4d &matr) const;
	Vector3d operator * (const Vector3d &vec) const;
	Vector4d operator * (const Vector4d &vec) const;
	Matrix4d operator + (const Matrix4d &matr) const;
	Matrix4d operator - (const Matrix4d &matr) const;
	Matrix4d operator*(real scalar) const;
	bool operator == (const Matrix4d &matr) const;
	bool operator != (const Matrix4d &matr) const;
	void operator = (const Matrix3d &matr);
	Matrix4d transpose(void) const;
	void setTrans(const Vector3d &vec);
	Vector3d getTrans() const;
	void makeTrans(const Vector3d &vec);
	void makeTrans(real x, real y, real z);
	void setScale(const Vector3d &vec);
	void extract3x3Matrix(Matrix3d &m3x3) const;
	bool hasScale() const;
	Quaternion extractQuaternion() const;
protected:
	/// The matrix entries, indexed by [row][col].
	union {
		real m[4][4];
		real _m[16];
	};
};


}

#endif // MATRIX4D_HPP

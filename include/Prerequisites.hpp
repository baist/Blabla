#if !defined(PREREQUISITES_HPP)
#define PREREQUISITES_HPP

#include <cstddef>
#include <limits>
#include <vector>
#include <list>

namespace bge
{

class Buffer;
class ColourValue;
class DepthBuffer;
class HDepthBuffer;
class IndexBuffer;
class Line2d;
class Math;
class Matrix3d;
class Matrix4d;
class Plane;
class Point;
class Quaternion;
class Ray;
class Vector2d;
class Vector3d;
class Vector4d;
class VertexBuffer;

typedef unsigned char uint8;
typedef unsigned int uint32;
typedef float real;

template <typename T>
struct vector {
	typedef typename std::vector<T> type;
	typedef typename std::vector<T>::iterator iterator;
	typedef typename std::vector<T>::const_iterator const_iterator;
};

template <typename T>
struct list {
	typedef typename std::list<T> type;
	typedef typename std::list<T>::iterator iterator;
	typedef typename std::list<T>::const_iterator const_iterator;
};

}

#endif // PREREQUISITES_HPP

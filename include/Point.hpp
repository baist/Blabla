#if !defined(POINT_HPP)
#define POINT_HPP

namespace bge
{

class Point
{
public:
	int x;
	int y;
public:
	Point():
		x(0), y(0) {
	}
	Point(int _x, int _y):
		x(_x), y(_y) {
	}
};

}

#endif // POINT_HPP

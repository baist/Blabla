#if !defined (MATRIX3D_HPP)
#define MATRIX3D_HPP

#include "Prerequisites.hpp"
#include "Vector3d.hpp"

namespace bge
{

class Matrix3d
{
public:
	static const real EPSILON;
	static const Matrix3d ZERO;
	static const Matrix3d IDENTITY;
public:
	friend Vector3d operator*(const Vector3d &vec, const Matrix3d &matr)
	{
		Vector3d resVec;
		for (size_t row = 0; row < 3; row++) {
			resVec[row] =
					vec[0] * matr.m[0][row] +
					vec[1] * matr.m[1][row] +
					vec[2] * matr.m[2][row];
		}
		return resVec;
	}
	friend Matrix3d operator* (real scalar, const Matrix3d &matr)
	{
		Matrix3d resMatr;
		for (size_t row = 0; row < 3; row++) {
			for (size_t col = 0; col < 3; col++) {
				resMatr[row][col] = scalar * matr.m[row][col];
			}
		}
		return resMatr;
	}
public:
	Matrix3d ();
	explicit Matrix3d (const real arr[3][3]);
	Matrix3d (const Matrix3d &matr);
	Matrix3d(const Quaternion &rot);
	Matrix3d (real val00, real val01, real val02,
						real val10, real val11, real val12,
						real val20, real val21, real val22);
	// member access, allows use of construct mat[r][c]
	real *operator[] (size_t row) const;
	Matrix3d &operator= (const Matrix3d &matr);
	bool operator== (const Matrix3d &matr) const;
	bool operator!= (const Matrix3d &matr) const;
	// arithmetic operations
	Matrix3d operator+ (const Matrix3d &matr) const;
	Matrix3d operator- (const Matrix3d &matr) const;
	Matrix3d operator* (const Matrix3d &matr) const;
	Matrix3d operator- () const;
	Vector3d operator* (const Vector3d &vec) const;
	Matrix3d operator*(real scalar) const;
	bool hasScale() const;
	// utilities
	Matrix3d transpose () const;
	bool inverse (Matrix3d &invMatr, real tolerance = 1e-06) const;
	Matrix3d inverse (real tolerance = 1e-06) const;
	real determinant () const;
	// matrix must be orthonormal
	void toAngleAxis (Vector3d &axis, real &angle) const;
	void fromAngleAxis (const Vector3d &axis, const real &radians);
	// The matrix must be orthonormal.  The decomposition is yaw*pitch*roll
	// where yaw is rotation about the Up vector, pitch is rotation about the
	// Right axis, and roll is rotation about the Direction axis.
	bool toEulerAnglesXYZ (real &yAngle, real &pAngle,
												 real &rAngle) const;
	bool toEulerAnglesXZY (real &yAngle, real &pAngle,
												 real &rAngle) const;
	bool toEulerAnglesYXZ (real &yAngle, real &pAngle,
												 real &rAngle) const;
	bool toEulerAnglesYZX (real &yAngle, real &pAngle,
												 real &rAngle) const;
	bool toEulerAnglesZXY (real &yAngle, real &pAngle,
												 real &rAngle) const;
	bool foEulerAnglesZYX (real &yAngle, real &pAngle,
												 real &rAngle) const;
	void fromEulerAnglesXYZ (const real &yAngle, const real &pAngle,
													 const real &rAngle);
	void fromEulerAnglesXZY (const real &yAngle, const real &pAngle,
													 const real &rAngle);
	void fromEulerAnglesYXZ (const real &yAngle, const real &pAngle,
													 const real &rAngle);
	void fromEulerAnglesYZX (const real &yAngle, const real &pAngle,
													 const real &rAngle);
	void fromEulerAnglesZXY (const real &yAngle, const real &pAngle,
													 const real &rAngle);
	void fromEulerAnglesZYX (const real &yAngle, const real &pAngle,
													 const real &rAngle);
protected:
	// for faster access
	friend class Matrix4d;
protected:
	real m[3][3];
};

}

#endif // MATRIX3D_HPP

#if !defined(RESOURCE_MANAGER_HPP)
#define RESOURCE_MANAGER_HPP

#include <map>
#include "Prerequisites.hpp"
#include "Resource.hpp"
#include "String.hpp"

namespace bge
{

class ResourceManager
{
public:
	ResourceManager();
	virtual ~ResourceManager();
	const String generateNewName(const String &suggestedName);
	virtual void create();
	virtual void deleteAll();
protected:
	bool isExist(const String &name);
	Resource* createRes(const String &name);
	void addRes(Resource *p_res);
	void removeRes(Resource *p_res);
	void removeRes(const String &name);
private:
	typedef std::map<String, Resource*> Resources;
private:
	Resources m_resources;
};

}

#endif // RESOURCE_MANAGER_HPP

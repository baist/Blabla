#if !defined(RESOURCE_HPP)
#define RESOURCE_HPP

#include "Prerequisites.hpp"
#include "String.hpp"

namespace bge
{

class Resource
{
public:
	Resource();
	Resource(const String &name);
	virtual ~Resource();
	const String &getName() const;
protected:
	String m_name;
};

}

#endif // RESOURCE_HPP

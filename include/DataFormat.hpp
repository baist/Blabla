#if !defined(DATA_FORMAT_HPP)
#define DATA_FORMAT_HPP

namespace bge {

enum DataFormat {
	DF_UNKNOWN = 0,
	DF_I8,
	DF_I8I8,
	DF_I8I8I8,
	DF_I8I8I8I8,
	DF_UI16,
	DF_R32,
	DF_R32R32,
	DF_R32R32R32,
	DF_R64
};

}

#endif // DATAFORMAT_HPP

#if !defined (VECTOR4D_HPP)
#define VECTOR4D_HPP

#include "Prerequisites.hpp"
#include "Vector3d.hpp"

namespace bge
{

class Vector4d
{
public:
	real x, y, z, w;
public:
	friend Vector4d operator * (real scalar, const Vector4d &vec) {
		return Vector4d(
							 scalar * vec.x,
							 scalar * vec.y,
							 scalar * vec.z,
							 scalar * vec.w);
	}

	friend Vector4d operator / (real scalar, const Vector4d &vec) {
		return Vector4d(
							 scalar / vec.x,
							 scalar / vec.y,
							 scalar / vec.z,
							 scalar / vec.w);
	}

	friend Vector4d operator + (const Vector4d &vec, real val) {
		return Vector4d(
							 vec.x + val,
							 vec.y + val,
							 vec.z + val,
							 vec.w + val);
	}

	friend Vector4d operator + (real val, const Vector4d &vec) {
		return Vector4d(
							 val + vec.x,
							 val + vec.y,
							 val + vec.z,
							 val + vec.w);
	}

	friend Vector4d operator - (const Vector4d &vec, real val) {
		return Vector4d(
							 vec.x - val,
							 vec.y - val,
							 vec.z - val,
							 vec.w - val);
	}

	friend Vector4d operator - (real val, const Vector4d &vec) {
		return Vector4d(
							 val - vec.x,
							 val - vec.y,
							 val - vec.z,
							 val - vec.w);
	}
public:
	Vector4d();
	Vector4d(real valX, real valY, real valZ, real valW);
	explicit Vector4d(const real val[4]);
	explicit Vector4d(real *const val);
	explicit Vector4d(real scalar);
	explicit Vector4d(const Vector3d &vec, real valW = 1.0f);
	real operator [] (const size_t i) const;
	real &operator [] (const size_t i);
	Vector4d &operator = (const Vector4d &vec);
	Vector4d &operator = (real scalar);
	bool operator == (const Vector4d &vec) const;
	bool operator != (const Vector4d &vec) const;
	Vector4d &operator = (const Vector3d &vec);
	// arithmetic operations
	Vector4d operator + (const Vector4d &vec) const;
	Vector4d operator - (const Vector4d &vec) const;
	Vector4d operator * (real scalar) const;
	Vector4d operator * (const Vector4d &vec) const;
	Vector4d operator / (real scalar) const;
	Vector4d operator / (const Vector4d &vec) const;
	const Vector4d &operator + () const;
	Vector4d operator - () const;
	// arithmetic updates
	Vector4d &operator += (const Vector4d &vec);
	Vector4d &operator -= (const Vector4d &vec);
	Vector4d &operator *= (real scalar);
	Vector4d &operator += (real scalar);
	Vector4d &operator -= (real scalar);
	Vector4d &operator *= (const Vector4d &vec);
	Vector4d &operator /= (real scalar);
	Vector4d &operator /= (const Vector4d &vec);
	real dotProduct(const Vector4d &vec) const;
};

}

#endif // VECTOR4D_HPP


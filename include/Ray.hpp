#if !defined (RAY_HPP)
#define RAY_HPP

#include "Prerequisites.hpp"
#include "Vector3d.hpp"

namespace bge
{

class Ray
{
public:
	Ray();
	Ray(const Vector3d &origin, const Vector3d &direction);
	void setOrigin(const Vector3d &origin);
	const Vector3d &getOrigin() const;
	void setDirection(const Vector3d &dir);
	const Vector3d &getDirection() const;
	Vector3d getPoint(real t) const;
	Vector3d operator*(real t) const;
	std::pair<bool, real> intersects(const Plane &p) const;
protected:
	Vector3d m_origin;
	Vector3d m_direction;
};

}

#endif // RAY_HPP

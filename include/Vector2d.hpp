#if !defined(VECTOR2D_HPP)
#define VECTOR2D_HPP

#include "Prerequisites.hpp"
#include "Math.hpp"

namespace bge
{

class Vector2d
{
public:
	real x, y;
public:
	friend Vector2d operator * (real scalar, const Vector2d & vec)
	{
		return Vector2d(scalar * vec.x, scalar * vec.y);
	}
	friend Vector2d operator / (real scalar, const Vector2d & vec)
	{
		return Vector2d(scalar / vec.x, scalar / vec.y);
	}
	friend Vector2d operator + (const Vector2d & vec, real val)
	{
		return Vector2d(vec.x + val, vec.y + val);
	}
	friend Vector2d operator + (real a, const Vector2d & vec)
	{
		return Vector2d(a + vec.x, a + vec.y);
	}
	friend Vector2d operator - (const Vector2d & vec, real val)
	{
		return Vector2d(vec.x - val, vec.y - val);
	}
	friend Vector2d operator - (real a, const Vector2d & vec)
	{
		return Vector2d(a - vec.x, a - vec.y);
	}
public:
	Vector2d();
	Vector2d(real valX, real valY);
	explicit Vector2d(real scalar);
	explicit Vector2d(const real val[2]);
	explicit Vector2d(real * const val);
	real operator [] (size_t i ) const;
	real & operator [] (size_t i );
	Vector2d & operator = (const Vector2d & vec);
	Vector2d & operator = (real scalar);
	bool operator == (const Vector2d & vec) const;
	bool operator != (const Vector2d & vec) const;
	bool operator < (const Vector2d & vec) const ;
	bool operator > (const Vector2d & vec) const;
	// arithmetic operations
	Vector2d operator + (const Vector2d & vec) const;
	Vector2d operator - (const Vector2d & vec) const;
	Vector2d operator * (real scalar) const;
	Vector2d operator * (const Vector2d & vec) const;
	Vector2d operator / (real scalar) const;
	Vector2d operator / (const Vector2d & vec) const;
	const Vector2d & operator + () const;
	Vector2d operator - () const;
	// arithmetic updates
	Vector2d & operator += (const Vector2d & vec);
	Vector2d & operator += (real scalar);
	Vector2d & operator -= (const Vector2d & vec);
	Vector2d & operator -= (real scalar);
	Vector2d & operator *= (real scalar);
	Vector2d & operator *= (const Vector2d & vec);
	Vector2d & operator /= (real scalar);
	Vector2d & operator /= (const Vector2d & vec);
	bool isZeroLength() const;
	real length () const;
	real dotProduct(const Vector2d & vec) const;
	real normalise();
	real crossProduct(const Vector2d & vec) const;
	Vector2d getNormalised() const;
};

}

#endif // VECTOR2D_HPP

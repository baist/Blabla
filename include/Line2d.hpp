#if !defined (LINE2D_HPP)
#define LINE2D_HPP

#include "Prerequisites.hpp"
#include "Vector2d.hpp"

namespace bge
{

class Line2d
{
public:
	Vector2d a;
	Vector2d b;
public:
	Line2d() {};
	//Line2d(const Line2d& v):m_iX0(v.m_iX0), m_iY0(v.m_iY0), m_iX1(v.m_iX1), m_iY1(v.m_iY1){};
	Line2d(int xA, int yA, int xB, int yB): a(xA, yA), b(xB, yB) {};
	Line2d(real xA, real yA, real xB, real yB): a(xA, yA), b(xB, yB) {};
};

}

#endif // LINE2D_HPP

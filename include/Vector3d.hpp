#if !defined (VECTOR3D_HPP)
#define VECTOR3D_HPP

#include "Prerequisites.hpp"
#include "Math.hpp"
#include "Quaternion.hpp"

namespace bge
{

class Vector3d
{
public:
	real x, y, z;
public:
	friend Vector3d operator * (real scalar, const Vector3d & vec)
	{
		return Vector3d(scalar * vec.x, scalar * vec.y, scalar * vec.z);
	}
	friend Vector3d operator / (real scalar, const Vector3d & vec)
	{
		return Vector3d(scalar / vec.x, scalar / vec.y, scalar / vec.z);
	}
	friend Vector3d operator + (const Vector3d & vec, real val)
	{
		return Vector3d(vec.x + val, vec.y + val, vec.z + val);
	}
	friend Vector3d operator + (real val, const Vector3d & vec)
	{
		return Vector3d(val + vec.x, val + vec.y, val + vec.z);
	}
	friend Vector3d operator - (const Vector3d & vec, real val)
	{
		return Vector3d(vec.x - val, vec.y - val, vec.z - val);
	}
	friend Vector3d operator - (real val, const Vector3d & vec)
	{
		return Vector3d(val - vec.x, val - vec.y, val - vec.z);
	}
public:
	Vector3d();
	Vector3d(real valX, real valY, real valZ);
	explicit Vector3d(const real val[3]);
	explicit Vector3d(real * const val);
	explicit Vector3d(real scalar);
	real operator [] (size_t i ) const;
	real & operator [] (size_t i );
	Vector3d & operator = (const Vector3d & vec);
	Vector3d & operator = (real scalar);
	bool operator == (const Vector3d & vec) const;
	bool operator != (const Vector3d & vec) const;
	bool operator < (const Vector3d & vec) const;
	bool operator > (const Vector3d & vec) const;
	// arithmetic operations
	Vector3d operator + (const Vector3d & vec) const;
	Vector3d operator - (const Vector3d & vec) const;
	Vector3d operator * (real scalar) const;
	Vector3d operator * ( const Vector3d & vec) const;
	Vector3d operator / (real scalar) const;
	Vector3d operator / ( const Vector3d & vec) const;
	const Vector3d & operator + () const;
	Vector3d operator - () const;
	// arithmetic updates
	Vector3d & operator += (const Vector3d & vec);
	Vector3d & operator += (real scalar);
	Vector3d & operator -= (const Vector3d & vec);
	Vector3d & operator -= (real scalar);
	Vector3d & operator *= (const real scalar);
	Vector3d & operator *= (const Vector3d & vec);
	Vector3d & operator /= (const real scalar);
	Vector3d & operator /= (const Vector3d & vec);
	bool isZeroLength() const;
	real length () const;
	real dotProduct(const Vector3d & vec) const;
	real normalise();
	Vector3d crossProduct(const Vector3d & vec) const;
	Vector3d getNormalised() const;
};

}

#endif // VECTOR3D_HPP

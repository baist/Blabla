#if !defined (PLANE_HPP)
#define PLANE_HPP

#include "Prerequisites.hpp"
#include "Vector3d.hpp"

namespace bge
{

class Plane
{
public:
	enum Side {
		NO_SIDE = 0,
		POSITIVE_SIDE,
		NEGATIVE_SIDE,
		BOTH_SIDE
	};
public:
	Vector3d normal;
	real d;
public:
	Plane ();
	Plane (const Plane &val);
	Plane (const Vector3d &norm, real scalar);
	Plane (real a, real b, real c, real d);
	Plane (const Vector3d &norm, const Vector3d &rkPoint);
	Plane (const Vector3d &point0, const Vector3d &point1,
				 const Vector3d &point2);
	bool operator==(const Plane &pln) const;
	bool operator!=(const Plane &pln) const;
	Side getSide (const Vector3d &point) const;
	real getDistance (const Vector3d &point) const;
	void redefine(const Vector3d &point0, const Vector3d &point1,
								const Vector3d &point2);
	void redefine(const Vector3d &norm, const Vector3d &point);
	real normalise(void);
};

}

#endif // PLANE_HPP

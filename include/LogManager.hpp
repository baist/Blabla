#if !defined(LOGMANAGER_HPP)
#define LOGMANAGER_HPP

#include <sstream>
#include "Prerequisites.hpp"
#include "Singleton.hpp"
#include "String.hpp"

namespace bge
{


class LogManager: public Singleton<LogManager>
{
public:
	class Stream
	{
	public:
		// Simple type to indicate a flush of the stream to the log
		struct Flush {};
		Stream(LogManager* p_logMngr):
			m_p_logMngr(p_logMngr)
		{
		}
		virtual ~Stream()
		{
			// flush on destroy
			if (m_cache.tellp() > 0) {
				m_p_logMngr->write(m_cache.str());
			}
		}
		Stream(const Stream& rhs):
			m_p_logMngr(rhs.m_p_logMngr)
		{
			// explicit copy of stream required, gcc doesn't like implicit
			m_cache.str(rhs.m_cache.str());
		}
		template <typename T>
		Stream & operator<<(const T& v)
		{
			m_cache << v;
			return *this;
		}
		Stream & operator<<(const Flush& v)
		{
			(void)v;
			m_p_logMngr->write(m_cache.str());
			return *this;
		}
	protected:
		typedef StringUtil::StrStreamType BaseStream;
	protected:
		LogManager * m_p_logMngr;
		BaseStream m_cache;
	};
public:
	static Stream LogInfo();
	static Stream LogError();
public:
	LogManager();
	virtual ~LogManager();
	void write(const String & msg);
	Stream getStream();
};

}

#endif // LOGMANAGER_HPP

#if !defined (QUATERNION_HPP)
#define QUATERNION_HPP

#include "Prerequisites.hpp"
#include "Math.hpp"

namespace bge
{

class Quaternion
{
public:
	// Cutoff for sine near zero
	static const real EPSILON;
	static const Quaternion ZERO;
	static const Quaternion IDENTITY;
public:
	friend Quaternion operator*(real fScalar, const Quaternion &q)
	{
		return Quaternion(fScalar * q.w, fScalar * q.x, fScalar * q.y,
											fScalar * q.z);
	}
	static Quaternion Slerp (real fT, const Quaternion &rkP,
													 const Quaternion &q, bool shortestPath = false);
	static Quaternion SlerpExtraSpins (real fT,
																		 const Quaternion &rkP, const Quaternion &q,
																		 int iExtraSpins);
	static void Intermediate (const Quaternion &q0,
														const Quaternion &q1, const Quaternion &q2,
														Quaternion &rka, Quaternion &rkB);
	static Quaternion Squad (real fT, const Quaternion &rkP,
													 const Quaternion &rkA, const Quaternion &rkB,
													 const Quaternion &q, bool shortestPath = false);
	static Quaternion nlerp(real fT, const Quaternion &rkP,
													const Quaternion &q, bool shortestPath = false);
public:
	real w, x, y, z;
public:
	Quaternion ();
	Quaternion (real valW, real valX, real valY, real valZ);
	Quaternion(const Matrix3d &rot);
	Quaternion(const real &angle, const Vector3d &axis);
	Quaternion(const Vector3d &xaxis, const Vector3d &yaxis,
						 const Vector3d &zaxis);
	Quaternion(const Vector3d *axis);
	Quaternion(real *valptr);
	real operator [] (const size_t i) const;
	real &operator [] (const size_t i);
	bool operator== (const Quaternion &rhs) const;
	bool operator!= (const Quaternion &rhs) const;
	Quaternion &operator= (const Quaternion &q);
	Quaternion operator+ (const Quaternion &q) const;
	Quaternion operator- (const Quaternion &q) const;
	Quaternion operator* (const Quaternion &q) const;
	Quaternion operator* (real fScalar) const;
	Quaternion operator- () const;
	Vector3d operator* (const Vector3d &vector) const;
	bool isNaN() const;
	real getRoll(bool reprojectAxis = true) const;
	real getPitch(bool reprojectAxis = true) const;
	real getYaw(bool reprojectAxis = true) const;
	void FromRotationMatrix (const Matrix3d &rot);
	void ToRotationMatrix (Matrix3d &rot) const;
	void FromAngleAxis (const real &angle, const Vector3d &axis);
	void ToAngleAxis (real &angle, Vector3d &axis) const;
	void FromAxes (const Vector3d *axis);
	void FromAxes (const Vector3d &xAxis, const Vector3d &yAxis,
								 const Vector3d &zAxis);
	void ToAxes (Vector3d *axis) const;
	void ToAxes (Vector3d &xAxis, Vector3d &yAxis, Vector3d &zAxis) const;
	Vector3d xAxis() const;
	Vector3d yAxis() const;
	Vector3d zAxis() const;
	real Dot (const Quaternion &q) const;
	real Norm () const;
	real normalise();
	Quaternion Inverse () const;
	Quaternion UnitInverse () const;
	Quaternion Exp () const;
	Quaternion Log () const;
	bool equals(const Quaternion &rhs, const real &tolerance) const;
};

}

#endif // QUATERNION_HPP

#if !defined(STRING_HPP)
#define STRING_HPP

#include "Prerequisites.hpp"
#include <string>

namespace bge
{

typedef std::string _StringBase;
typedef std::basic_stringstream<char,std::char_traits<char>,std::allocator<char> > _StringStreamBase;

typedef _StringBase String;
typedef _StringStreamBase StringStream;
typedef StringStream stringstream;

/** Utility class for manipulating Strings.  */
class StringUtil
{
public:
	typedef StringStream StrStreamType;
};

}

#endif // STRING_HPP

#if !defined(MATH_HPP)
#define MATH_HPP

#include "Prerequisites.hpp"
#include <cmath>

const int FIXP8_SHIFT = 8;
const int FIXP8_ROUND_UP = 0x00000080;
const int FIXP16_SHIFT = 16;
const int FIXP16_ROUND_UP = 0x00008000;

#define INT_TO_FIXP8(x) ((x) << FIXP8_SHIFT)
#define FIXP8_TO_INT(x) (((x) + FIXP8_ROUND_UP) >> FIXP8_SHIFT)
#define INT_TO_FIXP16(x) ((x) << FIXP16_SHIFT)
#define FIXP16_TO_INT(x) (((x) + FIXP16_ROUND_UP) >> FIXP16_SHIFT)

namespace bge
{

class Math
{
public:
	static const real POS_INFINITY;
	static const real NEG_INFINITY;
	static const real PI;
	static const real TWO_PI;
	static const real HALF_PI;
	// Stored value of log(2) for frequent use
	static const real LOG2;
public:
	static int AbsI (int val);
	static int CeilI (float val);
	static int FloorI (float val);
	static int SignI (int val);
	static real Abs (real val);
	template <typename T>
	static T Clamp(T val, T minval, T maxval);
	static real ACos (real val);
	static real ASin (real val);
	static real ATan (real val);
	static real ATan2 (real y, real x);
	static real Ceil (real val);
	// Floor function (Floor(1.9) = 1)
	static real Floor (real val);
	static bool isNaN(real f);
	static real Cos (real val, bool useTables = false);
	static real Exp (real val);
	static real Log (real val);
	static real Log2 (real val);
	static real LogN (real base, real val);
	static real Pow (real fBase, real fExponent);
	static real Sign (real val);
	static real Sin (real val, bool useTables = false);
	static real Sqr (real val);
	// Square root function.
	static real Sqrt (real val);
	static real InvSqrt (real val);
	static real Tan (real val, bool useTables = false);
	static bool pointInTri2D(const Vector2d &p, const Vector2d &a,
													 const Vector2d &b, const Vector2d &c);
	static bool pointInTri3D(
			const Vector3d &p, const Vector3d &a,
			const Vector3d &b, const Vector3d &c,
			const Vector3d &normal);
	/** Ray / plane intersection, returns boolean result and distance. */
	static std::pair<bool, real> Intersects(const Ray &ray, const Plane &plane);
	static std::pair<bool, real> Intersects(
			const Ray &ray, const Vector3d &a,
			const Vector3d &b, const Vector3d &c,
			const Vector3d &normal,
			bool positiveSide = true,	bool negativeSide = true);
	static std::pair<bool, real> Intersects(
			const Ray &ray, const Vector3d &a,
			const Vector3d &b, const Vector3d &c,
			bool positiveSide = true, bool negativeSide = true);
	static std::pair<bool, real> Intersects(
	    const Ray &ray, const vector<Plane>::type &planeList,
	    bool normalIsOutside);
	static std::pair<bool, real> Intersects(
	    const Ray &ray, const list<Plane>::type &planeList,
	    bool normalIsOutside);
	static bool RealEqual(
			real a, real b,
			real tolerance = std::numeric_limits<real>::epsilon());
	static Vector4d calcFaceNormal(const Vector3d &v1, const Vector3d &v2,
																 const Vector3d &v3);
	// Calculate a face normal, no w-information.
	static Vector3d calcBasicFaceNormal(const Vector3d &v1, const Vector3d &v2,
																			const Vector3d &v3);
	// Calculate a face normal without normalize, including the w component
	static Vector4d calcFaceNormalWithoutNormalize(const Vector3d &v1,
																								 const Vector3d &v2,
																								 const Vector3d &v3);
	// Calculate a face normal without normalize, no w-information
	static Vector3d calcBasicFaceNormalWithoutNormalize(const Vector3d &v1,
																											const Vector3d &v2,
																											const Vector3d &v3);
	static Vector3d calcBernstein(const Vector3d * p_points, float t);
	static Vector3d calcBernstein(const vector<Vector3d>::type & points,
																float t);
	static Vector3d calcDerivBernstein(const Vector3d * p_points, float t);
	static Vector3d calcDerivBernstein(const vector<Vector3d>::type & points,
																		 float t);
public:
	Math(unsigned int trigTableSize = 4096);
	~Math();
	void buildTrigTables();
protected:
	static size_t staticTrigTableSize;
	// real -> index factor value (staticTrigTableSize / 2 * PI)
	static real staticTrigTableFactor;
	static real *p_staticSinTable;
	static real *p_staticTanTable;
protected:
	static real SinTable (real val);
	static real TanTable (real val);
};

}
#endif // MATH_HPP

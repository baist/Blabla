#if !defined (LINE3D_HPP)
#define LINE3D_HPP

#include "Prerequisites.hpp"
#include "Vector3d.hpp"

namespace bge
{

class Line3d
{
public:
	Vector3d a;
	Vector3d b;
public:
	Line3d() {};
	//Line2d(const Line2d& v):m_iX0(v.m_iX0), m_iY0(v.m_iY0), m_iX1(v.m_iX1), m_iY1(v.m_iY1){};
	Line3d(real xA, real yA, real zA, real xB, real yB, real zB):
		a(xA, yA, zA), b(xB, yB, zB)  {};
};

}

#endif // LINE3D_HPP

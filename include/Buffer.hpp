#if !defined(BUFFER_HPP)
#define BUFFER_HPP

#include <cstddef>

namespace bge
{

class Buffer
{
public:
	explicit Buffer(size_t size);
	virtual ~Buffer();
	size_t getSize() const;
	void map(const void **pp_data) const;
	void unmap() const;
	void map(void **pp_data);
	void unmap();
private:
	bool m_locked;
	unsigned char *m_p_data;
	size_t m_size;
};

}

#endif // BUFFER_HPP

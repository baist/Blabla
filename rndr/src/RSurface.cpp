#include "RSurface.hpp"

namespace bge
{
namespace rndr
{

unsigned int RSurface::computeElemSize(RSurfFormat format)
{
	switch(format) {
	case RSF_I8:
		return 1;
	case RSF_I8I8:
		return 2;
	case RSF_I8I8I8:
		return 3;
	case RSF_I8I8I8I8:
	case RSF_R32:
		return 4;
	case RSF_R64:
		return 8;
	default:
		break;
	}
	return 0;
}

RSurface::RSurface(int width, int height, RSurfFormat format):
	Buffer(width * height * computeElemSize(format)),
	m_format(format),
	m_width(width),
	m_height(height)
{
}

RSurface::~RSurface()
{
}

unsigned int RSurface::getWidth() const
{
	return m_width;
}

unsigned int RSurface::getHeght() const
{
	return m_height;
}

RSurfFormat RSurface::getFormat() const
{
	return m_format;
}

}
}

#include "VertexDescr.hpp"

namespace bge
{
namespace rndr
{


VertexDescr::VertexDescr()
{
}

VertexDescr::~VertexDescr()
{
}

unsigned int VertexDescr::size() const
{
	unsigned int size = 0;
	for(unsigned char i = 0; i < m_descrs.size(); ++i) {
		switch(m_descrs[i].format) {
			case DF_I8:
				size += 1;
				break;
			case DF_I8I8:
			case DF_UI16:
				size += 2;
				break;
			case DF_I8I8I8:
				size += 3;
				break;
			case DF_I8I8I8I8:
			case DF_R32:
				size += 4;
				break;
			case DF_R64:
			case DF_R32R32:
				size += 8;
				break;
			case DF_R32R32R32:
				size += 12;
				break;
			default:
				size += 0;
		}
	}
	return size;
}

unsigned char VertexDescr::count() const
{
	return m_descrs.size();
}

void VertexDescr::add(VertexType vertType, DataFormat format)
{
	VertexDescrItem vertDescItem;
	vertDescItem.type = vertType;
	vertDescItem.format = format;
	m_descrs.push_back(vertDescItem);
}

DataFormat VertexDescr::getFormat(unsigned char index) const
{
	if(index >= m_descrs.size()) {
		return DF_UNKNOWN;
	}
	return m_descrs[index].format;
}

}
}

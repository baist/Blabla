#include "ViewFrustum.hpp"
#include "Vector2d.hpp"

namespace bge
{
namespace rndr
{

ViewFrustum::ViewFrustum():
	m_left(0.0f),
	m_right(0.0f),
	m_top(0.0f),
	m_bottom(0.0f),
	m_near(1.0f),
	m_far(1000.0f),
	m_FOV(Math::PI * 0.25f),
	m_aspect(4.0f / 3.0f),
	m_focalLength(1.0f),
	m_invalidViewMatrix(true),
	m_invalidProjMatrix(true)
{
	updateProjection();
}

ViewFrustum::~ViewFrustum()
{
}

const Matrix4d& ViewFrustum::getViewMatrix() const
{
	return m_viewMatrix;
}

const Matrix4d& ViewFrustum::getProjectionMatrix() const
{
	if(m_invalidProjMatrix) {
		updateProjection();
	}
	return m_projMatrix;
}

void ViewFrustum::updateProjection() const
{
	const real INFINITE_FAR_PLANE_ADJUST = 0.00001;
	// Common calcs
	real thetaY (m_FOV * 0.5f);
	real tanThetaY = Math::Tan(thetaY);
	real tanThetaX = tanThetaY * m_aspect;
	real nearFocal = m_near / m_focalLength;
	real nearOffsetX = m_frustumOffset.x * nearFocal;
	real nearOffsetY = m_frustumOffset.y * nearFocal;
	real halfW = tanThetaX * m_near;
	real halfH = tanThetaY * m_near;
	m_left   = -halfW + nearOffsetX;
	m_right  = +halfW + nearOffsetX;
	m_bottom = -halfH + nearOffsetY;
	m_top    = +halfH + nearOffsetY;
	real invW = 1 / (m_right - m_left);
	real invH = 1 / (m_top - m_bottom);
	real invD = 1 / (m_far - m_near);
	// Recalc if frustum params changed
	// Calc matrix elements for 'uniform' perspective projection matrix
	real A = 2 * m_near * invW;
	real B = 2 * m_near * invH;
	real C = (m_right + m_left) * invW;
	real D = (m_top + m_bottom) * invH;
	real q, qn;
	if (m_far == 0.0f) {
		// Infinite far plane
		q = INFINITE_FAR_PLANE_ADJUST - 1;
		qn = m_near * (INFINITE_FAR_PLANE_ADJUST - 2);
	}
	else {
		// depth range [0,1]
		q = (m_far) * invD;
		qn = (-q) * m_near;
	}
	m_projMatrix = Matrix4d::ZERO;
	// [ A   0   0   0 ]
	// [ 0   B   0   0 ]
	// [ C   D   q   1 ]
	// [ 0   0   qn  0 ]
	m_projMatrix[0][0] = A;
	m_projMatrix[1][1] = B;
	m_projMatrix[2][0] = C;
	m_projMatrix[2][1] = D;
	m_projMatrix[2][2] = q;
	m_projMatrix[2][3] = 1.0f;
	m_projMatrix[3][2] = qn;
	m_invalidProjMatrix = false;
}

}
}

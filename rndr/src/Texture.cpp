#include "Texture.hpp"

namespace bge
{
namespace rndr
{

Texture::Texture(const String &name, int width,
								 int height, ImgFormat format):
	Resource(name),
	m_imageFormat(format),
	m_width(width),
	m_height(height)
{
}

Texture::~Texture()
{
}

int Texture::getWidth() const
{
	return m_width;
}

int Texture::getHeght() const
{
	return m_height;
}

ImgFormat Texture::getFormat() const
{
	return m_imageFormat;
}

Image *Texture::getImage()
{
	return 0;
}

const Image *Texture::getImage() const
{
	return 0;
}

}
}

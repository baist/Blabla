#include <iostream>
#include "HDepthBuffer.hpp"

namespace bge
{
namespace rndr
{

HDepthBuffer::HDepthBuffer(unsigned int w, unsigned int h):
	DepthBuffer(w, h)
{
}

HDepthBuffer::~HDepthBuffer()
{
}

void HDepthBuffer::clear()
{
	DepthBuffer::clear();
}

bool HDepthBuffer::testTri(real aZ, real bZ, real cZ)
{
	bool res = true;
	//Real *p_zBuffer;
	//this->lock(reinterpret_cast<void **>(&p_zBuffer));
	//if(z < p_zBuffer[point.x + point.y * m_width]) {
	//	res = true;
	//}
	///this->unlock();
	return res;
}

bool HDepthBuffer::testPoint(Point point, real z)
{
	const unsigned int w = this->getWidth();
	bool res = false;
	real * p_zBuffer;
	this->map(reinterpret_cast<void **>(&p_zBuffer));
	try {
		if(z < p_zBuffer[point.x + point.y * w]) {
			res = true;
		}
	}
	catch(std::exception e) {
		std::cout << point.x << ", " << point.y << std::endl;
		std::cout << e.what() << std::endl;
	}
	catch(...) {
		std::cout << point.x << ", " << point.y << std::endl;
	}
	this->unmap();
	return res;
}

void HDepthBuffer::writeZ(Point point, real newZ)
{
	const unsigned int w = this->getWidth();
	real *p_zBuffer;
	this->map(reinterpret_cast<void **>(&p_zBuffer));
	p_zBuffer[point.x + point.y * w] = newZ;
	this->unmap();
}

}
}

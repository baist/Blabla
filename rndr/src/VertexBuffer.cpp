#include "VertexBuffer.hpp"

namespace bge
{
namespace rndr
{

VertexBuffer::VertexBuffer(VertexDescr vertDescr, unsigned int count):
	Buffer(vertDescr.size() * count),
	m_vertDescr(vertDescr)
{
}

VertexBuffer::~VertexBuffer()
{
}

VertexDescr VertexBuffer::getVertexDescr() const
{
	return m_vertDescr;
}

}
}

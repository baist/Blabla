#include "IndexBuffer.hpp"

namespace bge
{
namespace rndr
{

unsigned int IndexBuffer::sizeOfIndex(char format)
{
	return format;
}

IndexBuffer::IndexBuffer(char format, unsigned int count):
	Buffer(sizeOfIndex(format) * count)
{
}

IndexBuffer::~IndexBuffer()
{
}

}
}

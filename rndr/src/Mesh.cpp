
#include "Mesh.hpp"

namespace bge
{
namespace rndr
{

Mesh::Mesh(const String & name, VertexBuffer * p_vertBuff,
					 IndexBuffer * p_indBuff, size_t numPrim):
	Resource(name),
	m_p_vertBuffer(p_vertBuff),
	m_p_indBuffer(p_indBuff),
	m_numPrim(numPrim)
{
}

Mesh::~Mesh()
{
}

VertexBuffer * Mesh::getVertexBuffer() const
{
	return m_p_vertBuffer;
}

IndexBuffer * Mesh::getIndexBuffer() const
{
	return m_p_indBuffer;
}

size_t Mesh::getNumPrim() const
{
	return m_numPrim;
}



}
}

#include "Image.hpp"

namespace bge
{
namespace rndr
{

RSurfFormat Image::convertImgFormat2RSurfFormat(ImgFormat format)
{
	switch(format) {
	case IF_R8G8B8:
		return RSF_I8I8I8;
	case IF_A8R8G8B8:
	case IF_X8R8G8B8:
		return RSF_I8I8I8I8;
	default:
		// TODO: error handling
		break;
	}
	return RSF_UNKNOWN;
}

unsigned int Image::computeBPP(ImgFormat format)
{
	switch(format) {
	case IF_R8G8B8:
		return 3;
	case IF_A8R8G8B8:
	case IF_X8R8G8B8:
		return 4;
	default:
		// TODO: error handling
		break;
	}
	return 0;
}

Image::Image(int width, int height, ImgFormat format):
	RSurface(width, height, convertImgFormat2RSurfFormat(format)),
	m_bpp(computeBPP(format))
{
}

Image::~Image()
{
}

unsigned int Image::getBPP() const
{
	return m_bpp;
}

}
}

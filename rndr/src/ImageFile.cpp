
#include "ImageFile.hpp"

namespace bge
{
namespace rndr
{

ImageFile::ImageFile()
{
}

ImageFile::~ImageFile()
{
}

size_t ImageFile::getSize() const
{
	return m_size;
}

size_t ImageFile::getWidth() const
{
	return m_width;
}

size_t ImageFile::getHeight() const
{
	return m_height;
}

unsigned int ImageFile::getBPP() const
{
	return m_BPP;
}

ImgFormat ImageFile::getFormat() const
{
	return m_format;
}

size_t ImageFile::read(void * p_buff, size_t size)
{
	return 0;
}



}
}

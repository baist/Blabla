#include "LogManager.hpp"
#include "Rasterizer.hpp"
#include "Math.hpp"

namespace bge
{
namespace rndr
{

Rasterizer::Rasterizer(unsigned int width, unsigned int height):
	m_width(width),
	m_height(height),
	m_HZBuffer(m_width, m_height)
{
	m_defaultColor = ColourValue::fromABGR(0x00FFFFFF);
	m_p_SDL_window = SDL_CreateWindow(
				"Blabla", SDL_WINDOWPOS_UNDEFINED,
				SDL_WINDOWPOS_UNDEFINED, m_width, m_height, 0);
	if (m_p_SDL_window == NULL) {
		LogManager::LogError() << "Unable to create SDL window: "<< SDL_GetError();
	}
	m_p_SDL_screen = SDL_GetWindowSurface(m_p_SDL_window);
	if (m_p_SDL_screen == NULL) {
		LogManager::LogError() << "Unable to get window surface: "
													 << SDL_GetError();
	}
	m_p_texture = 0;
}

Rasterizer::~Rasterizer()
{
	SDL_DestroyWindow(m_p_SDL_window);
}

void Rasterizer::clear()
{
	SDL_LockSurface(m_p_SDL_screen);
	// Get a pointer to the video surface's memory.
	m_rawPixels = reinterpret_cast<PixelARGB *>(m_p_SDL_screen->pixels);
	SDL_FillRect(m_p_SDL_screen, 0, 0x002244);
	m_HZBuffer.clear();
	m_p_texture = 0;
}

void Rasterizer::update()
{
	SDL_UnlockSurface(m_p_SDL_screen);
	SDL_UpdateWindowSurface(m_p_SDL_window);
}

void Rasterizer::drawTriangle(const RasterTriangle &tri,
															const Texture *p_texture)
{
	m_p_texture = p_texture;
	Prim tmpTri;
	for(unsigned int i =0; i < 3; ++i) {
		tmpTri.p[i] = tri.v[i].pos;
		tmpTri.c[i] = tri.v[i].col;
		tmpTri.t[i] = tri.v[i].tex;
		tmpTri.w[i] = tri.v[i].w;
	}
	_stageScreen(tmpTri);
	/*switch(m_fillMode) {
		case FM_POINT:
			_drawPointedTri();
			break;
		case FM_WIREFRAME:
			_drawWiredTri();
			break;
		case FM_SOLID:
			_drawGSTri();
			break;
	}
	*/
	_drawGSTri(tmpTri);
}

void Rasterizer::_stageScreen(Prim &tri)
{
	for(int i = 0; i < 3; ++i) {
		tri.tp[i].x = (m_width >> 1) - 1;
		tri.tp[i].y = (m_height >> 1) - 1;
		tri.tp[i].x = tri.tp[i].x +
				static_cast<int>(tri.p[i].x * static_cast<float>(tri.tp[i].x));
		tri.tp[i].y = tri.tp[i].y -
				static_cast<int>(tri.p[i].y * static_cast<float>(tri.tp[i].y));
	}
}

int Rasterizer::_Clip_Line(int x1, int y1, int x2, int y2,
											 int *ox1, int *oy1, int *ox2, int *oy2)
{
	const int min_clip_x = 0;
	const int min_clip_y = 0;
	const int max_clip_x = m_width - 1;
	const int max_clip_y = m_height - 1;
	const int CLIP_CODE_C = 0x0000;
	const int CLIP_CODE_N = 0x0008;
	const int CLIP_CODE_S = 0x0004;
	const int CLIP_CODE_E = 0x0002;
	const int CLIP_CODE_W = 0x0001;
	const int CLIP_CODE_NE = 0x000a;
	const int CLIP_CODE_SE = 0x0006;
	const int CLIP_CODE_NW = 0x0009;
	const int CLIP_CODE_SW = 0x0005;
	int xc1 = x1;
	int yc1 = y1;
	int xc2 = x2;
	int yc2 = y2;
	int p1_code = 0;
	int p2_code = 0;
	if(y1 < min_clip_y) {
		p1_code |= CLIP_CODE_N;
	}
	else {
		if (y1 > max_clip_y) {
			p1_code |= CLIP_CODE_S;
		}
	}
	if(x1 < min_clip_x) {
		p1_code |= CLIP_CODE_W;
	}
	else {
		if (x1 > max_clip_x) {
			p1_code |= CLIP_CODE_E;
		}
	}
	if(y2 < min_clip_y) {
		p2_code |= CLIP_CODE_N;
	}
	else {
		if (y2 > max_clip_y) {
			p2_code |= CLIP_CODE_S;
		}
	}
	if(x2 < min_clip_x) {
		p2_code |= CLIP_CODE_W;
	}
	else {
		if (x2 > max_clip_x) {
			p2_code |= CLIP_CODE_E;
		}
	}
	if(p1_code & p2_code) {
		return(0);
	}
	if(p1_code == 0 && p2_code == 0) {
		return(1);
	}
	switch(p1_code) {
		case CLIP_CODE_C:
			break;
		case CLIP_CODE_N: {
			yc1 = min_clip_y;
			xc1 = x1 + 0.5 + (min_clip_y - y1) * (x2 - x1) / (y2 - y1);
		}
		break;
		case CLIP_CODE_S: {
			yc1 = max_clip_y;
			xc1 = x1 + 0.5 + (max_clip_y - y1) * (x2 - x1) / (y2 - y1);
		}
		break;
		case CLIP_CODE_W: {
			xc1 = min_clip_x;
			yc1 = x1 + 0.5 + (min_clip_x - x1) * (x2 - x1) / (y2 - y1);
		}
		break;
		case CLIP_CODE_E: {
			xc1 = max_clip_x;
			yc1 = x1 + 0.5 + (max_clip_x - x1) * (x2 - x1) / (y2 - y1);
		}
		break;
		case CLIP_CODE_NE: {
			yc1 = min_clip_y;
			xc1 = x1 + 0.5 + (min_clip_y - y1) * (x2 - x1) / (y2 - y1);
			if (xc1 < min_clip_x || xc1 > max_clip_x) {
				xc1 = max_clip_x;
				yc1 = y1 + 0.5 + (max_clip_x - x1) * (x2 - x1) / (y2 - y1);
			}
		}
		break;
		case CLIP_CODE_SE: {
			yc1 = max_clip_y;
			xc1 = x1 + 0.5 + (max_clip_y - y1) * (x2 - x1) / (y2 - y1);
			if (xc1 < min_clip_x || xc1 > max_clip_x) {
				xc1 = max_clip_x;
				yc1 = y1 + 0.5 + (max_clip_x - x1) * (x2 - x1) / (y2 - y1);
			}
		}
		break;
		case CLIP_CODE_NW: {
			yc1 = min_clip_y;
			xc1 = x1 + 0.5 + (min_clip_y - y1) * (x2 - x1) / (y2 - y1);
			if (xc1 < min_clip_x || xc1 > max_clip_x) {
				xc1 = min_clip_x;
				yc1 = y1 + 0.5 + (min_clip_x - x1) * (x2 - x1) / (y2 - y1);
			}
		}
		break;
		case CLIP_CODE_SW: {
			yc1 = max_clip_y;
			xc1 = x1 + 0.5 + (max_clip_y - y1) * (x2 - x1) / (y2 - y1);
			if (xc1 < min_clip_x || xc1 > max_clip_x) {
				xc1 = min_clip_x;
				yc1 = y1 + 0.5 + (min_clip_x - x1) * (x2 - x1) / (y2 - y1);
			}
		}
		break;
		default:
			break;
	}
	switch(p2_code) {
		case CLIP_CODE_C:
			break;
		case CLIP_CODE_N: {
			yc2 = min_clip_y;
			xc2 = x2 + (min_clip_y - y2) * (x1 - x2) / (y1 - y2);
		}
		break;
		case CLIP_CODE_S: {
			yc2 = max_clip_y;
			xc2 = x2 + (max_clip_y - y2) * (x1 - x2) / (y1 - y2);
		}
		break;
		case CLIP_CODE_W: {
			xc2 = min_clip_x;
			yc2 = y2 + (min_clip_x - x2) * (x1 - x2) / (y1 - y2);
		}
		break;
		case CLIP_CODE_E: {
			xc2 = max_clip_x;
			yc2 = y2 + (max_clip_x - x2) * (x1 - x2) / (y1 - y2);
		}
		break;
		case CLIP_CODE_NE: {
			yc2 = min_clip_y;
			xc2 = x2 + 0.5 + (min_clip_y - y2) * (x1 - x2) / (y1 - y2);
			if (xc2 < min_clip_x || xc2 > max_clip_x) {
				xc2 = max_clip_x;
				yc2 = y2 + 0.5 + (max_clip_x - x2) * (x1 - x2) / (y1 - y2);
			}
		}
		break;
		case CLIP_CODE_SE: {
			yc2 = max_clip_y;
			xc2 = x2 + 0.5 + (max_clip_y - y2) * (x1 - x2) / (y1 - y2);
			if (xc2 < min_clip_x || xc2 > max_clip_x) {
				xc2 = max_clip_x;
				yc2 = y2 + 0.5 + (max_clip_x - x2) * (x1 - x2) / (y1 - y2);
			}
		}
		break;
		case CLIP_CODE_NW: {
			yc2 = min_clip_y;
			xc2 = x2 + 0.5 + (min_clip_y - y2) * (x1 - x2) / (y1 - y2);
			if (xc2 < min_clip_x || xc2 > max_clip_x) {
				xc2 = min_clip_x;
				yc2 = y2 + 0.5 + (min_clip_x - x2) * (x1 - x2) / (y1 - y2);
			}
		}
		break;
		case CLIP_CODE_SW: {
			yc2 = max_clip_y;
			xc2 = x2 + 0.5 + (max_clip_y - y2) * (x1 - x2) / (y1 - y2);
			if (xc2 < min_clip_x || xc2 > max_clip_x) {
				xc2 = min_clip_x;
				yc2 = y2 + 0.5 + (min_clip_x - x2) * (x1 - x2) / (y1 - y2);
			}
		}
		break;
		default:
			break;
	}
	if((xc1 < min_clip_x) || (xc1 > max_clip_x) || (yc1 < min_clip_y) ||
		 (yc1 > max_clip_y) || (xc2 < min_clip_x) || (xc2 > max_clip_x) ||
		 (yc2 < min_clip_y) || (yc2 > max_clip_y)) {
		return 0;
	}
	*ox1 = xc1;
	*oy1 = yc1;
	*ox2 = xc2;
	*oy2 = yc2;
	return 1;
}

void Rasterizer::_draw2DLine(const Line2d & ln, ColourValue color)
{
	//cLn.a.y = HEIGHT - cLn.a.y - 1;
	//cLn.b.y = HEIGHT - cLn.b.y - 1;
	int x1 = ln.a.x;
	int y1 = ln.a.y;
	int x2 = ln.b.x;
	int y2 = ln.b.y;
	//Line2 tempLine(ln);
	if(!_Clip_Line(x1, y1, x2, y2, &x1, &y1, &x2, &y2)) {
		return;
	}
	const unsigned int pitch = m_width;
	PixelARGB *dstSurf = m_rawPixels + pitch * y1 + x1;
	int x_inc;          // amount in pixel space to move during drawing
	int y_inc;          // amount in pixel space to move during drawing
	int dx = x2 - x1; // difference in x's
	int dy = y2 - y1; // difference in y's
	if (dx >= 0) {
		x_inc = 1;
	}
	else {
		x_inc = -1;
		dx = -dx;
	}
	if (dy >= 0) {
		y_inc = pitch;
	}
	else {
		y_inc = -pitch;
		dy = -dy;
	}
	int dx2 = dx * 2;
	int dy2 = dy * 2;
	if (dx > dy) {
		int error = dy2 - dx;
		for (int i = 0; i <= dx; ++i) {
			dstSurf->r = color.getR();
			dstSurf->g = color.getG();
			dstSurf->b = color.getB();
			if (error >= 0)  {
				error -= dx2;
				dstSurf = dstSurf + y_inc;
			}
			error += dy2;
			dstSurf = dstSurf + x_inc;
		}
	}
	else {
		// the discriminant i.e. error i.e. decision variable
		int error = dx2 - dy;
		for (int i = 0; i <= dy; ++i) {
			dstSurf->r = color.getR();
			dstSurf->g = color.getG();
			dstSurf->b = color.getB();
			if (error >= 0) {
				error -= dy2;
				dstSurf = dstSurf + x_inc;
			}
			error += dx2;
			dstSurf = dstSurf + y_inc;
		}
	}
}

void Rasterizer::_drawPointedTri()
{
	//RenderItems::iterator it = m_renderItems.begin();
	//RenderItems::iterator end = m_renderItems.end();
	//for(; it != end; ++it) {
		Prim tri;// = (*it);
		PixelARGB *data = m_rawPixels;
		int x1 = tri.tp[0].x;
		int y1 = tri.tp[0].y;
		int x2 = tri.tp[1].x;
		int y2 = tri.tp[1].y;
		int x3 = tri.tp[2].x;
		int y3 = tri.tp[2].y;
		if((x1 > 0) && (x1 < m_width - 1) &&
			 (y1 > 0) && (y1 < m_height - 1)) {
			data[m_width * y1 + x1].r = tri.c[0].getR();
			data[m_width * y1 + x1].g = tri.c[0].getG();
			data[m_width * y1 + x1].b = tri.c[0].getB();
		}
		if((x2 > 0) && (x2 < m_width - 1) &&
			 (y2 > 0) && (y2 < m_height - 1)) {
			data[m_width * y2 + x2].r = tri.c[1].getR();
			data[m_width * y2 + x2].g = tri.c[1].getG();
			data[m_width * y2 + x2].b = tri.c[1].getB();
		}
		if((x3 > 0) && (x3 < m_width - 1) &&
			 (y3 > 0) && (y3 < m_height - 1)) {
			data[m_width * y3 + x3].r = tri.c[2].getR();
			data[m_width * y3 + x3].g = tri.c[2].getG();
			data[m_width * y3 + x3].b = tri.c[2].getB();
		}
	//}
}

void Rasterizer::_drawWiredTri(const Prim &tri)
{
	_draw2DLine(Line2d(tri.tp[0].x, tri.tp[0].y, tri.tp[1].x, tri.tp[1].y),
			tri.c[0]);
	_draw2DLine(Line2d(tri.tp[1].x, tri.tp[1].y, tri.tp[2].x, tri.tp[2].y),
			tri.c[1]);
	_draw2DLine(Line2d(tri.tp[2].x, tri.tp[2].y, tri.tp[0].x, tri.tp[0].y),
			tri.c[2]);
}

void Rasterizer::_drawFSTri()
{
}

void Rasterizer::_drawGSTri(const Prim &tri)
{
	int i0 = 0;
	int i1 = 1;
	int i2 = 2;
	if((tri.tp[i0].x == tri.tp[i1].x) && (tri.tp[i1].x == tri.tp[i2].x)) {
		return;
	}
	if((tri.tp[i0].y == tri.tp[i1].y) && (tri.tp[i1].y == tri.tp[i2].y)) {
		return;
	}
	if(!m_HZBuffer.testTri(0.0f, 0.0f, 0.0f)) {
		return;
	}
	if (tri.tp[i0].y > tri.tp[i1].y) {
		std::swap(i0, i1);
	}
	if (tri.tp[i2].y < tri.tp[i0].y) {
		std::swap(i2, i0);
	}
	if (tri.tp[i1].y > tri.tp[i2].y) {
		std::swap(i1, i2);
	}
	const int h0 = tri.tp[i1].y - tri.tp[i0].y;
	const int h1 = tri.tp[i2].y - tri.tp[i1].y;
	if(h0 == 0) {
		if(tri.tp[i0].x > tri.tp[i1].x) {
			std::swap(i0, i1);
		}
		Prim tmpTri;
		tmpTri.p[0] = tri.p[i0];
		tmpTri.p[1] = tri.p[i1];
		tmpTri.p[2] = tri.p[i2];
		tmpTri.w[0] = tri.w[i0];
		tmpTri.w[1] = tri.w[i1];
		tmpTri.w[2] = tri.w[i2];
		tmpTri.tp[0] = tri.tp[i0];
		tmpTri.tp[1] = tri.tp[i1];
		tmpTri.tp[2] = tri.tp[i2];
		tmpTri.c[0] = tri.c[i0];
		tmpTri.c[1] = tri.c[i1];
		tmpTri.c[2] = tri.c[i2];
		tmpTri.t[0] = tri.t[i0];
		tmpTri.t[1] = tri.t[i1];
		tmpTri.t[2] = tri.t[i2];
		_drawGSChunkTri(&tmpTri, 0);
	}
	else if(h1 == 0) {
		if(tri.tp[i1].x > tri.tp[i2].x) {
			std::swap(i1, i2);
		}
		Prim tmpTri;
		tmpTri.p[0] = tri.p[i0];
		tmpTri.p[1] = tri.p[i1];
		tmpTri.p[2] = tri.p[i2];
		tmpTri.w[0] = tri.w[i0];
		tmpTri.w[1] = tri.w[i1];
		tmpTri.w[2] = tri.w[i2];
		tmpTri.tp[0] = tri.tp[i0];
		tmpTri.tp[1] = tri.tp[i1];
		tmpTri.tp[2] = tri.tp[i2];
		tmpTri.c[0] = tri.c[i0];
		tmpTri.c[1] = tri.c[i1];
		tmpTri.c[2] = tri.c[i2];
		tmpTri.t[0] = tri.t[i0];
		tmpTri.t[1] = tri.t[i1];
		tmpTri.t[2] = tri.t[i2];
		_drawGSChunkTri(&tmpTri, 1);
	}
	else {
		// general triangle that's needs to be broken up along long edge
		const int bigH = tri.tp[i2].y - tri.tp[i0].y;
		const int smallH = tri.tp[i1].y - tri.tp[i0].y;
		const int newX = FIXP8_TO_INT( INT_TO_FIXP8(tri.tp[i0].x) +
			INT_TO_FIXP8(smallH) * (tri.tp[i2].x - tri.tp[i0].x) / bigH );
		const real newZ = tri.p[i0].z + static_cast<real>(smallH) *
				(tri.p[i2].z - tri.p[i0].z) / static_cast<real>(bigH);
		const real newW = tri.w[i0] + static_cast<real>(smallH) *
				(tri.w[i2] - tri.w[i0]) / static_cast<real>(bigH);
		const ColourValue newC = tri.c[i0] + (tri.c[i2] - tri.c[i0]) /
				bigH * smallH;
		const real newU = tri.t[i0].x + static_cast<real>(smallH) *
				(tri.t[i2].x - tri.t[i0].x) / static_cast<real>(bigH);
		const real newV = tri.t[i0].y + static_cast<real>(smallH) *
				(tri.t[i2].y - tri.t[i0].y) / static_cast<real>(bigH);
		int newIndex = newX > tri.tp[i1].x ? 1 : 0;
		Prim prim0;
		prim0.p[0] = tri.p[i0];
		prim0.p[1] = tri.p[i1];
		prim0.p[2] = tri.p[i1];
		prim0.w[0] = tri.w[i0];
		prim0.w[1] = tri.w[i1];
		prim0.w[2] = tri.w[i1];
		prim0.tp[0] = tri.tp[i0];
		prim0.tp[1] = tri.tp[i1];
		prim0.tp[2] = tri.tp[i1];
		prim0.c[0] = tri.c[i0];
		prim0.c[1] = tri.c[i1];
		prim0.c[2] = tri.c[i1];
		prim0.t[0] = tri.t[i0];
		prim0.t[1] = tri.t[i1];
		prim0.t[2] = tri.t[i1];
		prim0.tp[newIndex + 1].x = newX;
		prim0.p[newIndex + 1].z = newZ;
		prim0.w[newIndex + 1] = newW;
		prim0.c[newIndex + 1] = newC;
		prim0.t[newIndex + 1].x = newU;
		prim0.t[newIndex + 1].y = newV;
		_drawGSChunkTri(&prim0, 1);
		Prim prim1;
		prim1.p[0] = tri.p[i1];
		prim1.p[1] = tri.p[i1];
		prim1.p[2] = tri.p[i2];
		prim1.w[0] = tri.w[i1];
		prim1.w[1] = tri.w[i1];
		prim1.w[2] = tri.w[i2];
		prim1.tp[0] = tri.tp[i1];
		prim1.tp[1] = tri.tp[i1];
		prim1.tp[2] = tri.tp[i2];
		prim1.c[0] = tri.c[i1];
		prim1.c[1] = tri.c[i1];
		prim1.c[2] = tri.c[i2];
		prim1.t[0] = tri.t[i1];
		prim1.t[1] = tri.t[i1];
		prim1.t[2] = tri.t[i2];
		prim1.tp[newIndex].x = newX;
		prim1.p[newIndex].z = newZ;
		prim1.w[newIndex] = newW;
		prim1.c[newIndex] = newC;
		prim1.t[newIndex].x = newU;
		prim1.t[newIndex].y = newV;
		_drawGSChunkTri(&prim1, 0);
	}
}

void Rasterizer::_drawGSChunkTri(const Prim *p_prim, unsigned int orient)
{
	// this function draws a gouraud shaded polygon, based on the affine
	// texture mapper, instead of interpolating the texture coordinates, we
	// simply interpolate the (R,G,B) values across the polygons, I simply
	// needed at another interpolant, I have mapped u->red, v->green, w->blue
	// note that this is the 8-bit version, and I have decided to throw
	// caution at the wind and see what happens if we do a full RGB
	// interpolation and then at the last minute use the color lookup to find
	// the appropriate color
	int lDX;
	int rDX;
	int eX;
	real lDZ;
	real rDZ;
	real eZ;
	real lDW;
	real rDW;
	real eW;
	ColourValue lDC;
	ColourValue rDC;
	ColourValue eC;
	real lDU;
	real lDV;
	real rDU;
	real rDV;
	real eU;
	real eV;
	const int h = (p_prim->tp[2].y - p_prim->tp[0].y);
	int sX = INT_TO_FIXP16(p_prim->tp[0].x);
	real sZ = p_prim->p[0].z;
	real sW = p_prim->w[0];
	ColourValue sC = p_prim->c[0];
	//real sU = p_prim->t[0].x * p_prim->w[0];
	//real sV = p_prim->t[0].y * p_prim->w[0];
	real sU = p_prim->t[0].x;
	real sV = p_prim->t[0].y;
	// compute all deltas
	if(orient) {
		// top
		lDX = INT_TO_FIXP16(p_prim->tp[1].x - p_prim->tp[0].x) / h;
		rDX = INT_TO_FIXP16(p_prim->tp[2].x - p_prim->tp[0].x) / h;
		eX = sX;
		lDZ = (p_prim->p[1].z - p_prim->p[0].z) / static_cast<real>(h);
		rDZ = (p_prim->p[2].z - p_prim->p[0].z) / static_cast<real>(h);
		eZ = sZ;
		lDW = (p_prim->w[1] - p_prim->w[0]) / static_cast<real>(h);
		rDW = (p_prim->w[2] - p_prim->w[0]) / static_cast<real>(h);
		eW = sW;
		lDC = (p_prim->c[1] - p_prim->c[0]) / static_cast<real>(h);
		rDC = (p_prim->c[2] - p_prim->c[0]) / static_cast<real>(h);
		eC = sC;
		lDU = (p_prim->t[1].x - p_prim->t[0].x) / static_cast<real>(h);
		lDV = (p_prim->t[1].y - p_prim->t[0].y) / static_cast<real>(h);
		rDU = (p_prim->t[2].x - p_prim->t[0].x) / static_cast<real>(h);
		rDV = (p_prim->t[2].y - p_prim->t[0].y) / static_cast<real>(h);
		eU = sU;
		eV = sV;
	}
	else {
		// bottom
		lDX = INT_TO_FIXP16(p_prim->tp[2].x - p_prim->tp[0].x) / h;
		rDX = INT_TO_FIXP16(p_prim->tp[2].x - p_prim->tp[1].x) / h;
		eX = INT_TO_FIXP16(p_prim->tp[1].x);
		lDZ = (p_prim->p[2].z - p_prim->p[0].z) / static_cast<real>(h);
		rDZ = (p_prim->p[2].z - p_prim->p[1].z) / static_cast<real>(h);
		eZ = p_prim->p[1].z;
		lDW = (p_prim->w[2] - p_prim->w[0]) / static_cast<real>(h);
		rDW = (p_prim->w[2] - p_prim->w[1]) / static_cast<real>(h);
		eW = p_prim->w[1];
		lDC = (p_prim->c[2] - p_prim->c[0]) / static_cast<real>(h);
		rDC = (p_prim->c[2] - p_prim->c[1]) / static_cast<real>(h);
		eC = p_prim->c[1];
		lDU = (p_prim->t[2].x - p_prim->t[0].x) / static_cast<real>(h);
		lDV = (p_prim->t[2].y - p_prim->t[0].y) / static_cast<real>(h);
		rDU = (p_prim->t[2].x - p_prim->t[1].x) / static_cast<real>(h);
		rDV = (p_prim->t[2].y - p_prim->t[1].y) / static_cast<real>(h);
		eU = p_prim->t[1].x;
		eV = p_prim->t[1].y;
	}
	// point screen ptr to starting line
	PixelARGB *p_dstSurf = m_rawPixels + m_width * p_prim->tp[0].y;
	for (int tY = p_prim->tp[0].y; tY <= p_prim->tp[2].y; ++tY) {
		if(tY >= 0 || tY < static_cast<int>(m_height)) {
			ScanLinePoint a;
			a.x = sX;
			a.u = sU;
			a.v = sV;
			a.z = sZ;
			a.w = sW;
			a.c = sC;
			ScanLinePoint b;
			b.x = eX;
			b.u = eU;
			b.v = eV;
			b.z = eZ;
			b.w = eW;
			b.c = eC;
			_drawScanLine(a, b, tY, p_dstSurf);
		}
		else {
			// TODO: print error for Y
		}
		sX += lDX;
		eX += rDX;
		sZ += lDZ;
		eZ += rDZ;
		sW += lDW;
		eW += rDW;
		sC += lDC;
		eC += rDC;
		sU += lDU;
		sV += lDV;
		eU += rDU;
		eV += rDV;
		p_dstSurf += m_width;
	}
}

void Rasterizer::_drawScanLine(ScanLinePoint a, ScanLinePoint b, int y,
													 PixelARGB *p_dstSurf)
{
	const int startX = FIXP16_TO_INT(a.x);
	const int endX   = FIXP16_TO_INT(b.x);
	real tZ = a.z;
	real dZ;
	real tW = a.w;
	real dW;
	ColourValue tC = a.c;
	ColourValue dC;
	real tU = a.u;
	real tV = a.v;
	real dU;
	real dV;
	const char *p_imgBits;
	const Image *p_img;
	if(m_p_texture != 0) {
		p_img = m_p_texture->getImage();
		p_img->map(reinterpret_cast<const void **>(&p_imgBits));
	}
	const int d = endX - startX;
	if (d > 0) {
		const real realD = static_cast<real>(d);
		dZ = (b.z - a.z) / realD;
		dW = (b.w - a.w) / realD;
		dC = (b.c - a.c) / realD;
		dU = (b.u - a.u) / realD;
		dV = (b.v - a.v) / realD;
	}
	else {
		dZ = (b.z - a.z);
		dW = (b.w - a.w);
		dC = (b.c - a.c);
		dU = (b.u - a.u);
		dV = (b.v - a.v);
	}
	for (int tX = startX; tX <= endX; ++tX) {
		if(tX >= 0 || tX < static_cast<int>(m_width)) {
			Point point(tX, y);
			if(m_HZBuffer.testPoint(point, tZ)) {
				PixelARGB &dstPixel = p_dstSurf[tX];
				dstPixel.a = 0xFF;
				if(m_p_texture != 0) {
					ImageLock imgLock;
					imgLock.pitch = p_img->getWidth() * p_img->getBPP();
					imgLock.p_bits = p_imgBits;
					ColourValue clrValue =
							_noneFiltrng(&imgLock, p_img, tU / tW, tV / tW);
					clrValue *= tC;
					dstPixel.r = clrValue.getR();
					dstPixel.g = clrValue.getG();
					dstPixel.b = clrValue.getB();
				}
				else {
					dstPixel.r = tC.getR();
					dstPixel.g = tC.getG();
					dstPixel.b = tC.getB();
				}
				m_HZBuffer.writeZ(point, tZ);
			}
		}
		else {
			// TODO: print error for X
		}
		tZ += dZ;
		tW += dW;
		tC += dC;
		tU += dU;
		tV += dV;
	}
	if(m_p_texture != 0) {
		p_img->unmap();
	}
}

ColourValue Rasterizer::_noneFiltrng(const ImageLock *p_imgLock,
																	const Image *p_img,
																	real u, real v)
{
	if((u < 0.0f) || (v < 0.0f)) {
		return ColourValue(1.0f, 0.0f, 1.0f);
	}
	if((u > 1.0f) || (v > 1.0f)) {
		return ColourValue(1.0f, 0.0f, 1.0f);
	}
	const unsigned int bpp = p_img->getBPP();
	const uint8 *p_pixelData =
			reinterpret_cast<const uint8 *>(p_imgLock->p_bits);
	const real tU = static_cast<real>(p_img->getWidth() - 1) * u;
	const real tV = static_cast<real>(p_img->getHeght() - 1) * v;
	const unsigned int tX = static_cast<unsigned int>(Math::CeilI(tU));
	const unsigned int tY = static_cast<unsigned int>(Math::CeilI(tV));
	const size_t off = p_imgLock->pitch * tY + tX * bpp;
	ColourValue clr =
			ColourValue::fromXRGB(
				*reinterpret_cast<const XRGB*>(p_pixelData + off));
	return clr;
}

ColourValue Rasterizer::_bilnrFiltrng(const ImageLock *p_imgLock,
																	const Image * p_img,
																	real u, real v)
{
	if((u < 0.0f) || (v < 0.0f)) {
		return ColourValue(1.0f, 0.0f, 1.0f);
	}
	if((u > 1.0f) || (v > 1.0f)) {
		return ColourValue(1.0f, 0.0f, 1.0f);
	}
	const unsigned int bpp = p_img->getBPP();
	const uint8 *p_pixelData =
			reinterpret_cast<const uint8 *>(p_imgLock->p_bits);
	const real tU = static_cast<real>(p_img->getWidth()) * u;
	const real tV = static_cast<real>(p_img->getHeght()) * v;
	const unsigned int tX = static_cast<unsigned int>(Math::FloorI(tU));
	const unsigned int tY = static_cast<unsigned int>(Math::FloorI(tV));
	const real ratioU = tU - static_cast<real>(tX);
	const real ratioV = tV - static_cast<real>(tY);
	const real oppositeU = 1.0f - ratioU;
	const real oppositeV = 1.0f - ratioV;
	const unsigned int offsA = p_imgLock->pitch * tY + (bpp * tX);
	const unsigned int offsB = p_imgLock->pitch * tY + (bpp * (tX + 1));
	const unsigned int offsC = (p_imgLock->pitch * (tY + 1)) + (bpp * tX);
	const unsigned int offsD = (p_imgLock->pitch * (tY + 1)) + (bpp * (tX + 1));
	ColourValue clrA =
			ColourValue::fromXRGB(
				*reinterpret_cast<const XRGB*>(p_pixelData + offsA));
	ColourValue clrB =
			ColourValue::fromXRGB(
				*reinterpret_cast<const XRGB*>(p_pixelData + offsB));
	ColourValue clrC =
			ColourValue::fromXRGB(
				*reinterpret_cast<const XRGB*>(p_pixelData + offsC));
	ColourValue clrD =
			ColourValue::fromXRGB(
				*reinterpret_cast<const XRGB*>(p_pixelData + offsD));
	return ((clrA * oppositeU + clrB * ratioU) * oppositeV +
					(clrC * oppositeU + clrD * ratioU) * ratioV);
}


}
}

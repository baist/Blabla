#include "DepthBuffer.hpp"

namespace bge
{
namespace rndr
{

DepthBuffer::DepthBuffer(unsigned int w, unsigned int h):
	RSurface(w, h, RSF_R32)
{
}

DepthBuffer::~DepthBuffer()
{
}

void DepthBuffer::clear()
{
	const unsigned int w = this->getWidth();
	const unsigned int h = this->getHeght();
	real *p_zBuffer;
	this->map(reinterpret_cast<void **>(&p_zBuffer));
	std::fill_n(p_zBuffer, w * h, 1.0f);
	this->unmap();
}

}
}

/* Example of direct pixel access with SDL. */

#include <stdio.h>
#include <stdlib.h>
#include <queue>
#include <algorithm>
#include "Plane.hpp"
#include "Line2d.hpp"
#include "Line3d.hpp"
#include "Vector3d.hpp"
#include "Vector4d.hpp"
#include "Matrix4d.hpp"
#include "Ray.hpp"
#include "Math.hpp"
#include "ColourValue.hpp"
#include "Point.hpp"
#include "Buffer.hpp"
#include "HDepthBuffer.hpp"
#include "Render.hpp"
#include "VertexDescr.hpp"

namespace bge
{
namespace rndr
{

const int FIXP8_SHIFT = 8;
const int FIXP8_ROUND_UP = 0x00000080;
const int FIXP16_SHIFT = 16;
const int FIXP16_ROUND_UP = 0x00008000;

#define INT_TO_FIXP8(x) ((x) << FIXP8_SHIFT)
#define FIXP8_TO_INT(x) (((x) + FIXP8_ROUND_UP) >> FIXP8_SHIFT)
#define INT_TO_FIXP16(x) ((x) << FIXP16_SHIFT)
#define FIXP16_TO_INT(x) (((x) + FIXP16_ROUND_UP) >> FIXP16_SHIFT)

Render::Render():
	m_width(640),
	m_height(480),
	m_rasterizer(m_width, m_height)
{
	m_defaultColor = 0x00FFFFFF;
	m_cullMode = CM_CW;
	m_fillMode = FM_SOLID;
	m_p_vertBuffer = 0;
	m_p_indBuffer = 0;
	m_worldMatr = Matrix4d::IDENTITY;
	m_projMatr = Matrix4d::IDENTITY;
	m_p_texture = 0;
}

Render::~Render()
{
}

void Render::begin()
{
	m_rasterizer.clear();
	m_p_texture = 0;
}

void Render::end()
{
	m_rasterizer.update();
}

void Render::setDefaultColor(Uint32 color)
{
	m_defaultColor = color;
}

void Render::setCullMode(CullMode cullMode)
{
	m_cullMode = cullMode;
}

void Render::setFillMode(FillMode fillMode)
{
	m_fillMode = fillMode;
}

void Render::setWorldMatrix(const Matrix4d * mat)
{
	m_worldMatr = *mat;
}

void Render::setProjMatrix(const Matrix4d & matr)
{
	m_projMatr = matr;
}

void Render::setTexture(const Texture * tex)
{
	m_p_texture = tex;
}

void Render::setVertexBuffer(VertexBuffer *p_vertBuffer)
{
	m_p_vertBuffer = p_vertBuffer;
}

void Render::setIndexBuffer(IndexBuffer *p_indBuffer)
{
	m_p_indBuffer = p_indBuffer;
}

void Render::drawLine2D(Line2d & ln, ColourValue color)
{
	//_draw2DLine(ln, color);
}

void Render::drawTriMesh(unsigned int numTri)
{
	if(numTri == 0) {
		return;
	}
	if(m_p_vertBuffer->getSize() == 0 || m_p_indBuffer->getSize() == 0) {
		return;
	}
	VertexDescr vrtDscr = m_p_vertBuffer->getVertexDescr();
	if(vrtDscr.count() == 0) {
		return;
	}
	bool hasNorms = false;
	const char *p_v;
	const unsigned int *p_i;
	m_p_vertBuffer->map(reinterpret_cast<const void **>(&p_v));
	m_p_indBuffer->map(reinterpret_cast<const void **>(&p_i));
	for(unsigned int i = 0; i < numTri; ++i, p_i += 3) {
		VisualTri tri;
		tri.t[0].x = 0.0f;
		tri.t[0].y = 0.0f;
		tri.t[1].x = 1.0f;
		tri.t[1].y = 0.0f;
		tri.t[2].x = 0.0f;
		tri.t[2].y = 1.0f;
		for(unsigned int j = 0; j < 3; ++j) {
			unsigned char sel = 0;
			const char *p_temp = p_v + p_i[j] * vrtDscr.size();
			if(vrtDscr.getFormat(sel) == DF_R32R32R32) {
				tri.p[j] = *reinterpret_cast<const Vector3d *>(p_temp);
				p_temp += sizeof(Vector3d);
				++sel;
			}
			if(vrtDscr.getFormat(sel) == DF_I8I8I8I8) {
				tri.c[j].setAsXRGB(*reinterpret_cast<const unsigned int *>(p_temp));
				p_temp += sizeof(unsigned int);
				++sel;
			}
			else {
				tri.c[j].setAsARGB(m_defaultColor);
			}
			if(vrtDscr.getFormat(sel) == DF_R32R32) {
				tri.t[j] = *reinterpret_cast<const Vector2d *>(p_temp);
				p_temp += sizeof(Vector2d);
				++sel;
			}
			if(vrtDscr.getFormat(sel) == DF_R32R32R32) {
				tri.n[j] = *reinterpret_cast<const Vector3d *>(p_temp);
				p_temp += sizeof(Vector3d);
				hasNorms = true;
			}
		}
		tri.faceNormal = Math::calcBasicFaceNormal(
					tri.p[0], tri.p[1], tri.p[2]);
		if(!hasNorms) {
			tri.n[0] = tri.faceNormal;
			tri.n[1] = tri.faceNormal;
			tri.n[2] = tri.faceNormal;
		}
		m_renderItems.push_back(tri);
		_drawTri();
		m_renderItems.clear();
	}
	m_p_indBuffer->unmap();
	m_p_vertBuffer->unmap();
}

Render::RenderItems Render::clipTriangleByPlane(
		const VisualTri & tri, const Plane & plane)
{
	std::list<VisualTri> res;

	unsigned int cnt = 0;
	bool isOutside0 = false;
	bool isOutside1 = false;
	bool isOutside2 = false;
	if(plane.getSide(tri.p[0]) == Plane::NEGATIVE_SIDE) {
		cnt += 1;
		isOutside0 = true;
	}
	if(plane.getSide(tri.p[1]) == Plane::NEGATIVE_SIDE) {
		cnt += 1;
		isOutside1 = true;
	}
	if(plane.getSide(tri.p[2]) == Plane::NEGATIVE_SIDE) {
		cnt += 1;
		isOutside2 = true;
	}
	if(cnt == 0) {
		res.push_back(tri);
	}
	else if(cnt == 3) {
		return res;
	}
	else if(cnt == 2) {
		unsigned int i0;
		unsigned int i1;
		unsigned int i2;
		if(!isOutside1) {
			i0 = 1;
			i1 = 2;
			i2 = 0;
		}
		else if(!isOutside2) {
			i0 = 2;
			i1 = 0;
			i2 = 1;
		}
		else {
			i0 = 0;
			i1 = 1;
			i2 = 2;
		}
		VisualTri newTri;
		newTri.p[0] = tri.p[i0];
		newTri.w[0] = tri.w[i0];
		newTri.w[1] = tri.w[i1];
		newTri.w[2] = tri.w[i2];
		newTri.n[0] = tri.n[i0];
		newTri.n[1] = tri.n[i1];
		newTri.n[2] = tri.n[i2];
		newTri.c[0] = tri.c[i0];
		newTri.c[1] = tri.c[i1];
		newTri.c[2] = tri.c[i2];
		newTri.t[0] = tri.t[i0];
		newTri.t[1] = tri.t[i1];
		newTri.t[2] = tri.t[i2];
		const Vector3d &p0 = tri.p[i0];
		const Vector3d &p1 = tri.p[i1];
		const Vector3d &p2 = tri.p[i2];
		Vector3d way0 = p1 - p0;
		Vector3d way1 = p2 - p0;
		Ray ray0(p0, way0.getNormalised());
		Ray ray1(p0, way1.getNormalised());
		//std::pair<bool, real> res0 = Math::Intersects(ray0, plane);
		//std::pair<bool, real> res1 = Math::Intersects(ray1, plane);
		const real dist0 = Math::Intersects(ray0, plane).second / way0.length();
		const real dist1 = Math::Intersects(ray1, plane).second / way1.length();
		newTri.p[1] = p0 + way0 * dist0;
		newTri.p[2] = p0 + way1 * dist1;
		newTri.w[1] = tri.w[i0] + (tri.w[i1] - tri.w[i0]) * dist0;
		newTri.w[2] = tri.w[i0] + (tri.w[i2] - tri.w[i0]) * dist1;
		newTri.n[1] = tri.n[i0] + (tri.n[i1] - tri.n[i0]) * dist0;
		newTri.n[2] = tri.n[i0] + (tri.n[i2] - tri.n[i0]) * dist1;
		newTri.t[1] = tri.t[i0] + (tri.t[i1] - tri.t[i0]) * dist0;
		newTri.t[2] = tri.t[i0] + (tri.t[i2] - tri.t[i0]) * dist1;
		newTri.c[1] = tri.c[i0] + (tri.c[i1] - tri.c[i0]) * dist0;
		newTri.c[2] = tri.c[i0] + (tri.c[i2] - tri.c[i0]) * dist1;
		res.push_back(newTri);
	}
	else if(cnt == 1) {
		unsigned int i0;
		unsigned int i1;
		unsigned int i2;
		if(isOutside0) {
			i0 = 1;
			i1 = 2;
			i2 = 0;
		}
		else if(isOutside1) {
			i0 = 2;
			i1 = 0;
			i2 = 1;
		}
		else {
			i0 = 0;
			i1 = 1;
			i2 = 2;
		}
		VisualTri newTri0;
		newTri0.p[0] = tri.p[i0];
		newTri0.p[1] = tri.p[i1];
		newTri0.w[0] = tri.w[i0];
		newTri0.w[1] = tri.w[i1];
		newTri0.w[2] = tri.w[i2];
		newTri0.n[0] = tri.n[i0];
		newTri0.n[1] = tri.n[i1];
		newTri0.n[2] = tri.n[i2];
		newTri0.c[0] = tri.c[i0];
		newTri0.c[1] = tri.c[i1];
		newTri0.c[2] = tri.c[i2];
		newTri0.t[0] = tri.t[i0];
		newTri0.t[1] = tri.t[i1];
		newTri0.t[2] = tri.t[i2];
		const Vector3d &p1 = tri.p[i1];
		const Vector3d &p2 = tri.p[i2];
		Vector3d way0 = p2 - p1;
		Ray ray0(p1, way0.getNormalised());
		//std::pair<bool, real> res = Math::Intersects(ray, plane);
		const real dist0 = Math::Intersects(ray0, plane).second / way0.length();
		newTri0.p[2] = p1 + way0 * dist0;
		newTri0.w[2] = tri.w[i1] + (tri.w[i2] - tri.w[i1]) * dist0;
		newTri0.n[2] = tri.n[i1] + (tri.n[i2] - tri.n[i1]) * dist0;
		newTri0.t[2] = tri.t[i1] + (tri.t[i2] - tri.t[i1]) * dist0;
		newTri0.c[2] = tri.c[i1] + (tri.c[i2] - tri.c[i1]) * dist0;
		VisualTri newTri1;
		newTri1.p[0] = tri.p[i0];
		newTri1.p[1] = newTri0.p[2];
		newTri1.w[0] = tri.w[i0];
		newTri1.w[1] = newTri0.w[2];
		newTri1.w[2] = tri.w[i2];
		newTri1.n[0] = tri.n[i0];
		newTri1.n[1] = newTri0.n[2];
		newTri1.n[2] = tri.n[i2];
		newTri1.c[0] = tri.c[i0];
		newTri1.c[1] = newTri0.c[2];
		newTri1.c[2] = tri.c[i2];
		newTri1.t[0] = tri.t[i0];
		newTri1.t[1] = newTri0.t[2];
		newTri1.t[2] = tri.t[i2];
		const Vector3d &p0 = tri.p[i0];
		Vector3d way1 = p2 - p0;
		Ray ray1(p0, way1.getNormalised());
		//std::pair<bool, real> res = Math::Intersects(ray, plane);
		const real dist1 = Math::Intersects(ray1, plane).second / way1.length();
		newTri1.p[2] = p0 + way1 * dist1;
		newTri1.w[2] = tri.w[i0] + (tri.w[i2] - tri.w[i0]) * dist1;
		newTri1.n[2] = tri.n[i0] + (tri.n[i2] - tri.n[i0]) * dist1;
		newTri1.t[2] = tri.t[i0] + (tri.t[i2] - tri.t[i0]) * dist1;
		newTri1.c[2] = tri.c[i0] + (tri.c[i2] - tri.c[i0]) * dist1;
		res.push_back(newTri0);
		res.push_back(newTri1);
	}

	return res;
}

void Render::_stageView()
{
	RenderItems::iterator it = m_renderItems.begin();
	RenderItems::iterator end = m_renderItems.end();
	for(; it != end; ++it) {
		VisualTri & tri = (*it);
		for(int i = 0; i < 3; ++i) {
			Vector4d tempPos = Vector4d (tri.p[i]) * m_worldMatr;
			tri.p[i].x = tempPos.x;
			tri.p[i].y = tempPos.y;
			tri.p[i].z = tempPos.z;
			Vector4d tempNorm = Vector4d(tri.n[i], 0.0f) * m_worldMatr;
			tri.n[i].x = tempNorm.x;
			tri.n[i].y = tempNorm.y;
			tri.n[i].z = tempNorm.z;
			tri.n[i].normalise();
		}
		Vector4d tempFaceNorm = Vector4d(tri.faceNormal, 0.0f) * m_worldMatr;
		tri.faceNormal.x = tempFaceNorm.x;
		tri.faceNormal.y = tempFaceNorm.y;
		tri.faceNormal.z = tempFaceNorm.z;
		tri.faceNormal.normalise();
	}
}

void Render::_stageLighting()
{
	RenderItems::iterator it = m_renderItems.begin();
	RenderItems::iterator end = m_renderItems.end();
	for(; it != end; ++it) {
		VisualTri &tri = (*it);
		Vector3d light(0.0f, 0.0f, -1.0f);
		for(unsigned int i = 0; i < 3; ++i) {
			real dp = tri.n[i].dotProduct(light);
			if(dp < 0.0f) {
				dp = 0.0f;
			}
			tri.c[i] *= dp;
			//tri.c[i] = ColourValue(0.5f + tri.n[i].x * 0.5f,
			//											 0.5f + tri.n[i].y * 0.5f,
			//											 0.5f + tri.n[i].z * 0.5f);
		}
	}
}

void Render::_stageBackFaceCulling()
{
	RenderItems::iterator it = m_renderItems.begin();
	RenderItems::iterator end = m_renderItems.end();
	while(it != end) {
		VisualTri tri = (*it);
		real dp = tri.faceNormal.dotProduct(tri.p[0]);
		RenderItems::iterator tempIt = it;
		++it;
		if((dp <= 0.0f) && (m_cullMode == CM_CCW)) {
			// CW
			m_renderItems.erase(tempIt);
		}
		else if(dp > 0.0f) {
			// CCW
			m_renderItems.erase(tempIt);
			if(m_cullMode != CM_CW) {
				VisualTri newTri;
				newTri.p[0] = tri.p[2];
				newTri.p[1] = tri.p[1];
				newTri.p[2] = tri.p[0];
				newTri.n[0] = tri.n[2];
				newTri.n[1] = tri.n[1];
				newTri.n[2] = tri.n[0];
				newTri.c[0] = tri.c[2];
				newTri.c[1] = tri.c[1];
				newTri.c[2] = tri.c[0];
				newTri.t[0] = tri.t[2];
				newTri.t[1] = tri.t[1];
				newTri.t[2] = tri.t[0];
				newTri.faceNormal = tri.faceNormal;
				m_renderItems.insert(it, newTri);
			}
		}
	}
}

void Render::_stageProjection()
{
	RenderItems::iterator it = m_renderItems.begin();
	RenderItems::iterator end = m_renderItems.end();
	for(; it != end; ++it) {
		VisualTri & tri = (*it);
		for(int i = 0; i < 3; ++i) {
			Vector4d tmpVec = Vector4d(tri.p[i]) * m_projMatr;
			tri.p[i].x = tmpVec.x;
			tri.p[i].y = tmpVec.y;
			tri.p[i].z = tmpVec.z;
			tri.w[i] = tmpVec.w;
		}
	}
}

void Render::_stageNearPlaneClipping()
{
	Plane nearPlane =
			Plane(Vector3d( 0.0f, 0.0f, 1.0f), Vector3d( 0.0f, 0.0f, 0.0f));
	RenderItems::iterator it = m_renderItems.begin();
	RenderItems::iterator end = m_renderItems.end();
	while(it != end) {
		const RenderItems::value_type &tri = (*it);
		RenderItems res = clipTriangleByPlane(tri, nearPlane);
		RenderItems::iterator tempIt = it;
		++it;
		m_renderItems.erase(tempIt);
		unsigned int cnt = res.size();
		if(cnt == 0) {
			continue;
		}
		else if(cnt == 1) {
			m_renderItems.insert(it, *res.begin());
		}
		else if(cnt == 2) {
			RenderItems::const_iterator cit = res.begin();
			m_renderItems.insert(it, *cit);
			++cit;
			m_renderItems.insert(it, *cit);
		}
		else {
			// TODO: print error
			continue;
		}
	}
}

void Render::_stageNormalization()
{
	RenderItems::iterator it = m_renderItems.begin();
	RenderItems::iterator end = m_renderItems.end();
	for(; it != end; ++it) {
		VisualTri &tri = (*it);
		for(size_t i = 0; i < 3; ++i) {
			const float invW = 1.0f / tri.w[i];
			tri.p[i].x *= invW;
			tri.p[i].y *= invW;
			tri.p[i].z *= invW;
			tri.w[i] = invW;
			tri.t[i].x *= invW;
			tri.t[i].y *= invW;
		}
	}
}

void Render::_stageClipping()
{
  const unsigned int NUM_PLANES = 5;
  Plane planes[NUM_PLANES];
	planes[0] = Plane(Vector3d( 0.0f,-1.0f, 0.0f), Vector3d( 0.0f, 1.0f, 0.0f));
	planes[1] = Plane(Vector3d( 0.0f, 1.0f, 0.0f), Vector3d( 0.0f,-1.0f, 0.0f));
	planes[2] = Plane(Vector3d(-1.0f, 0.0f, 0.0f), Vector3d( 1.0f, 0.0f, 0.0f));
	planes[3] = Plane(Vector3d( 1.0f, 0.0f, 0.0f), Vector3d(-1.0f, 0.0f, 0.0f));
	planes[4] = Plane(Vector3d( 0.0f, 0.0f,-1.0f), Vector3d( 0.0f, 0.0f, 1.0f));

	//planes[0] = Plane(Vector3d( 0.0f,-1.0f, 0.0f), Vector3d( 0.0f, 0.5f, 0.0f));
	//planes[1] = Plane(Vector3d( 0.0f, 1.0f, 0.0f), Vector3d( 0.0f,-0.5f, 0.0f));
	//planes[2] = Plane(Vector3d(-1.0f, 0.0f, 0.0f), Vector3d( 0.5f, 0.0f, 0.0f));
	//planes[3] = Plane(Vector3d( 1.0f, 0.0f, 0.0f), Vector3d(-0.5f, 0.0f, 0.0f));
	//planes[4] = Plane(Vector3d( 0.0f, 0.0f,-1.0f), Vector3d( 0.0f, 0.0f, 0.5f));
	//std::cout << "/// Star Clipping \\\\\\"  << std::endl;
	for(int i = 0; i < NUM_PLANES; ++i) {
		const Plane &plane = planes[i];
		RenderItems::iterator it = m_renderItems.begin();
		RenderItems::iterator end = m_renderItems.end();
		while(it != end) {
			const VisualTri tri = (*it);
			RenderItems res = clipTriangleByPlane(tri, plane);
			RenderItems::iterator tempIt = it;
			++it;
			m_renderItems.erase(tempIt);
			unsigned int cnt = res.size();
			if(cnt == 0) {
				continue;
			}
			else if(cnt == 1) {
				m_renderItems.insert(it, *res.begin());
			}
			else if(cnt == 2) {
				RenderItems::const_iterator cit = res.begin();
				m_renderItems.insert(it, *cit);
				++cit;
				m_renderItems.insert(it, *cit);
			}
			else {
				// TODO: print error
				continue;
			}
		}
	}
	//std::cout << "\\\\\\ Stop Clipping ///"  << std::endl;
}

void Render::_stageScreen()
{
	RenderItems::iterator it = m_renderItems.begin();
	RenderItems::iterator end = m_renderItems.end();
	for(; it != end; ++it) {
		const VisualTri &visTri = (*it);
		RasterTriangle rasTri;
		for(unsigned int i =0; i < 3; ++i) {
			rasTri.v[i].pos = visTri.p[i];
			rasTri.v[i].col = visTri.c[i];
			rasTri.v[i].tex = visTri.t[i];
			rasTri.v[i].w = visTri.w[i];
		}
		m_rasterizer.drawTriangle(rasTri, m_p_texture);
	}
}

void Render::_drawTri()
{
	_stageView();
	_stageLighting();
	_stageBackFaceCulling();
	_stageProjection();
	_stageNearPlaneClipping();
	_stageNormalization();
	_stageClipping();
	_stageScreen();
}

}
}

#include "ColourValue.hpp"
#include "Math.hpp"

namespace bge
{
namespace rndr
{

const ColourValue ColourValue::ZERO = ColourValue(0.0, 0.0, 0.0, 0.0);
const ColourValue ColourValue::Black = ColourValue(0.0, 0.0, 0.0);
const ColourValue ColourValue::White = ColourValue(1.0, 1.0, 1.0);
const ColourValue ColourValue::Red = ColourValue(1.0, 0.0, 0.0);
const ColourValue ColourValue::Green = ColourValue(0.0, 1.0, 0.0);
const ColourValue ColourValue::Blue = ColourValue(0.0, 0.0, 1.0);

ColourValue ColourValue::fromRGBX(RGBX val)
{
	ColourValue res;
	res.setAsRGBX(val);
	return res;
}

ColourValue ColourValue::fromXRGB(XRGB val)
{
	ColourValue res;
	res.setAsXRGB(val);
	return res;
}

ColourValue ColourValue::fromXBGR(XBGR val)
{
	ColourValue res;
	res.setAsXBGR(val);
	return res;
}

ColourValue ColourValue::fromBGRX(BGRX val)
{
	ColourValue res;
	res.setAsBGRX(val);
	return res;
}

ColourValue ColourValue::fromRGBA(RGBA val)
{
	ColourValue res;
	res.setAsRGBA(val);
	return res;
}

ColourValue ColourValue::fromARGB(ARGB val)
{
	ColourValue res;
	res.setAsARGB(val);
	return res;
}

ColourValue ColourValue::fromABGR(ABGR val)
{
	ColourValue res;
	res.setAsABGR(val);
	return res;
}

ColourValue ColourValue::fromBGRA(BGRA val)
{
	ColourValue res;
	res.setAsBGRA(val);
	return res;
}

uint8 ColourValue::getA() const
{
	return static_cast<uint8>(a * 255.0f);
}
uint8 ColourValue::getR() const
{
	return static_cast<uint8>(r * 255.0f);
}
uint8 ColourValue::getG() const
{
	return static_cast<uint8>(g * 255.0f);
}
uint8 ColourValue::getB() const
{
	return static_cast<uint8>(b * 255.0f);
}
//---------------------------------------------------------------------
RGBX ColourValue::getAsRGBX(void) const
{
	uint8 val8;
	uint32 val32 = 0;
	// Convert to 32bit pattern
	// (RGBA = 8888)
	// Red
	val8 = static_cast<uint8>(r * 255);
	val32 = val8 << 24;
	// Green
	val8 = static_cast<uint8>(g * 255);
	val32 += val8 << 16;
	// Blue
	val8 = static_cast<uint8>(b * 255);
	val32 += val8 << 8;
	return val32;
}
//---------------------------------------------------------------------
XRGB ColourValue::getAsXRGB(void) const
{
	uint8 val8;
	uint32 val32 = 0;
	// Convert to 32bit pattern
	// (XRGB = 888)
	// Red
	val8 = static_cast<uint8>(r * 255);
	val32 += val8 << 16;
	// Green
	val8 = static_cast<uint8>(g * 255);
	val32 += val8 << 8;
	// Blue
	val8 = static_cast<uint8>(b * 255);
	val32 += val8;
	return val32;
}
//---------------------------------------------------------------------
BGRX ColourValue::getAsBGRX(void) const
{
	uint8 val8;
	uint32 val32 = 0;
	// Convert to 32bit pattern
	// (XRGB = 0888)
	// Blue
	val8 = static_cast<uint8>(b * 255);
	val32 = val8 << 24;
	// Green
	val8 = static_cast<uint8>(g * 255);
	val32 += val8 << 16;
	// Red
	val8 = static_cast<uint8>(r * 255);
	val32 += val8 << 8;
	return val32;
}
//---------------------------------------------------------------------
XBGR ColourValue::getAsXBGR(void) const
{
	uint8 val8;
	uint32 val32 = 0;
	// Convert to 32bit pattern
	// (XBRG = 8888)
	// Blue
	val8 = static_cast<uint8>(b * 255);
	val32 += val8 << 16;
	// Green
	val8 = static_cast<uint8>(g * 255);
	val32 += val8 << 8;
	// Red
	val8 = static_cast<uint8>(r * 255);
	val32 += val8;
	return val32;
}
//---------------------------------------------------------------------
RGBA ColourValue::getAsRGBA(void) const
{
	uint8 val8;
	uint32 val32 = 0;
	// Convert to 32bit pattern
	// (RGBA = 8888)
	// Red
	val8 = static_cast<uint8>(r * 255);
	val32 = val8 << 24;
	// Green
	val8 = static_cast<uint8>(g * 255);
	val32 += val8 << 16;
	// Blue
	val8 = static_cast<uint8>(b * 255);
	val32 += val8 << 8;
	// Alpha
	val8 = static_cast<uint8>(a * 255);
	val32 += val8;
	return val32;
}
//---------------------------------------------------------------------
ARGB ColourValue::getAsARGB(void) const
{
	uint8 val8;
	uint32 val32 = 0;
	// Convert to 32bit pattern
	// (ARGB = 8888)
	// Alpha
	val8 = static_cast<uint8>(a * 255);
	val32 = val8 << 24;
	// Red
	val8 = static_cast<uint8>(r * 255);
	val32 += val8 << 16;
	// Green
	val8 = static_cast<uint8>(g * 255);
	val32 += val8 << 8;
	// Blue
	val8 = static_cast<uint8>(b * 255);
	val32 += val8;
	return val32;
}
//---------------------------------------------------------------------
BGRA ColourValue::getAsBGRA(void) const
{
	uint8 val8;
	uint32 val32 = 0;
	// Convert to 32bit pattern
	// (ARGB = 8888)
	// Blue
	val8 = static_cast<uint8>(b * 255);
	val32 = val8 << 24;
	// Green
	val8 = static_cast<uint8>(g * 255);
	val32 += val8 << 16;
	// Red
	val8 = static_cast<uint8>(r * 255);
	val32 += val8 << 8;
	// Alpha
	val8 = static_cast<uint8>(a * 255);
	val32 += val8;
	return val32;
}
//---------------------------------------------------------------------
ABGR ColourValue::getAsABGR(void) const
{
	uint8 val8;
	uint32 val32 = 0;
	// Convert to 32bit pattern
	// (ABRG = 8888)
	// Alpha
	val8 = static_cast<uint8>(a * 255);
	val32 = val8 << 24;
	// Blue
	val8 = static_cast<uint8>(b * 255);
	val32 += val8 << 16;
	// Green
	val8 = static_cast<uint8>(g * 255);
	val32 += val8 << 8;
	// Red
	val8 = static_cast<uint8>(r * 255);
	val32 += val8;
	return val32;
}
void ColourValue::setA(unsigned int val)
{
	a = val / 255.0f;
}
void ColourValue::setR(unsigned int val)
{
	r = val / 255.0f;
}
void ColourValue::setG(unsigned int val)
{
	g = val / 255.0f;
}
void ColourValue::setB(unsigned int val)
{
	b = val / 255.0f;
}
//---------------------------------------------------------------------
void ColourValue::setAsRGBX(const RGBX val)
{
	uint32 val32 = val;
	// Convert from 32bit pattern
	// (RGBX = 0888)
	// Red
	r = ((val32 >> 24) & 0xFF) / 255.0f;
	// Green
	g = ((val32 >> 16) & 0xFF) / 255.0f;
	// Blue
	b = ((val32 >> 8) & 0xFF) / 255.0f;
	// Alpha
	a = 1.0f;
}
//---------------------------------------------------------------------
void ColourValue::setAsXRGB(const XRGB val)
{
	uint32 val32 = val;
	// Convert from 32bit pattern
	// (XRGB = 0888)
	// Alpha
	a = 1.0f;
	// Red
	r = ((val32 >> 16) & 0xFF) / 255.0f;
	// Green
	g = ((val32 >> 8) & 0xFF) / 255.0f;
	// Blue
	b = (val32 & 0xFF) / 255.0f;
}
//---------------------------------------------------------------------
void ColourValue::setAsBGRX(const BGRX val)
{
	uint32 val32 = val;
	// Convert from 32bit pattern
	// (XRGB = 0888)
	// Blue
	b = ((val32 >> 24) & 0xFF) / 255.0f;
	// Green
	g = ((val32 >> 16) & 0xFF) / 255.0f;
	// Red
	r = ((val32 >> 8) & 0xFF) / 255.0f;
	// Alpha
	a = 1.0f;
}
//---------------------------------------------------------------------
void ColourValue::setAsXBGR(const XBGR val)
{
	uint32 val32 = val;
	// Convert from 32bit pattern
	// (XBGR = 0888)
	// Alpha
	a = 1.0f;
	// Blue
	b = ((val32 >> 16) & 0xFF) / 255.0f;
	// Green
	g = ((val32 >> 8) & 0xFF) / 255.0f;
	// Red
	r = (val32 & 0xFF) / 255.0f;
}
//---------------------------------------------------------------------
void ColourValue::setAsRGBA(const RGBA val)
{
	uint32 val32 = val;
	// Convert from 32bit pattern
	// (RGBA = 8888)
	// Red
	r = ((val32 >> 24) & 0xFF) / 255.0f;
	// Green
	g = ((val32 >> 16) & 0xFF) / 255.0f;
	// Blue
	b = ((val32 >> 8) & 0xFF) / 255.0f;
	// Alpha
	a = (val32 & 0xFF) / 255.0f;
}
//---------------------------------------------------------------------
void ColourValue::setAsARGB(const ARGB val)
{
	uint32 val32 = val;
	// Convert from 32bit pattern
	// (ARGB = 8888)
	// Alpha
	a = ((val32 >> 24) & 0xFF) / 255.0f;
	// Red
	r = ((val32 >> 16) & 0xFF) / 255.0f;
	// Green
	g = ((val32 >> 8) & 0xFF) / 255.0f;
	// Blue
	b = (val32 & 0xFF) / 255.0f;
}
//---------------------------------------------------------------------
void ColourValue::setAsBGRA(const BGRA val)
{
	uint32 val32 = val;
	// Convert from 32bit pattern
	// (ARGB = 8888)
	// Blue
	b = ((val32 >> 24) & 0xFF) / 255.0f;
	// Green
	g = ((val32 >> 16) & 0xFF) / 255.0f;
	// Red
	r = ((val32 >> 8) & 0xFF) / 255.0f;
	// Alpha
	a = (val32 & 0xFF) / 255.0f;
}
//---------------------------------------------------------------------
void ColourValue::setAsABGR(const ABGR val)
{
	uint32 val32 = val;
	// Convert from 32bit pattern
	// (ABGR = 8888)
	// Alpha
	a = ((val32 >> 24) & 0xFF) / 255.0f;
	// Blue
	b = ((val32 >> 16) & 0xFF) / 255.0f;
	// Green
	g = ((val32 >> 8) & 0xFF) / 255.0f;
	// Red
	r = (val32 & 0xFF) / 255.0f;
}
//---------------------------------------------------------------------
bool ColourValue::operator==(const ColourValue &rhs) const
{
	return (r == rhs.r &&
					g == rhs.g &&
					b == rhs.b &&
					a == rhs.a);
}
//---------------------------------------------------------------------
bool ColourValue::operator!=(const ColourValue &rhs) const
{
	return !(*this == rhs);
}
//---------------------------------------------------------------------
void ColourValue::setHSB(real hue, real saturation, real brightness)
{
	// wrap hue
	if (hue > 1.0f) {
		hue -= (int)hue;
	}
	else if (hue < 0.0f) {
		hue += (int)hue + 1;
	}
	// clamp saturation / brightness
	saturation = std::min(saturation, (real)1.0);
	saturation = std::max(saturation, (real)0.0);
	brightness = std::min(brightness, (real)1.0);
	brightness = std::max(brightness, (real)0.0);
	if (brightness == 0.0f) {
		// early exit, this has to be black
		r = g = b = 0.0f;
		return;
	}
	if (saturation == 0.0f) {
		// early exit, this has to be grey
		r = g = b = brightness;
		return;
	}
	real hueDomain  = hue * 6.0f;
	if (hueDomain >= 6.0f) {
		// wrap around, and allow mathematical errors
		hueDomain = 0.0f;
	}
	unsigned short domain = (unsigned short)hueDomain;
	real f1 = brightness * (1 - saturation);
	real f2 = brightness * (1 - saturation * (hueDomain - domain));
	real f3 = brightness * (1 - saturation * (1 - (hueDomain - domain)));
	switch (domain) {
		case 0:
			// red domain; green ascends
			r = brightness;
			g = f3;
			b = f1;
			break;
		case 1:
			// yellow domain; red descends
			r = f2;
			g = brightness;
			b = f1;
			break;
		case 2:
			// green domain; blue ascends
			r = f1;
			g = brightness;
			b = f3;
			break;
		case 3:
			// cyan domain; green descends
			r = f1;
			g = f2;
			b = brightness;
			break;
		case 4:
			// blue domain; red ascends
			r = f3;
			g = f1;
			b = brightness;
			break;
		case 5:
			// magenta domain; blue descends
			r = brightness;
			g = f1;
			b = f2;
			break;
	}
}

void ColourValue::getHSB(real *hue, real *saturation, real *brightness) const
{
	real vMin = std::min(r, std::min(g, b));
	real vMax = std::max(r, std::max(g, b));
	real delta = vMax - vMin;
	*brightness = vMax;
	if (Math::RealEqual(delta, 0.0f, 1e-6)) {
		// grey
		*hue = 0;
		*saturation = 0;
	}
	else {
		// a colour
		*saturation = delta / vMax;
		real deltaR = (((vMax - r) / 6.0f) + (delta / 2.0f)) / delta;
		real deltaG = (((vMax - g) / 6.0f) + (delta / 2.0f)) / delta;
		real deltaB = (((vMax - b) / 6.0f) + (delta / 2.0f)) / delta;
		if (Math::RealEqual(r, vMax)) {
			*hue = deltaB - deltaG;
		}
		else if (Math::RealEqual(g, vMax)) {
			*hue = 0.3333333f + deltaR - deltaB;
		}
		else if (Math::RealEqual(b, vMax)) {
			*hue = 0.6666667f + deltaG - deltaR;
		}
		if (*hue < 0.0f) {
			*hue += 1.0f;
		}
		if (*hue > 1.0f) {
			*hue -= 1.0f;
		}
	}
}

}
}


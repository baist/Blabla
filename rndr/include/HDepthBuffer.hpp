#if !defined(H_DEPTH_BUFFER_HPP)
#define H_DEPTH_BUFFER_HPP

#include "Prerequisites.hpp"
#include "DepthBuffer.hpp"
#include "Point.hpp"

namespace bge
{
namespace rndr
{

class HDepthBuffer: public DepthBuffer
{
public:
	HDepthBuffer(unsigned int w, unsigned int h);
	virtual ~HDepthBuffer();
	void clear();
	bool testTri(real aZ, real bZ, real cZ);
	bool testPoint(Point point, real z);
	void writeZ(Point point, real newZ);
};

}
}

#endif // H_DEPTH_BUFFER_HPP

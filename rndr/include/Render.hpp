#if !defined (RENDER_HPP)
#define RENDER_HPP

#include <deque>
#include "Prerequisites.hpp"
#include "Texture.hpp"
#include "Point.hpp"
#include "HDepthBuffer.hpp"
#include "VertexBuffer.hpp"
#include "IndexBuffer.hpp"
#include "Rasterizer.hpp"

namespace bge
{
namespace rndr
{

enum CullMode {
	CM_NONE = 0,
	CM_CW,
	CM_CCW
};

const int FVF_POSITION = 1;
const int FVF_NORMAL = 2;
const int FVF_COLOUR = 4;
const int FVF_TEXUV = 8;

struct VisualTri
{
	Vector3d p[3];
	real w[3];
	ColourValue c[3];
	Vector3d n[3];
	Vector2d t[3];
	Vector3d faceNormal;
};

class Render
{
public:
	Render();
	virtual ~Render();
	void begin();
	void end();
	void setDefaultColor(Uint32 color);
	void setCullMode(CullMode cullMode);
	void setFillMode(FillMode fillMode);
	void setWorldMatrix(const Matrix4d * mat);
	void setProjMatrix(const Matrix4d & matr);
	void setTexture(const Texture * tex);
	void setVertexBuffer(VertexBuffer *p_vertBuffer);
	void setIndexBuffer(IndexBuffer *p_indBuffer);
	void drawLine2D(Line2d & ln, ColourValue color);
	void drawTriMesh(unsigned int numPrim);
private:
	typedef std::list<VisualTri> RenderItems;
private:
	unsigned int m_width;
	unsigned int m_height;
	Rasterizer m_rasterizer;
	Uint32 m_defaultColor;
	CullMode m_cullMode;
	FillMode m_fillMode;
	VertexBuffer *m_p_vertBuffer;
	IndexBuffer *m_p_indBuffer;
	const Texture *m_p_texture;
	Matrix4d m_worldMatr;
	Matrix4d m_projMatr;
	RenderItems m_renderItems;
private:
	RenderItems clipTriangleByPlane(const VisualTri & tri, const Plane & plane);
	void _stageView();
	void _stageLighting();
	void _stageBackFaceCulling();
	void _stageProjection();
	void _stageNearPlaneClipping();
	void _stageNormalization();
	void _stageClipping();
	void _stageScreen();
	void _drawTri();
};

}
}

#endif // RENDER_HPP

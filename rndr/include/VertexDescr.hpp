#if !defined(VERTEXDESCR_HPP)
#define VERTEXDESCR_HPP

#include "Prerequisites.hpp"
#include "DataFormat.hpp"

namespace bge
{
namespace rndr
{

class VertexDescr
{
public:
	enum VertexType {
		VT_UNKNOWN = 0,
		VT_POS,
		VT_CLR,
		VT_NORM,
		VT_TEX
	};
public:
	VertexDescr();
	virtual ~VertexDescr();
	unsigned char count() const;
	unsigned int size() const;
	void add(VertexType vertType, DataFormat format);
	VertexType getType(unsigned char index) const;
	DataFormat getFormat(unsigned char index) const;
private:
	struct VertexDescrItem {
		VertexType type;
		DataFormat format;
	};
private:
	std::vector<VertexDescrItem> m_descrs;
};

}
}

#endif // VERTEXDESCR_HPP

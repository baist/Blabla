#if !defined(TEXTURE_HPP)
#define TEXTURE_HPP

#include "Prerequisites.hpp"
#include "Resource.hpp"
#include "Image.hpp"

namespace bge
{
namespace rndr
{

class Texture: public Resource
{
public:
	Texture(const String &name, int getWidth, int height, ImgFormat format);
	virtual ~Texture();
	int getWidth() const;
	int getHeght() const;
	ImgFormat getFormat() const;
	virtual Image *getImage();
	virtual const Image *getImage() const;
protected:
	ImgFormat m_imageFormat;
	unsigned int m_width;
	unsigned int m_height;
private:
	Texture();
	Texture(const Texture &tex);
};

}
}

#endif // TEXTURE_HPP

#if !defined(IMAGEFILE_HPP)
#define IMAGEFILE_HPP

#include "Prerequisites.hpp"
#include "Image.hpp"

namespace bge
{
namespace rndr
{


class ImageFile
{
public:
	ImageFile();
	virtual ~ImageFile();
	size_t getSize() const;
	size_t getWidth() const;
	size_t getHeight() const;
	unsigned int getBPP() const;
	ImgFormat getFormat() const;
	virtual size_t read(void * p_buff, size_t size);
protected:
	size_t m_size;
	size_t m_width;
	size_t m_height;
	unsigned int m_BPP;
	ImgFormat m_format;
};

}
}

#endif // IMAGEFILE_HPP

#if !defined(RSURFACE_HPP)
#define RSURFACE_HPP

#include "Prerequisites.hpp"
#include "Buffer.hpp"

namespace bge
{
namespace rndr
{

enum RSurfFormat
{
	RSF_UNKNOWN = 0,
	RSF_I8,
	RSF_I8I8,
	RSF_I8I8I8,
	RSF_I8I8I8I8,
	RSF_R32,
	RSF_R64
};

class RSurface: public Buffer
{
public:
	static unsigned int computeElemSize(RSurfFormat format);
public:
	RSurface(int width, int height, RSurfFormat format);
	virtual ~RSurface();
	unsigned int getWidth() const;
	unsigned int getHeght() const;
	RSurfFormat getFormat() const;
private:
	RSurfFormat m_format;
	unsigned int m_width;
	unsigned int m_height;
};

}
}

#endif // RSURFACE_HPP

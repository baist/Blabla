#if !defined(TEXTURE_MANAGER_HPP)
#define TEXTURE_MANAGER_HPP

#include "Prerequisites.hpp"
#include "Singleton.hpp"
#include "ResourceManager.hpp"
#include "Texture.hpp"
#include "String.hpp"

namespace bge
{
namespace rndr
{

class TextureManager: public Singleton<TextureManager>, public ResourceManager
{
public:
	TextureManager();
	virtual ~TextureManager();
	virtual Texture * getChessboard();
	virtual Texture* getLena();
	virtual Texture* getTexture(const String &fileName);
private:
};

}
}

#endif // TEXTURE_MANAGER_HPP

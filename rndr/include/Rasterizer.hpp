#if !defined(RASTERIZER_HPP)
#define RASTERIZER_HPP

#include <SDL2/SDL.h>

#include "Prerequisites.hpp"
#include "ColourValue.hpp"
#include "Vector2d.hpp"
#include "Vector3d.hpp"
#include "Point.hpp"
#include "HDepthBuffer.hpp"
#include "Texture.hpp"
#include "Line2d.hpp"

namespace bge
{
namespace rndr
{

enum FillMode {
	FM_POINT = 0,
	FM_WIREFRAME,
	FM_SOLID
};

struct RasterVertex
{
	Vector3d pos;
	real w;
	ColourValue col;
	Vector2d tex;
};

struct RasterTriangle
{
	RasterVertex v[3];
};

struct Prim
{
	Vector3d p[3];
	real w[3];
	Point tp[3];
	ColourValue c[3];
	Vector2d t[3];
};

struct ImageLock
{
	unsigned int pitch;
	const void *p_bits;
};

class Rasterizer
{
public:
	Rasterizer(unsigned int width, unsigned int height);
	virtual ~Rasterizer();
	void clear();
	void update();
	void drawTriangle(const RasterTriangle &tri, const Texture *p_texture);
private:
	typedef struct
	{
		uint8 b;
		uint8 g;
		uint8 r;
		uint8 a;
	} PixelARGB;
	typedef struct
	{
		int x;
		real u;
		real v;
		real z;
		real w;
		ColourValue c;
	} ScanLinePoint;
private:
	unsigned int m_width;
	unsigned int m_height;
	ColourValue m_defaultColor;
	SDL_Window *m_p_SDL_window;
	SDL_Surface *m_p_SDL_screen;
	const Texture *m_p_texture;
	PixelARGB *m_rawPixels;
	HDepthBuffer m_HZBuffer;
private:
	void _stageScreen(Prim &tri);
	int _Clip_Line(int x1, int y1, int x2, int y2,
								 int *ox1, int *oy1, int *ox2, int *oy2);
	void _draw2DLine(const Line2d &ln, ColourValue color);
	void _drawPointedTri();
	void _drawWiredTri(const Prim &tri);
	void _drawFSTri();
	void _drawGSTri(const Prim &tri);
	void _drawGSChunkTri(const Prim *p_tri, unsigned int orient);
	void _drawScanLine(ScanLinePoint a, ScanLinePoint b, int y,
										 PixelARGB *p_dstSurf);
	ColourValue _noneFiltrng(const ImageLock *p_imgLock,
													 const Image * p_img,
													 real u, real v);
	ColourValue _bilnrFiltrng(const ImageLock *p_imgLock,
														const Image *p_img,
														real u, real v);
};

}
}

#endif // RASTERIZER_HPP

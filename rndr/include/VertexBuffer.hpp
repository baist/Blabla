#if !defined(VERTEX_BUFFER_HPP)
#define VERTEX_BUFFER_HPP

#include "Prerequisites.hpp"
#include "Buffer.hpp"
#include "VertexDescr.hpp"

namespace bge
{
namespace rndr
{

class VertexBuffer: public Buffer
{
public:
	VertexBuffer(VertexDescr vertDescr, unsigned int count);
	virtual ~VertexBuffer();
	VertexDescr getVertexDescr() const;
private:
	VertexDescr m_vertDescr;
};

}
}

#endif // VERTEX_BUFFER_HPP

#if !defined(MESH_HPP)
#define MESH_HPP

#include "Prerequisites.hpp"
#include "Resource.hpp"
#include "VertexBuffer.hpp"
#include "IndexBuffer.hpp"

namespace bge
{
namespace rndr
{

class Mesh: public Resource
{
public:
	Mesh(const String &name, VertexBuffer * p_vertBuff,
			 IndexBuffer * p_indBuff, size_t numPrim);
	virtual ~Mesh();
	VertexBuffer * getVertexBuffer() const;
	IndexBuffer * getIndexBuffer() const;
	size_t getNumPrim() const;
private:
	VertexBuffer *m_p_vertBuffer;
	IndexBuffer *m_p_indBuffer;
	size_t m_numPrim;
};

}
}

#endif // MESH_HPP

#if !defined(MESH_MANAGER_HPP)
#define MESH_MANAGER_HPP

#include "Prerequisites.hpp"
#include "Singleton.hpp"
#include "ResourceManager.hpp"
#include "Mesh.hpp"
#include "String.hpp"

namespace bge
{
namespace rndr
{

class MeshManager: public ResourceManager, public Singleton<MeshManager>
{
public:
	MeshManager();
	virtual ~MeshManager();
	virtual Mesh * getMesh(const String & name);
	virtual Mesh * getBox();
	virtual Mesh * getTeapot();
};

}
}

#endif // MESH_MANAGER_HPP

#if !defined(VIEW_FRUSTUM_HPP)
#define VIEW_FRUSTUM_HPP

#include "Prerequisites.hpp"
#include "Vector2d.hpp"
#include "Vector3d.hpp"
#include "Quaternion.hpp"
#include "Matrix4d.hpp"

namespace bge
{
namespace rndr
{

class ViewFrustum
{
public:
	ViewFrustum();
	virtual ~ViewFrustum();
	const Matrix4d& getViewMatrix() const;
	const Matrix4d& getProjectionMatrix() const;
private:
	mutable real m_left;
	mutable real m_right;
	mutable real m_top;
	mutable real m_bottom;
	real m_near;
	real m_far;
	real m_FOV;
	real m_aspect;
	real m_focalLength;
	Quaternion m_orientation;
	Vector3d m_position;
	Vector2d m_frustumOffset;
	mutable bool m_invalidViewMatrix;
	mutable bool m_invalidProjMatrix;
	mutable Matrix4d m_viewMatrix;
	mutable Matrix4d m_projMatrix;
private:
	void updateProjection() const;
};

}
}

#endif // VIEW_FRUSTUM_HPP

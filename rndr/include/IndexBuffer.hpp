#if !defined(INDEX_BUFFER_HPP)
#define INDEX_BUFFER_HPP

#include "Prerequisites.hpp"
#include "Buffer.hpp"

namespace bge
{
namespace rndr
{

class IndexBuffer: public Buffer
{
public:
	static unsigned int sizeOfIndex(char format);
public:
	IndexBuffer(char format, unsigned int count);
	virtual ~IndexBuffer();
};

}
}

#endif // INDEX_BUFFER_HPP

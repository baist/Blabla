#if !defined(IMAGE_HPP)
#define IMAGE_HPP

#include "Prerequisites.hpp"
#include "RSurface.hpp"

namespace bge
{
namespace rndr
{

enum ImgFormat {
	IF_R8G8B8 = 0,
	IF_A8R8G8B8,
	IF_X8R8G8B8
};

class Image: public RSurface
{
public:
	static RSurfFormat convertImgFormat2RSurfFormat(ImgFormat format);
	static unsigned int computeBPP(ImgFormat format);
public:
	explicit Image(int width, int height, ImgFormat format);
	virtual ~Image();
	unsigned int getBPP() const;
private:
	unsigned int m_bpp;
};

}
}

#endif // IMAGE_HPP

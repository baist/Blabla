#if !defined(DEPTH_BUFFER_HPP)
#define DEPTH_BUFFER_HPP

#include "Prerequisites.hpp"
#include "RSurface.hpp"

namespace bge
{
namespace rndr
{

class DepthBuffer: public RSurface
{
public:
	DepthBuffer(unsigned int w, unsigned int h);
	virtual ~DepthBuffer();
	void clear();
};

}
}

#endif // DEPTH_BUFFER_HPP

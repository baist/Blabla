
#include "Stream.hpp"

namespace bge
{
namespace snd
{

Stream::Stream():
	m_pos(0),
	m_size(0),
	m_bitDepth(0),
	m_frequency(0),
	m_numChannels(0),
	m_duration(0.0)
{
}

Stream::~Stream()
{
}

char Stream::getBitDepth() const
{
	return m_bitDepth;
}

int Stream::getFrequency() const
{
	return m_frequency;
}

char Stream::getNumChannels() const
{
	return m_numChannels;
}

size_t Stream::getSize() const
{
	return m_size;
}

double Stream::getDuration() const
{
	return m_duration;
}

size_t Stream::read(void * p_buff, size_t size)
{
	return 0;
}

bool Stream::seek(double time)
{
	return false;
}



}
}

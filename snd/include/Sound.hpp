#if !defined(SOUND_HPP)
#define SOUND_HPP

#include "Prerequisites.hpp"
#include "Resource.hpp"

namespace bge
{
namespace snd
{


class Sound: public Resource
{
public:
	Sound();
	virtual ~Sound();
	Sound(const String & name);
	virtual void play();
	virtual void stop();
};

}
}

#endif // SOUND_HPP

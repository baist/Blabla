#if !defined(STREAM_HPP)
#define STREAM_HPP

#include "Prerequisites.hpp"

namespace bge
{
namespace snd
{


class Stream
{
public:
	Stream();
	virtual ~Stream();
	char getBitDepth() const;
	int getFrequency() const;
	char getNumChannels() const;
	size_t getSize() const;
	double getDuration() const;
	virtual size_t read(void * p_buff, size_t size);
	virtual bool seek(double time);
protected:
	size_t m_pos;
	size_t m_size;
	char m_bitDepth;
	int m_frequency;
	char m_numChannels;
	double m_duration;
};

}
}

#endif // STREAM_HPP

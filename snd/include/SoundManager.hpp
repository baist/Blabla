#if !defined(SOUNDMANAGER_HPP)
#define SOUNDMANAGER_HPP

#include "Prerequisites.hpp"
#include "Singleton.hpp"
#include "ResourceManager.hpp"
#include "Sound.hpp"

namespace bge
{
namespace snd
{


class SoundManager: public ResourceManager, public Singleton<SoundManager>
{
public:
	SoundManager();
	virtual ~SoundManager();
	virtual Sound * newSound(const String & name);
	virtual void update();
};

}
}

#endif // SOUNDMANAGER_HPP

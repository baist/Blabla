cmake_minimum_required(VERSION 2.8)

project(My_rndr)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++03")
set(CMAKE_BUILD_TYPE Debug)

set(CPP_FILES
	./src/MyTexture.cpp
	./src/MyTextureManager.cpp
	./src/lena.cpp
	./src/TGAFile.cpp
	./src/MyMeshManager.cpp
	./src/MyMesh.cpp
	./src/MeshReader.cpp
	./src/OBJReader.cpp
)

set(HPP_FILES
	./include/MyTexture.hpp
	./include/MyTextureManager.hpp
	./include/TGAFile.hpp
	./include/MyMeshManager.hpp
	./include/MyMesh.hpp
	./include/MeshReader.hpp
	./include/OBJReader.hpp
)

include_directories(
	${CMAKE_SOURCE_DIR}/include
	${CMAKE_CURRENT_SOURCE_DIR}/include
	${CMAKE_SOURCE_DIR}/rndr/include
	${PROJECT_BINARY_DIR}/include
)

add_library(My_rndr STATIC ${CPP_FILES} ${HPP_FILES})
target_link_libraries(My_rndr engine pthread)

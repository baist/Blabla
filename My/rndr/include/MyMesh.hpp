#if !defined(MY_MESH_HPP)
#define MY_MESH_HPP

#include "Prerequisites.hpp"
#include "Mesh.hpp"
#include "VertexBuffer.hpp"
#include "IndexBuffer.hpp"

namespace bge
{
namespace rndr
{


class MyMesh: public Mesh
{
public:
	MyMesh(const String &name, VertexBuffer * p_vertBuff,
				 IndexBuffer * p_indBuff, size_t numPrim);
	virtual ~MyMesh();
};

}
}

#endif // MY_MESH_HPP

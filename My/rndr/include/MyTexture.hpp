#if !defined(MY_TEXTURE_HPP)
#define MY_TEXTURE_HPP

#include "Prerequisites.hpp"
#include "Texture.hpp"

namespace bge
{
namespace rndr
{

class MyTexture: public Texture
{
public:
	static unsigned int computeSizeOfTexel(ImgFormat format);
public:
	MyTexture(const String &name,	int width, int height, ImgFormat format);
	virtual ~MyTexture();
	virtual Image *getImage();
	virtual const Image *getImage() const;
private:
	Image *m_imageBuffer;
private:
	MyTexture();
	MyTexture(const MyTexture &tex);
	void _computeSizeOfTexel();
};

}
}

#endif // MY_TEXTURE_HPP

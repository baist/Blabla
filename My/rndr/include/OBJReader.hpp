#if !defined(OBJREADER_HPP)
#define OBJREADER_HPP

#include <fstream>
#include "Prerequisites.hpp"
#include "MeshReader.hpp"

namespace bge
{
namespace rndr
{


class OBJReader: public MeshReader
{
public:
	OBJReader();
	virtual ~OBJReader();
	OBJReader(const String & fileName);
	virtual size_t readPositions(void * p_data, void * p_indcs);
	virtual size_t readTexCoords(void * p_data, void * p_indcs);
	virtual size_t readNormals(void * p_data, void * p_indcs);
private:
	typedef std::list<String> StringList;
private:
	String m_fileName;
	bool m_ok;
	std::ifstream m_ifs;
	std::ifstream::pos_type m_vOffs;
	std::ifstream::pos_type m_vtOffs;
	std::ifstream::pos_type m_vnOffs;
	std::ifstream::pos_type m_fOffs;
private:
	bool openOBJFile();
	bool exploreOBJFile();
	String readOBJLine();
	StringList splitString(String line, char delim = ' ');
	unsigned int strToUInt(String str);
	unsigned short int strToUSInt(String str);
	float strToFloat(String str);
	size_t readOBJV(void * p_data);
	size_t readOBJVT(void * p_data);
	size_t readOBJVN(void * p_data);
	size_t readOBJF(void * p_indcs, unsigned char advance);
	void closeOBJFile();
};

}
}

#endif // OBJREADER_HPP

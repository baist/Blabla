#if !defined(MY_TEXTURE_MANAGER_HPP)
#define MY_TEXTURE_MANAGER_HPP

#include "Prerequisites.hpp"
#include "TextureManager.hpp"

namespace bge
{
namespace rndr
{

class MyTextureManager: public TextureManager
{
public:
	MyTextureManager();
	virtual ~MyTextureManager();
	virtual Texture * getChessboard();
	Texture* getLena();
	Texture* getTexture(const String &name);
protected:
	Resource* createRes(const String &name);
};

}
}

#endif // MY_TEXTURE_MANAGER_HPP

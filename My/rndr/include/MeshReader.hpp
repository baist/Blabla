#if !defined(MESHREADER_HPP)
#define MESHREADER_HPP

#include "Prerequisites.hpp"

namespace bge
{
namespace rndr
{


class MeshReader
{
public:
	MeshReader();
	virtual ~MeshReader();
	size_t getPosCount() const;
	size_t getTexCount() const;
	size_t getNormCount() const;
	size_t getNumPrim() const;
	virtual size_t readPositions(void * p_data, void * p_indcs);
	virtual size_t readTexCoords(void * p_data, void * p_indcs);
	virtual size_t readNormals(void * p_data, void * p_indcs);
protected:
	size_t m_posCount;
	size_t m_texCount;
	size_t m_normCount;
	size_t m_numPrim;
};

}
}

#endif // MESHREADER_HPP

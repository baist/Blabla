#if !defined(TGAFILE_HPP)
#define TGAFILE_HPP

#include <fstream>
#include "String.hpp"
#include "ImageFile.hpp"

namespace bge
{
namespace rndr
{


class TGAFile: public ImageFile
{
public:
	TGAFile();
	virtual ~TGAFile();
	TGAFile(const String & fileName);
	virtual size_t read(void * p_buf, size_t size);
private:
	bool m_ok;
	String m_fileName;
	std::ifstream::pos_type m_dataOffs;
	std::ifstream m_ifs;
private:
	bool openTGAFile();
	size_t readTGAFile(void * p_buf, size_t size);
	void closeTGAFile();
};

}
}

#endif // TGAFILE_HPP

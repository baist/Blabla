#if !defined(MY_MESH_MANAGER_HPP)
#define MY_MESH_MANAGER_HPP

#include "Prerequisites.hpp"
#include "String.hpp"
#include "VertexBuffer.hpp"
#include "IndexBuffer.hpp"
#include "MeshManager.hpp"
#include "Mesh.hpp"
#include "Vector2d.hpp"
#include "Vector3d.hpp"

namespace bge
{
namespace rndr
{


class MyMeshManager: public MeshManager
{
public:
	MyMeshManager();
	virtual ~MyMeshManager();
	virtual Mesh * getMesh(const String & name);
	virtual Mesh * getBox();
	virtual Mesh * getTeapot();
private:
	typedef std::list<VertexBuffer *> VertexBuffers;
	typedef std::list<IndexBuffer *> IndexBuffers;
	struct _Vertex
	{
		Vector3d p;
		unsigned int c;
		Vector2d t;
		Vector3d n;
	};
private:
	VertexBuffers m_vertexBuffs;
	IndexBuffers m_indexBuffs;
private:
	Mesh * makeBox();
	_Vertex calcBezierVertex(Vector3d *points, float u, float v);
	Mesh * makeTeapot(unsigned int segments);
};

}
}

#endif // MY_MESH_MANAGER_HPP

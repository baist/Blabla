
#include <cstdlib>
#include <cstring>
#include "LogManager.hpp"
#include "Vector2d.hpp"
#include "Vector3d.hpp"
#include "MyMesh.hpp"
#include "MyMeshManager.hpp"
#include "OBJReader.hpp"

namespace bge
{
namespace rndr
{

MyMeshManager::MyMeshManager()
{
}

MyMeshManager::~MyMeshManager()
{
	for(IndexBuffers::iterator it = m_indexBuffs.begin();
			it != m_indexBuffs.end(); ++it) {
		delete *it;
	}
	for(VertexBuffers::iterator it = m_vertexBuffs.begin();
			it != m_vertexBuffs.end(); ++it) {
		delete *it;
	}
}

Mesh * MyMeshManager::getMesh(const String & name)
{
	const String newName = generateNewName(name);
	size_t pointPos = name.find_last_of('.');
	const String mashExt = name.substr(pointPos + 1);
	MeshReader * p_meshRdr = 0;
	if(!mashExt.compare("obj")) {
		p_meshRdr = new OBJReader(name);
	}
	else {
		LogManager::LogError() << "unknown file extension \"" << mashExt << "\"";
		return 0;
	}
	const bool hasPos = p_meshRdr->getPosCount() > 0;
	const bool hasTex = p_meshRdr->getTexCount() > 0;
	const bool hasNor = p_meshRdr->getNormCount() > 0;
	VertexDescr vertDescr;
	if(hasPos) {
		vertDescr.add(VertexDescr::VT_UNKNOWN, DF_R32R32R32);
	}
	if(hasTex) {
		vertDescr.add(VertexDescr::VT_UNKNOWN, DF_R32R32);
	}
	if(hasNor) {
		vertDescr.add(VertexDescr::VT_UNKNOWN, DF_R32R32R32);
	}
	const size_t numPrim = p_meshRdr->getNumPrim();
	unsigned char vertexFormat =
			static_cast<unsigned char>(hasPos) * sizeof(float) * 3 +
			static_cast<unsigned char>(hasTex) * sizeof(float) * 2 +
			static_cast<unsigned char>(hasNor) * sizeof(float) * 3;
	const size_t vertexCount = numPrim * 3;
	void * p_temp;
	VertexBuffer *p_vertBuffer = new VertexBuffer(vertDescr, vertexCount);
	m_vertexBuffs.push_back(p_vertBuffer);
	p_vertBuffer->map(&p_temp);
	if(hasPos) {
		char * p_outData = static_cast<char *>(p_temp);
		std::vector<float> data;
		data.resize(vertexCount * 3);
		std::vector<unsigned int> indcs;
		indcs.resize(vertexCount);
		float * p_data = &data[0];
		unsigned int * p_indcs = &indcs[0];
		p_meshRdr->readPositions(p_data, p_indcs);
		for(size_t i = 0; i < vertexCount; ++i) {
			float * p_elem = reinterpret_cast<float *>(p_outData);
			const size_t offs = indcs[i] * 3;
			p_elem[0] = data[offs + 0];
			p_elem[1] = data[offs + 1];
			p_elem[2] = data[offs + 2];
			p_outData += vertexFormat;
		}
	}
	if(hasTex) {
		char * p_outData = static_cast<char *>(p_temp);
		p_outData += sizeof(float) * 3;
		std::vector<float> data;
		data.resize(vertexCount * 2);
		std::vector<unsigned int> indcs;
		indcs.resize(vertexCount);
		float * p_data = &data[0];
		unsigned int * p_indcs = &indcs[0];
		p_meshRdr->readTexCoords(p_data, p_indcs);
		for(size_t i = 0; i < vertexCount; ++i) {
			float * p_elem = reinterpret_cast<float *>(p_outData);
			const size_t offs = indcs[i] * 2;
			p_elem[0] = data[offs + 0];
			p_elem[1] = data[offs + 1];
			p_outData += vertexFormat;
		}
	}
	if(hasNor) {
		char * p_outData = static_cast<char *>(p_temp);
		p_outData += sizeof(float) * 3;
		p_outData += sizeof(float) * 2;
		std::vector<float> data;
		data.resize(vertexCount * 3);
		std::vector<unsigned int> indcs;
		indcs.resize(vertexCount);
		float * p_data = &data[0];
		unsigned int * p_indcs = &indcs[0];
		p_meshRdr->readNormals(p_data, p_indcs);
		for(size_t i = 0; i < vertexCount; ++i) {
			float * p_elem = reinterpret_cast<float *>(p_outData);
			const size_t offs = indcs[i] * 3;
			p_elem[0] = data[offs + 0];
			p_elem[1] = data[offs + 1];
			p_elem[2] = data[offs + 2];
			p_outData += vertexFormat;
		}
	}
	p_vertBuffer->unmap();
	delete p_meshRdr;
	IndexBuffer *p_indBuffer =
			new IndexBuffer(sizeof(unsigned int), vertexCount);
	m_indexBuffs.push_back(p_indBuffer);
	p_indBuffer->map(&p_temp);
	unsigned int * p_outData = static_cast<unsigned int *>(p_temp);
	for(size_t i = 0; i < vertexCount; ++i) {
		p_outData[i] = i;
	}
	p_indBuffer->unmap();
	Mesh * p_mesh = new MyMesh(newName, p_vertBuffer, p_indBuffer, numPrim);
	addRes(p_mesh);
	return p_mesh;
}

Mesh * MyMeshManager::getBox()
{
	Mesh * p_mesh = makeBox();
	addRes(p_mesh);
	return p_mesh;
}

Mesh * MyMeshManager::getTeapot()
{
	const int SEGMENTS = 4;
	Mesh * p_mesh = makeTeapot(SEGMENTS);
	addRes(p_mesh);
	return p_mesh;
}

Mesh * MyMeshManager::makeBox()
{
	const unsigned int primCount = 12;
	struct _Vertex
	{
		Vector3d p;
		unsigned int c;
		Vector2d t;
	}
	exampleVertices[] = {
		Vector3d(-1.0f,  1.0f,  1.0f), 0x0000FFFF, Vector2d(1.0f, 1.0f),
		Vector3d(-1.0f, -1.0f,  1.0f), 0x0000FFFF, Vector2d(0.0f, 1.0f),
		Vector3d( 1.0f, -1.0f,  1.0f), 0x0000FFFF, Vector2d(0.0f, 0.0f),

		Vector3d( 1.0f, -1.0f,  1.0f), 0x0000FFFF, Vector2d(0.0f, 0.0f),
		Vector3d( 1.0f,  1.0f,  1.0f), 0x0000FFFF, Vector2d(1.0f, 0.0f),
		Vector3d(-1.0f,  1.0f,  1.0f), 0x0000FFFF, Vector2d(1.0f, 1.0f),


		Vector3d(-1.0f,  1.0f, -1.0f), 0x00FF00FF, Vector2d(1.0f, 1.0f),
		Vector3d(-1.0f, -1.0f, -1.0f), 0x00FF00FF, Vector2d(0.0f, 1.0f),
		Vector3d(-1.0f, -1.0f,  1.0f), 0x00FF00FF, Vector2d(0.0f, 0.0f),

		Vector3d(-1.0f, -1.0f,  1.0f), 0x00FF00FF, Vector2d(0.0f, 0.0f),
		Vector3d(-1.0f,  1.0f,  1.0f), 0x00FF00FF, Vector2d(1.0f, 0.0f),
		Vector3d(-1.0f,  1.0f, -1.0f), 0x00FF00FF, Vector2d(1.0f, 1.0f),


		Vector3d( 1.0f, -1.0f, -1.0f), 0x00FF0000, Vector2d(1.0f, 1.0f),
		Vector3d(-1.0f, -1.0f, -1.0f), 0x00FF0000, Vector2d(0.0f, 1.0f),
		Vector3d(-1.0f,  1.0f, -1.0f), 0x00FF0000, Vector2d(0.0f, 0.0f),

		Vector3d(-1.0f,  1.0f, -1.0f), 0x00FF0000, Vector2d(0.0f, 0.0f),
		Vector3d( 1.0f,  1.0f, -1.0f), 0x00FF0000, Vector2d(1.0f, 0.0f),
		Vector3d( 1.0f, -1.0f, -1.0f), 0x00FF0000, Vector2d(1.0f, 1.0f),


		Vector3d( 1.0f,  1.0f,  1.0f), 0x000000FF, Vector2d(1.0f, 1.0f),
		Vector3d( 1.0f, -1.0f,  1.0f), 0x000000FF, Vector2d(0.0f, 1.0f),
		Vector3d( 1.0f, -1.0f, -1.0f), 0x000000FF, Vector2d(0.0f, 0.0f),

		Vector3d( 1.0f, -1.0f, -1.0f), 0x000000FF, Vector2d(0.0f, 0.0f),
		Vector3d( 1.0f,  1.0f, -1.0f), 0x000000FF, Vector2d(1.0f, 0.0f),
		Vector3d( 1.0f,  1.0f,  1.0f), 0x000000FF, Vector2d(1.0f, 1.0f),


		Vector3d(-1.0f,  1.0f, -1.0f), 0x0000FF00, Vector2d(1.0f, 1.0f),
		Vector3d(-1.0f,  1.0f,  1.0f), 0x0000FF00, Vector2d(0.0f, 1.0f),
		Vector3d( 1.0f,  1.0f,  1.0f), 0x0000FF00, Vector2d(0.0f, 0.0f),

		Vector3d( 1.0f,  1.0f,  1.0f), 0x0000FF00, Vector2d(0.0f, 0.0f),
		Vector3d( 1.0f,  1.0f, -1.0f), 0x0000FF00, Vector2d(1.0f, 0.0f),
		Vector3d(-1.0f,  1.0f, -1.0f), 0x0000FF00, Vector2d(1.0f, 1.0f),


		Vector3d( 1.0f, -1.0f,  1.0f), 0x00FFFF00, Vector2d(1.0f, 1.0f),
		Vector3d(-1.0f, -1.0f,  1.0f), 0x00FFFF00, Vector2d(0.0f, 1.0f),
		Vector3d(-1.0f, -1.0f, -1.0f), 0x00FFFF00, Vector2d(0.0f, 0.0f),

		Vector3d(-1.0f, -1.0f, -1.0f), 0x00FFFF00, Vector2d(0.0f, 0.0f),
		Vector3d( 1.0f, -1.0f, -1.0f), 0x00FFFF00, Vector2d(1.0f, 0.0f),
		Vector3d( 1.0f, -1.0f,  1.0f), 0x00FFFF00, Vector2d(1.0f, 1.0f)
	};
	unsigned int exampleIndices[] = {
		0,  1,  2,
		3,  4,  5,
		6,  7,  8,
		9,  10, 11,
		12, 13, 14,
		15, 16, 17,
		18, 19, 20,
		21, 22, 23,
		24, 25, 26,
		27, 28, 29,
		30, 31, 32,
		33, 34, 35
	};
	void *p_temp;
	VertexDescr vertDescr;
	vertDescr.add(VertexDescr::VT_UNKNOWN, DF_R32R32R32);
	vertDescr.add(VertexDescr::VT_UNKNOWN, DF_I8I8I8I8);
	vertDescr.add(VertexDescr::VT_UNKNOWN, DF_R32R32);
	VertexBuffer *p_vertBuffer = new VertexBuffer(vertDescr, primCount * 3);
	m_vertexBuffs.push_back(p_vertBuffer);
	p_vertBuffer->map(&p_temp);
	std::memcpy(p_temp, exampleVertices, sizeof(exampleVertices));
	p_vertBuffer->unmap();
	IndexBuffer *p_indBuffer =
			new IndexBuffer(sizeof(unsigned int), primCount * 3);
	m_indexBuffs.push_back(p_indBuffer);
	p_indBuffer->map(&p_temp);
	std::memcpy(p_temp, exampleIndices, sizeof(exampleIndices));
	p_indBuffer->unmap();
	Mesh * p_mesh = new MyMesh("box", p_vertBuffer, p_indBuffer, primCount);
	return p_mesh;
}

MyMeshManager::_Vertex MyMeshManager::calcBezierVertex(
		Vector3d *points, float u, float v)
{
	// first of all we will need to evaluate 4 curves in the u
	// direction. The points from those will be stored in this
	// temporary arrays
	Vector3d temp0[4];
	Vector3d temp1[4];
	//
	// Given our 16 control points, we can imagine 4 curves travelling
	// in a given surface direction. For example, if the parametric u
	// value was 0.5, we could imagine evaluating 4 seperate curves
	// at u=0.5.
	//
	// This function is basically here to perform that very task. each
	// row of 4 points in the u-direction is evaluated to provide us
	// with 4 new points. These new points then form a curve we can
	// evaluate in the v direction to calculate our final output point.
	//
	for(int i = 0; i < 4; ++i) {
		unsigned int indx = i * 4;
		Vector3d tempPoints0[] = {
			points[indx + 0], points[indx + 1], points[indx + 2], points[indx + 3]
		};
		Vector3d tempPoints1[] = {
			points[i + 0], points[i + 4], points[i + 8], points[i + 12]
		};
		temp0[i] = Math::calcBernstein(tempPoints0, u);
		temp1[i] = Math::calcBernstein(tempPoints1, v);
	}
	Vector3d p0 = Math::calcDerivBernstein(temp1, u);
	Vector3d p1 = Math::calcDerivBernstein(temp0, v);
	//
	// Having generated 4 points in the u direction, we need to
	// use those points to generate the final point on the surface
	// by calculating a final bezier curve in the v direction.
	//     This function takes the temporary points and generates
	// the final point for the rendered surface
	// having got 4 points, we can use it as a bezier curve
	// to calculate the v direction. This should give us our
	// final point
	//
	_Vertex vert;
	vert.p = Math::calcBernstein(temp0, v);
	vert.t = Vector2d(u, v);
	vert.n = -p0.crossProduct(p1).getNormalised();
	return vert;
}

Mesh * MyMeshManager::makeTeapot(unsigned int segments)
{
	Vector3d points[] = {
		Vector3d(0, 0, 0),
		Vector3d(1.4, 2.4, 0),
		Vector3d(1.4, 2.4, -0.784),
		Vector3d(0.784, 2.4, -1.4),
		Vector3d(0, 2.4, -1.4),
		Vector3d(1.3375, 2.53125, 0),
		Vector3d(1.3375, 2.53125, -0.749),
		Vector3d(0.749, 2.53125, -1.3375),
		Vector3d(0, 2.53125, -1.3375),
		Vector3d(1.4375, 2.53125, 0),
		Vector3d(1.4375, 2.53125, -0.805),
		Vector3d(0.805, 2.53125, -1.4375),
		Vector3d(0, 2.53125, -1.4375),
		Vector3d(1.5, 2.4, 0),
		Vector3d(1.5, 2.4, -0.84),
		Vector3d(0.84, 2.4, -1.5),
		Vector3d(0, 2.4, -1.5),
		Vector3d(-0.784, 2.4, -1.4),
		Vector3d(-1.4, 2.4, -0.784),
		Vector3d(-1.4, 2.4, 0),
		Vector3d(-0.749, 2.53125, -1.3375),
		Vector3d(-1.3375, 2.53125, -0.749),
		Vector3d(-1.3375, 2.53125, 0),
		Vector3d(-0.805, 2.53125, -1.4375),
		Vector3d(-1.4375, 2.53125, -0.805),
		Vector3d(-1.4375, 2.53125, 0),
		Vector3d(-0.84, 2.4, -1.5),
		Vector3d(-1.5, 2.4, -0.84),
		Vector3d(-1.5, 2.4, 0),
		Vector3d(-1.4, 2.4, 0.784),
		Vector3d(-0.784, 2.4, 1.4),
		Vector3d(0, 2.4, 1.4),
		Vector3d(-1.3375, 2.53125, 0.749),
		Vector3d(-0.749, 2.53125, 1.3375),
		Vector3d(0, 2.53125, 1.3375),
		Vector3d(-1.4375, 2.53125, 0.805),
		Vector3d(-0.805, 2.53125, 1.4375),
		Vector3d(0, 2.53125, 1.4375),
		Vector3d(-1.5, 2.4, 0.84),
		Vector3d(-0.84, 2.4, 1.5),
		Vector3d(0, 2.4, 1.5),
		Vector3d(0.784, 2.4, 1.4),
		Vector3d(1.4, 2.4, 0.784),
		Vector3d(0.749, 2.53125, 1.3375),
		Vector3d(1.3375, 2.53125, 0.749),
		Vector3d(0.805, 2.53125, 1.4375),
		Vector3d(1.4375, 2.53125, 0.805),
		Vector3d(0.84, 2.4, 1.5),
		Vector3d(1.5, 2.4, 0.84),
		Vector3d(1.75, 1.875, 0),
		Vector3d(1.75, 1.875, -0.98),
		Vector3d(0.98, 1.875, -1.75),
		Vector3d(0, 1.875, -1.75),
		Vector3d(2, 1.35, 0),
		Vector3d(2, 1.35, -1.12),
		Vector3d(1.12, 1.35, -2),
		Vector3d(0, 1.35, -2),
		Vector3d(2, 0.9, 0),
		Vector3d(2, 0.9, -1.12),
		Vector3d(1.12, 0.9, -2),
		Vector3d(0, 0.9, -2),
		Vector3d(-0.98, 1.875, -1.75),
		Vector3d(-1.75, 1.875, -0.98),
		Vector3d(-1.75, 1.875, 0),
		Vector3d(-1.12, 1.35, -2),
		Vector3d(-2, 1.35, -1.12),
		Vector3d(-2, 1.35, 0),
		Vector3d(-1.12, 0.9, -2),
		Vector3d(-2, 0.9, -1.12),
		Vector3d(-2, 0.9, 0),
		Vector3d(-1.75, 1.875, 0.98),
		Vector3d(-0.98, 1.875, 1.75),
		Vector3d(0, 1.875, 1.75),
		Vector3d(-2, 1.35, 1.12),
		Vector3d(-1.12, 1.35, 2),
		Vector3d(0, 1.35, 2),
		Vector3d(-2, 0.9, 1.12),
		Vector3d(-1.12, 0.9, 2),
		Vector3d(0, 0.9, 2),
		Vector3d(0.98, 1.875, 1.75),
		Vector3d(1.75, 1.875, 0.98),
		Vector3d(1.12, 1.35, 2),
		Vector3d(2, 1.35, 1.12),
		Vector3d(1.12, 0.9, 2),
		Vector3d(2, 0.9, 1.12),
		Vector3d(2, 0.45, 0),
		Vector3d(2, 0.45, -1.12),
		Vector3d(1.12, 0.45, -2),
		Vector3d(0, 0.45, -2),
		Vector3d(1.5, 0.225, 0),
		Vector3d(1.5, 0.225, -0.84),
		Vector3d(0.84, 0.225, -1.5),
		Vector3d(0, 0.225, -1.5),
		Vector3d(1.5, 0.15, 0),
		Vector3d(1.5, 0.15, -0.84),
		Vector3d(0.84, 0.15, -1.5),
		Vector3d(0, 0.15, -1.5),
		Vector3d(-1.12, 0.45, -2),
		Vector3d(-2, 0.45, -1.12),
		Vector3d(-2, 0.45, 0),
		Vector3d(-0.84, 0.225, -1.5),
		Vector3d(-1.5, 0.225, -0.84),
		Vector3d(-1.5, 0.225, 0),
		Vector3d(-0.84, 0.15, -1.5),
		Vector3d(-1.5, 0.15, -0.84),
		Vector3d(-1.5, 0.15, 0),
		Vector3d(-2, 0.45, 1.12),
		Vector3d(-1.12, 0.45, 2),
		Vector3d(0, 0.45, 2),
		Vector3d(-1.5, 0.225, 0.84),
		Vector3d(-0.84, 0.225, 1.5),
		Vector3d(0, 0.225, 1.5),
		Vector3d(-1.5, 0.15, 0.84),
		Vector3d(-0.84, 0.15, 1.5),
		Vector3d(0, 0.15, 1.5),
		Vector3d(1.12, 0.45, 2),
		Vector3d(2, 0.45, 1.12),
		Vector3d(0.84, 0.225, 1.5),
		Vector3d(1.5, 0.225, 0.84),
		Vector3d(0.84, 0.15, 1.5),
		Vector3d(1.5, 0.15, 0.84),
		Vector3d(-1.6, 2.025, 0),
		Vector3d(-1.6, 2.025, -0.3),
		Vector3d(-1.5, 2.25, -0.3),
		Vector3d(-1.5, 2.25, 0),
		Vector3d(-2.3, 2.025, 0),
		Vector3d(-2.3, 2.025, -0.3),
		Vector3d(-2.5, 2.25, -0.3),
		Vector3d(-2.5, 2.25, 0),
		Vector3d(-2.7, 2.025, 0),
		Vector3d(-2.7, 2.025, -0.3),
		Vector3d(-3, 2.25, -0.3),
		Vector3d(-3, 2.25, 0),
		Vector3d(-2.7, 1.8, 0),
		Vector3d(-2.7, 1.8, -0.3),
		Vector3d(-3, 1.8, -0.3),
		Vector3d(-3, 1.8, 0),
		Vector3d(-1.5, 2.25, 0.3),
		Vector3d(-1.6, 2.025, 0.3),
		Vector3d(-2.5, 2.25, 0.3),
		Vector3d(-2.3, 2.025, 0.3),
		Vector3d(-3, 2.25, 0.3),
		Vector3d(-2.7, 2.025, 0.3),
		Vector3d(-3, 1.8, 0.3),
		Vector3d(-2.7, 1.8, 0.3),
		Vector3d(-2.7, 1.575, 0),
		Vector3d(-2.7, 1.575, -0.3),
		Vector3d(-3, 1.35, -0.3),
		Vector3d(-3, 1.35, 0),
		Vector3d(-2.5, 1.125, 0),
		Vector3d(-2.5, 1.125, -0.3),
		Vector3d(-2.65, 0.9375, -0.3),
		Vector3d(-2.65, 0.9375, 0),
		Vector3d(-2, 0.9, -0.3),
		Vector3d(-1.9, 0.6, -0.3),
		Vector3d(-1.9, 0.6, 0),
		Vector3d(-3, 1.35, 0.3),
		Vector3d(-2.7, 1.575, 0.3),
		Vector3d(-2.65, 0.9375, 0.3),
		Vector3d(-2.5, 1.125, 0.3),
		Vector3d(-1.9, 0.6, 0.3),
		Vector3d(-2, 0.9, 0.3),
		Vector3d(1.7, 1.425, 0),
		Vector3d(1.7, 1.425, -0.66),
		Vector3d(1.7, 0.6, -0.66),
		Vector3d(1.7, 0.6, 0),
		Vector3d(2.6, 1.425, 0),
		Vector3d(2.6, 1.425, -0.66),
		Vector3d(3.1, 0.825, -0.66),
		Vector3d(3.1, 0.825, 0),
		Vector3d(2.3, 2.1, 0),
		Vector3d(2.3, 2.1, -0.25),
		Vector3d(2.4, 2.025, -0.25),
		Vector3d(2.4, 2.025, 0),
		Vector3d(2.7, 2.4, 0),
		Vector3d(2.7, 2.4, -0.25),
		Vector3d(3.3, 2.4, -0.25),
		Vector3d(3.3, 2.4, 0),
		Vector3d(1.7, 0.6, 0.66),
		Vector3d(1.7, 1.425, 0.66),
		Vector3d(3.1, 0.825, 0.66),
		Vector3d(2.6, 1.425, 0.66),
		Vector3d(2.4, 2.025, 0.25),
		Vector3d(2.3, 2.1, 0.25),
		Vector3d(3.3, 2.4, 0.25),
		Vector3d(2.7, 2.4, 0.25),
		Vector3d(2.8, 2.475, 0),
		Vector3d(2.8, 2.475, -0.25),
		Vector3d(3.525, 2.49375, -0.25),
		Vector3d(3.525, 2.49375, 0),
		Vector3d(2.9, 2.475, 0),
		Vector3d(2.9, 2.475, -0.15),
		Vector3d(3.45, 2.5125, -0.15),
		Vector3d(3.45, 2.5125, 0),
		Vector3d(2.8, 2.4, 0),
		Vector3d(2.8, 2.4, -0.15),
		Vector3d(3.2, 2.4, -0.15),
		Vector3d(3.2, 2.4, 0),
		Vector3d(3.525, 2.49375, 0.25),
		Vector3d(2.8, 2.475, 0.25),
		Vector3d(3.45, 2.5125, 0.15),
		Vector3d(2.9, 2.475, 0.15),
		Vector3d(3.2, 2.4, 0.15),
		Vector3d(2.8, 2.4, 0.15),
		Vector3d(0, 3.15, 0),
		Vector3d(0, 3.15, -0.002),
		Vector3d(0.002, 3.15, 0),
		Vector3d(0.8, 3.15, 0),
		Vector3d(0.8, 3.15, -0.45),
		Vector3d(0.45, 3.15, -0.8),
		Vector3d(0, 3.15, -0.8),
		Vector3d(0, 2.85, 0),
		Vector3d(0.2, 2.7, 0),
		Vector3d(0.2, 2.7, -0.112),
		Vector3d(0.112, 2.7, -0.2),
		Vector3d(0, 2.7, -0.2),
		Vector3d(-0.002, 3.15, 0),
		Vector3d(-0.45, 3.15, -0.8),
		Vector3d(-0.8, 3.15, -0.45),
		Vector3d(-0.8, 3.15, 0),
		Vector3d(-0.112, 2.7, -0.2),
		Vector3d(-0.2, 2.7, -0.112),
		Vector3d(-0.2, 2.7, 0),
		Vector3d(0, 3.15, 0.002),
		Vector3d(-0.8, 3.15, 0.45),
		Vector3d(-0.45, 3.15, 0.8),
		Vector3d(0, 3.15, 0.8),
		Vector3d(-0.2, 2.7, 0.112),
		Vector3d(-0.112, 2.7, 0.2),
		Vector3d(0, 2.7, 0.2),
		Vector3d(0.45, 3.15, 0.8),
		Vector3d(0.8, 3.15, 0.45),
		Vector3d(0.112, 2.7, 0.2),
		Vector3d(0.2, 2.7, 0.112),
		Vector3d(0.4, 2.55, 0),
		Vector3d(0.4, 2.55, -0.224),
		Vector3d(0.224, 2.55, -0.4),
		Vector3d(0, 2.55, -0.4),
		Vector3d(1.3, 2.55, 0),
		Vector3d(1.3, 2.55, -0.728),
		Vector3d(0.728, 2.55, -1.3),
		Vector3d(0, 2.55, -1.3),
		Vector3d(1.3, 2.4, 0),
		Vector3d(1.3, 2.4, -0.728),
		Vector3d(0.728, 2.4, -1.3),
		Vector3d(0, 2.4, -1.3),
		Vector3d(-0.224, 2.55, -0.4),
		Vector3d(-0.4, 2.55, -0.224),
		Vector3d(-0.4, 2.55, 0),
		Vector3d(-0.728, 2.55, -1.3),
		Vector3d(-1.3, 2.55, -0.728),
		Vector3d(-1.3, 2.55, 0),
		Vector3d(-0.728, 2.4, -1.3),
		Vector3d(-1.3, 2.4, -0.728),
		Vector3d(-1.3, 2.4, 0),
		Vector3d(-0.4, 2.55, 0.224),
		Vector3d(-0.224, 2.55, 0.4),
		Vector3d(0, 2.55, 0.4),
		Vector3d(-1.3, 2.55, 0.728),
		Vector3d(-0.728, 2.55, 1.3),
		Vector3d(0, 2.55, 1.3),
		Vector3d(-1.3, 2.4, 0.728),
		Vector3d(-0.728, 2.4, 1.3),
		Vector3d(0, 2.4, 1.3),
		Vector3d(0.224, 2.55, 0.4),
		Vector3d(0.4, 2.55, 0.224),
		Vector3d(0.728, 2.55, 1.3),
		Vector3d(1.3, 2.55, 0.728),
		Vector3d(0.728, 2.4, 1.3),
		Vector3d(1.3, 2.4, 0.728),
		Vector3d(0, 0, 0),
		Vector3d(1.5, 0.15, 0),
		Vector3d(1.5, 0.15, 0.84),
		Vector3d(0.84, 0.15, 1.5),
		Vector3d(0, 0.15, 1.5),
		Vector3d(1.5, 0.075, 0),
		Vector3d(1.5, 0.075, 0.84),
		Vector3d(0.84, 0.075, 1.5),
		Vector3d(0, 0.075, 1.5),
		Vector3d(1.425, 0, 0),
		Vector3d(1.425, 0, 0.798),
		Vector3d(0.798, 0, 1.425),
		Vector3d(0, 0, 1.425),
		Vector3d(-0.84, 0.15, 1.5),
		Vector3d(-1.5, 0.15, 0.84),
		Vector3d(-1.5, 0.15, 0),
		Vector3d(-0.84, 0.075, 1.5),
		Vector3d(-1.5, 0.075, 0.84),
		Vector3d(-1.5, 0.075, 0),
		Vector3d(-0.798, 0, 1.425),
		Vector3d(-1.425, 0, 0.798),
		Vector3d(-1.425, 0, 0),
		Vector3d(-1.5, 0.15, -0.84),
		Vector3d(-0.84, 0.15, -1.5),
		Vector3d(0, 0.15, -1.5),
		Vector3d(-1.5, 0.075, -0.84),
		Vector3d(-0.84, 0.075, -1.5),
		Vector3d(0, 0.075, -1.5),
		Vector3d(-1.425, 0, -0.798),
		Vector3d(-0.798, 0, -1.425),
		Vector3d(0, 0, -1.425),
		Vector3d(0.84, 0.15, -1.5),
		Vector3d(1.5, 0.15, -0.84),
		Vector3d(0.84, 0.075, -1.5),
		Vector3d(1.5, 0.075, -0.84),
		Vector3d(0.798, 0, -1.425),
		Vector3d(1.425, 0, -0.798)
	};
	unsigned int indeces[] = {
		1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,
		4, 17, 18, 19, 8, 20, 21, 22, 12, 23, 24, 25, 16, 26, 27, 28,
		19, 29, 30, 31, 22, 32, 33, 34, 25, 35, 36, 37, 28, 38, 39, 40,
		31, 41, 42, 1, 34, 43, 44, 5, 37, 45, 46, 9, 40, 47, 48, 13,
		13, 14, 15, 16, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60,
		16, 26, 27, 28, 52, 61, 62, 63, 56, 64, 65, 66, 60, 67, 68, 69,
		28, 38, 39, 40, 63, 70, 71, 72, 66, 73, 74, 75, 69, 76, 77, 78,
		40, 47, 48, 13, 72, 79, 80, 49, 75, 81, 82, 53, 78, 83, 84, 57,
		57, 58, 59, 60, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96,
		60, 67, 68, 69, 88, 97, 98, 99, 92, 100, 101, 102, 96, 103, 104, 105,
		69, 76, 77, 78, 99, 106, 107, 108, 102, 109, 110, 111, 105, 112, 113, 114,
		78, 83, 84, 57, 108, 115, 116, 85, 111, 117, 118, 89, 114, 119, 120, 93,
		121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136,
		124, 137, 138, 121, 128, 139, 140, 125, 132, 141, 142, 129, 136, 143, 144, 133,
		133, 134, 135, 136, 145, 146, 147, 148, 149, 150, 151, 152, 69, 153, 154, 155,
		136, 143, 144, 133, 148, 156, 157, 145, 152, 158, 159, 149, 155, 160, 161, 69,
		162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177,
		165, 178, 179, 162, 169, 180, 181, 166, 173, 182, 183, 170, 177, 184, 185, 174,
		174, 175, 176, 177, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197,
		177, 184, 185, 174, 189, 198, 199, 186, 193, 200, 201, 190, 197, 202, 203, 194,
		204, 204, 204, 204, 207, 208, 209, 210, 211, 211, 211, 211, 212, 213, 214, 215,
		204, 204, 204, 204, 210, 217, 218, 219, 211, 211, 211, 211, 215, 220, 221, 222,
		204, 204, 204, 204, 219, 224, 225, 226, 211, 211, 211, 211, 222, 227, 228, 229,
		204, 204, 204, 204, 226, 230, 231, 207, 211, 211, 211, 211, 229, 232, 233, 212,
		212, 213, 214, 215, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245,
		215, 220, 221, 222, 237, 246, 247, 248, 241, 249, 250, 251, 245, 252, 253, 254,
		222, 227, 228, 229, 248, 255, 256, 257, 251, 258, 259, 260, 254, 261, 262, 263,
		229, 232, 233, 212, 257, 264, 265, 234, 260, 266, 267, 238, 263, 268, 269, 242,
		270, 270, 270, 270, 279, 280, 281, 282, 275, 276, 277, 278, 271, 272, 273, 274,
		270, 270, 270, 270, 282, 289, 290, 291, 278, 286, 287, 288, 274, 283, 284, 285,
		270, 270, 270, 270, 291, 298, 299, 300, 288, 295, 296, 297, 285, 292, 293, 294,
		270, 270, 270, 270, 300, 305, 306, 279, 297, 303, 304, 275, 294, 301, 302, 271
	};
	const unsigned int CNT = 32;
	const unsigned int PEAKS = segments + 1;
	const real dU = 1.0f / segments;
	const real dV = 1.0f / segments;
	VertexDescr vertDescr;
	vertDescr.add(VertexDescr::VT_UNKNOWN, DF_R32R32R32);
	vertDescr.add(VertexDescr::VT_UNKNOWN, DF_I8I8I8I8);
	vertDescr.add(VertexDescr::VT_UNKNOWN, DF_R32R32);
	vertDescr.add(VertexDescr::VT_UNKNOWN, DF_R32R32R32);
	VertexBuffer *p_vertBuffer =
			new VertexBuffer(vertDescr, PEAKS * PEAKS * CNT);
	IndexBuffer *p_indBuffer =
			new IndexBuffer(sizeof(unsigned int), segments * segments * CNT * 6);
	_Vertex *p_v;
	unsigned int *p_i;
	p_vertBuffer->map(reinterpret_cast<void**>(&p_v));
	p_indBuffer->map(reinterpret_cast<void**>(&p_i));
	for(unsigned int l = 0; l < CNT; ++l) {
		Vector3d tempPoints[] = {
			points[indeces[16 * l + 0]],  points[indeces[16 * l + 1]],
			points[indeces[16 * l + 2]],  points[indeces[16 * l + 3]],
			points[indeces[16 * l + 4]],  points[indeces[16 * l + 5]],
			points[indeces[16 * l + 6]],  points[indeces[16 * l + 7]],
			points[indeces[16 * l + 8]],  points[indeces[16 * l + 9]],
			points[indeces[16 * l + 10]], points[indeces[16 * l + 11]],
			points[indeces[16 * l + 12]], points[indeces[16 * l + 13]],
			points[indeces[16 * l + 14]], points[indeces[16 * l + 15]],
		};
		real tV = 0.0f;
		int clr = rand() % 0x00FFFFFF;
		for(unsigned int j = 0; j < segments; ++j) {
			real tU = 0.0f;
			for(unsigned int i = 0; i < segments; ++i) {
				// calculate the point on the surface
				const unsigned int aIndx = (l * PEAKS + j) * PEAKS + i;
				const unsigned int bIndx = (l * PEAKS + j) * PEAKS + (i + 1);
				const unsigned int cIndx = (l * PEAKS + (j+1)) * PEAKS + i;
				const unsigned int dIndx = (l * PEAKS + (j+1)) * PEAKS + (i + 1);
				if((i == 0) && (j == 0)) {
					p_v[aIndx] = calcBezierVertex(tempPoints,
						static_cast<real>(i) / segments,
						static_cast<real>(j) / segments);
					p_v[aIndx].c = clr;
				}
				if(j == 0) {
					p_v[bIndx] = calcBezierVertex(tempPoints,
						static_cast<real>(i + 1) / segments,
						static_cast<real>(j) / segments);
					p_v[bIndx].c = clr;
				}
				if(i == 0) {
					p_v[cIndx] = calcBezierVertex(tempPoints,
						static_cast<real>(i) / segments,
						static_cast<real>(j + 1) / segments);
					p_v[cIndx].c = clr;
				}
				{
					p_v[dIndx] = calcBezierVertex(tempPoints,
						static_cast<real>(i + 1) / segments,
						static_cast<real>(j + 1) / segments);
					p_v[dIndx].c = clr;
				}
				int index = ((l * segments + j) * segments + i) * 6;
				p_i[index + 0] = cIndx;
				p_i[index + 1] = bIndx;
				p_i[index + 2] = aIndx;
				p_i[index + 3] = dIndx;
				p_i[index + 4] = bIndx;
				p_i[index + 5] = cIndx;
				tU += dU;
			}
			tV += dV;
		}
	}
	p_indBuffer->unmap();
	p_vertBuffer->unmap();
	m_vertexBuffs.push_back(p_vertBuffer);
	m_indexBuffs.push_back(p_indBuffer);
	Mesh *p_mesh =
			new MyMesh("teapot", p_vertBuffer, p_indBuffer,
								 segments * segments * CNT * 2);
	return p_mesh;
}


}
}

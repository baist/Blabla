#include "LogManager.hpp"
#include "ColourValue.hpp"
#include "ImageFile.hpp"
#include "MyTextureManager.hpp"
#include "MyTexture.hpp"
#include "TGAFile.hpp"

#include "lena.cpp"

namespace bge
{
namespace rndr
{

MyTextureManager::MyTextureManager()
{
}

MyTextureManager::~MyTextureManager()
{
}

Texture * MyTextureManager::getChessboard()
{
	const unsigned int width = 512;
	const unsigned int height = 512;
	const unsigned int bpp = 4;
	const ImgFormat format = IF_X8R8G8B8;
	Texture *p_texture = new MyTexture("chessdesk", width, height, format);
	Image *p_imgBuff = p_texture->getImage();
	char *p_imgBits;
	p_imgBuff->map(reinterpret_cast<void **>(&p_imgBits));
	ColourValue clr;
	for(int j = 0; j < (height); ++j) {
		if((j % 32) == 0) {
			clr = ColourValue::White - clr;
		}
		for(int i = 0; i < (width); ++i) {
			if((i % 32) == 0) {
				clr = ColourValue::White - clr;
			}
			const size_t off = (j * width + i) * bpp;
			p_imgBits[off + 0] = clr.getB();
			p_imgBits[off + 1] = clr.getG();
			p_imgBits[off + 2] = clr.getR();
			p_imgBits[off + 3] = 0;
		}
	}
	p_imgBuff->unmap();
	addRes(p_texture);
	return p_texture;
}

Texture *MyTextureManager::getLena()
{
	const unsigned int width = gimp_image.width;
	const unsigned int height = gimp_image.height;
	const unsigned int bpp = gimp_image.bytes_per_pixel;
	const ImgFormat format = IF_X8R8G8B8;
	Texture *p_texture = new MyTexture("lena", width, height, format);
	Image *p_imgBuff = p_texture->getImage();
	char *p_imgBits;
	p_imgBuff->map(reinterpret_cast<void **>(&p_imgBits));
	for(int j = 0; j < (height); ++j) {
		for(int i = 0; i < (width); ++i) {
			const size_t off = (j * width + i) * bpp;
			p_imgBits[off + 0] = gimp_image.pixel_data[off + 2];
			p_imgBits[off + 1] = gimp_image.pixel_data[off + 1];
			p_imgBits[off + 2] = gimp_image.pixel_data[off + 0];
			p_imgBits[off + 3] = 0x00;
		}
	}
	p_imgBuff->unmap();

	return p_texture;
}

Texture *MyTextureManager::getTexture(const String &name)
{
	const String newName = generateNewName(name);
	size_t pointPos = name.find_last_of('.');
	const String imageExt = name.substr(pointPos + 1);
	ImageFile * p_imgFile;
	if(!imageExt.compare("tga")) {
		p_imgFile = new TGAFile(name);
	}
	else {
		LogManager::LogError() << "unknown file extension \"" << imageExt << "\"";
		return 0;
	}
	const size_t width = p_imgFile->getWidth();
	const size_t height = p_imgFile->getHeight();
	const unsigned int bpp = p_imgFile->getBPP();
	Texture *p_texture = new MyTexture(newName, width, height,
																		 p_imgFile->getFormat());
	Image *p_imgBuff = p_texture->getImage();
	char *p_imgBits;
	p_imgBuff->map(reinterpret_cast<void **>(&p_imgBits));
	const size_t size = p_imgFile->getSize();
	std::vector<char> bufferData;
	bufferData.resize(size);
	char * p_data = &bufferData[0];
	p_imgFile->read(p_data, size);
	for(int j = 0; j < (height); ++j) {
		for(int i = 0; i < (width); ++i) {
			const size_t off = (j * width + i) * bpp;
			p_imgBits[off + 0] = p_data[off + 0];
			p_imgBits[off + 1] = p_data[off + 1];
			p_imgBits[off + 2] = p_data[off + 2];
			p_imgBits[off + 3] = 0x00;
		}
	}
	p_imgBuff->unmap();
	addRes(p_texture);
	return p_texture;
}

Resource *MyTextureManager::createRes(const String &name)
{
	return 0;
}

}
}

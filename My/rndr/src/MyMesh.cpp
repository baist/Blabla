
#include "MyMesh.hpp"

namespace bge
{
namespace rndr
{

MyMesh::MyMesh(const String & name, VertexBuffer * p_vertBuff,
							 IndexBuffer * p_indBuff, size_t numPrim):
	Mesh(name, p_vertBuff, p_indBuff, numPrim)
{
}

MyMesh::~MyMesh()
{
}



}
}


#include <algorithm>
#include "LogManager.hpp"
#include "OBJReader.hpp"

namespace bge
{
namespace rndr
{

OBJReader::OBJReader()
{
}

OBJReader::~OBJReader()
{
}

OBJReader::OBJReader(const String & fileName):
	m_fileName(fileName)
{
	m_ok = true;
	if(!openOBJFile()) {
		m_ok = false;
		return;
	}
	if(!exploreOBJFile()) {
		m_ok = false;
	}
	closeOBJFile();
}

size_t OBJReader::readPositions(void * p_data, void * p_indcs)
{
	if(!m_ok) {
		return 0;
	}
	if(!openOBJFile()) {
		m_ok = false;
		return 0;
	}
	size_t res = readOBJV(p_data);
	if(res == 0) {
		m_ok = false;
	}
	res = readOBJF(p_indcs, 0);
	if(res == 0) {
		m_ok = false;
	}
	closeOBJFile();
	return res;
}

size_t OBJReader::readTexCoords(void * p_data, void * p_indcs)
{
	if(!m_ok) {
		return 0;
	}
	if(!openOBJFile()) {
		m_ok = false;
		return 0;
	}
	size_t res = readOBJVT(p_data);
	if(res == 0) {
		m_ok = false;
	}
	res = readOBJF(p_indcs, 1);
	if(res == 0) {
		m_ok = false;
	}
	closeOBJFile();
	return res;
}

size_t OBJReader::readNormals(void * p_data, void * p_indcs)
{
	if(!m_ok) {
		return 0;
	}
	if(!openOBJFile()) {
		m_ok = false;
		return 0;
	}
	size_t res = readOBJVN(p_data);
	if(res == 0) {
		m_ok = false;
	}
	res = readOBJF(p_indcs, 2);
	if(res == 0) {
		m_ok = false;
	}
	closeOBJFile();
	return res;
}

bool OBJReader::openOBJFile()
{
	m_ifs.open(m_fileName.c_str());
	if(!m_ifs.good()) {
		return false;
	}
	return true;
}

bool OBJReader::exploreOBJFile()
{
	m_posCount = 0;
	m_texCount = 0;
	m_normCount = 0;
	m_numPrim = 0;
	for(;;) {
		const std::ifstream::pos_type strmPos = m_ifs.tellg();
		String buff;
		std::getline(m_ifs, buff);
		if(!m_ifs.good()) {
			break;
		}
		if(buff.find("v ") == 0) {
			if(m_posCount == 0) {
				m_vOffs = strmPos;
				LogManager::LogInfo() << m_vOffs;
			}
			m_posCount += 1;
			buff.clear();
		}
		else if(buff.find("vt ") == 0) {
			if(m_texCount == 0) {
				m_vtOffs = strmPos;
				LogManager::LogInfo() << m_vtOffs;
			}
			m_texCount += 1;
			buff.clear();
		}
		else if(buff.find("vn ") == 0) {
			if(m_normCount == 0) {
				m_vnOffs = strmPos;
				LogManager::LogInfo() << m_vnOffs;
			}
			m_normCount += 1;
			buff.clear();

		}
		else if(buff.find("f ") == 0) {
			if(m_numPrim == 0) {
				m_fOffs = strmPos;
				LogManager::LogInfo() << m_fOffs;
			}
			m_numPrim += 1;
			buff.clear();
		}
	}
	if(m_posCount > 0 && m_numPrim > 0) {
		return true;
	}
	return false;
}

String OBJReader::readOBJLine()
{
	String buff;
	std::getline(m_ifs, buff);
	if(!m_ifs.good()) {
		return String();
	}
	String::iterator end = std::remove(buff.begin(), buff.end(), '\r');
	end = std::remove(buff.begin(), end, '\n');
	return String(buff.begin(), end);
}

OBJReader::StringList OBJReader::splitString(String line, char delim)
{
	StringList res;
	std::stringstream str(line);
	String temp;
	while(std::getline(str, temp, delim)) {
		res.push_back(temp);
	}
	return res;
}

unsigned int OBJReader::strToUInt(String str)
{
	std::stringstream ss(str);
	unsigned int res = 0;
	ss >> res;
	return res;
}

unsigned short int OBJReader::strToUSInt(String str)
{
	std::stringstream ss(str);
	unsigned short int res = 0;
	ss >> res;
	return res;
}


float OBJReader::strToFloat(String str)
{
	std::stringstream ss(str);
	float res = 0.0f;
	ss >> res;
	return res;
}

size_t OBJReader::readOBJV(void * p_data)
{
	size_t res = 0;
	m_ifs.seekg(m_vOffs);
	float * p_buff = static_cast<float *>(p_data);
	String line = readOBJLine();
	while(!line.empty()) {
		StringList strList = splitString(line, ' ');
		StringList::const_iterator it = strList.begin();
		if(it->compare("v") != 0) {
			break;
		}
		if(strList.size() != 4) {
			LogManager::LogError() << "supports only 3 components of position";
			break;
		}
		++it;
		for(; it != strList.end(); ++it) {
			const String & temp = *it;
			if(temp.empty()) {
				// TODO: print error
				return 0;
			}
			float comp = strToFloat(temp);
			// TODO: check comp
			*p_buff = comp;
			++p_buff;
		}
		++res;
		line = readOBJLine();
	}
	return res;
}

size_t OBJReader::readOBJVT(void * p_data)
{
	size_t res = 0;
	m_ifs.seekg(m_vtOffs);
	float * p_buff = static_cast<float *>(p_data);
	String line = readOBJLine();
	while(!line.empty()) {
		StringList strList = splitString(line, ' ');
		StringList::const_iterator it = strList.begin();
		if(it->compare("vt") != 0) {
			break;
		}
		if(strList.size() != 3) {
			LogManager::LogError() << "supports only 2 components of tex coord";
			break;
		}
		++it;
		for(; it != strList.end(); ++it) {
			const String & temp = *it;
			if(temp.empty()) {
				// TODO: print error
				return 0;
			}
			float comp = strToFloat(temp);
			// TODO: check comp
			*p_buff = comp;
			++p_buff;
		}
		++res;
		line = readOBJLine();
	}
	return res;
}

size_t OBJReader::readOBJVN(void * p_data)
{
	size_t res = 0;
	m_ifs.seekg(m_vnOffs);
	float * p_buff = static_cast<float *>(p_data);
	String line = readOBJLine();
	while(!line.empty()) {
		StringList strList = splitString(line, ' ');
		StringList::const_iterator it = strList.begin();
		if(it->compare("vn") != 0) {
			break;
		}
		if(strList.size() != 4) {
			LogManager::LogError() << "supports only 3 components of normal";
			break;
		}
		++it;
		for(; it != strList.end(); ++it) {
			const String & temp = *it;
			if(temp.empty()) {
				// TODO: print error
				return 0;
			}
			float comp = strToFloat(temp);
			// TODO: check comp
			*p_buff = comp;
			++p_buff;
		}
		++res;
		line = readOBJLine();
	}
	return res;
}

size_t OBJReader::readOBJF(void * p_indcs, unsigned char advance)
{
	size_t res = 0;
	m_ifs.seekg(m_fOffs);
	unsigned int * p_buff = static_cast<unsigned int *>(p_indcs);
	String line = readOBJLine();
	while(!line.empty()) {
		StringList strList = splitString(line, ' ');
		StringList::const_iterator it = strList.begin();
		if(it->find("f") != 0) {
			break;
		}
		if(strList.size() != 4) {
			LogManager::LogError() << "supports only triangles";
			break;
		}
		for(++it; it != strList.end(); ++it) {
			StringList strList2 = splitString(*it, '/');
			StringList::const_iterator it2 = strList2.begin();
			std::advance(it2, advance);
			const String & temp = *(it2);
			if(temp.empty()) {
				// TODO: print error
				return 0;
			}
			float indx = strToUInt(temp);
			if(indx == 0) {
				// TODO: print error
				return 0;
			}
			*p_buff = indx - 1;
			++p_buff;
		}
		++res;
		line = readOBJLine();
	}
	return res;
}

void OBJReader::closeOBJFile()
{
	m_ifs.close();
}



}
}

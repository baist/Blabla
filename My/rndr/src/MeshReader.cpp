
#include "MeshReader.hpp"

namespace bge
{
namespace rndr
{

MeshReader::MeshReader()
{
}

MeshReader::~MeshReader()
{
}

size_t MeshReader::getPosCount() const
{
	return m_posCount;
}

size_t MeshReader::getTexCount() const
{
	return m_texCount;
}

size_t MeshReader::getNormCount() const
{
	return m_normCount;
}

size_t MeshReader::getNumPrim() const
{
	return m_numPrim;
}

size_t MeshReader::readPositions(void * p_data, void * p_indcs)
{
	return 0;
}

size_t MeshReader::readTexCoords(void * p_data, void * p_indcs)
{
	return 0;
}

size_t MeshReader::readNormals(void * p_data, void * p_indcs)
{
	return 0;
}


}
}


#include "TGAFile.hpp"

namespace bge
{
namespace rndr
{

TGAFile::TGAFile()
{
}

TGAFile::~TGAFile()
{
}

TGAFile::TGAFile(const String & fileName):
	m_fileName(fileName)
{
	m_ok = true;
	if(!openTGAFile()) {
		m_ok = false;
		return;
	}
	closeTGAFile();
}

size_t TGAFile::read(void * p_buf, size_t size)
{
	if(!m_ok) {
		return 0;
	}
	if(!openTGAFile()) {
		m_ok = false;
		return 0;
	}
	size_t res = readTGAFile(p_buf, size);
	if(res == 0) {
		m_ok = false;
	}
	closeTGAFile();
	return res;
}

bool TGAFile::openTGAFile()
{
	m_ifs.open(m_fileName.c_str());
	if(!m_ifs.good()) {
		return false;
	}
	char hdr[12];
	m_ifs.read(reinterpret_cast<std::ifstream::char_type*>(hdr),
						 sizeof(hdr));
	if(hdr[2] != 2) {
		return false;
	}
	unsigned short int width = 0;
	m_ifs.read(reinterpret_cast<std::ifstream::char_type*>(&width),
						 sizeof(width));
	unsigned short int height = 0;
	m_ifs.read(reinterpret_cast<std::ifstream::char_type*>(&height),
						 sizeof(height));
	unsigned char bpp = 0;
	m_ifs.read(reinterpret_cast<std::ifstream::char_type*>(&bpp),
						 sizeof(bpp));
	if(bpp == 32) {
		m_BPP = 4;
		m_format = IF_A8R8G8B8;
	}
	else if(bpp == 24) {
		m_BPP = 3;
		m_format = IF_R8G8B8;
	}
	else {
		return false;
	}
	m_width = width;
	m_height = height;
	m_size = m_width * m_height * m_BPP;
	m_ifs.read(reinterpret_cast<std::ifstream::char_type*>(&bpp),
						 sizeof(bpp));
	m_dataOffs = m_ifs.tellg();
	return true;
}

size_t TGAFile::readTGAFile(void * p_buf, size_t size)
{
	m_ifs.seekg(m_dataOffs);
	m_ifs.read(reinterpret_cast<std::ifstream::char_type*>(p_buf), size);
	if(m_ifs.fail()) {
		return 0;
	}
	return m_ifs.gcount();
}

void TGAFile::closeTGAFile()
{
	m_ifs.close();
}


}
}

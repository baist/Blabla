#include "MyTexture.hpp"

namespace bge
{
namespace rndr
{

MyTexture::MyTexture(const String &name, int width, int height,
										 ImgFormat format):
	Texture(name, width, height, format)
{
	m_imageBuffer = new Image(width, height, format);
}

MyTexture::~MyTexture()
{
	delete m_imageBuffer;
}

Image *MyTexture::getImage()
{
	return m_imageBuffer;
}

const Image *MyTexture::getImage() const
{
	return m_imageBuffer;
}



}
}


#include "LogManager.hpp"
#include "VorbisStream.hpp"

namespace bge
{
namespace snd
{

VorbisStream::VorbisStream()
{
}

VorbisStream::~VorbisStream()
{
}

VorbisStream::VorbisStream(const String & fileName):
	m_fileName(fileName)
{
	m_ok = true;
	m_pos = 0;
	m_size = 0;
	m_offs = 0;
	m_seekingTime = -1.0;
	if(!openOGGVorbis()) {
		m_ok = false;
		return;
	}
	m_p_vorbisInfo  = ov_info(&m_oggVorbis, -1);
	m_numChannels = m_p_vorbisInfo->channels;
	m_frequency   = m_p_vorbisInfo->rate;
	m_bitDepth    = 16;
	m_duration = ov_time_total (&m_oggVorbis, -1);
	m_size = static_cast<size_t>(
				ov_pcm_total(&m_oggVorbis, -1) * m_numChannels * 2);
	//m_size = static_cast<size_t>(m_duration * m_numChannels * m_frequency * 2);
	closeOGGVorbis();
}

size_t VorbisStream::read(void * p_buf, size_t size)
{
	if(!m_ok) {
		return 0;
	}
	if(!openOGGVorbis()) {
		m_ok = false;
		return 0;
	}
	if(m_seekingTime >= 0.0) {
		ov_time_seek(&m_oggVorbis, m_seekingTime);
		m_seekingTime = -1.0;
	}
	else if(ov_pcm_seek(&m_oggVorbis, m_offs) != 0) {
		m_ok = false;
		return 0;
	}
	int	curSection;
	long int res = 0;
	int	bytesRead = 0;
	while (bytesRead < size) {
		res = ov_read (&m_oggVorbis, static_cast<char *>(p_buf) + bytesRead,
									 size - bytesRead, 0, 2, 1, &curSection);
		if (res <= 0) {
			break;
		}
		bytesRead += res;
	}
	LogManager::LogInfo() << "reading " << bytesRead << " bytes " <<
							 "from " << m_fileName;
	m_offs = ov_pcm_tell(&m_oggVorbis);
	closeOGGVorbis();
	return bytesRead;
}

bool VorbisStream::seek(double time)
{
	m_seekingTime = time;
	return true;
}

bool VorbisStream::openOGGVorbis()
{
	m_p_filePtr = fopen(m_fileName.c_str(), "rb");
	if (m_p_filePtr == 0) {
		LogManager::LogError() << "ERROR: cannot open " << m_fileName;
		return false;
	}
	if (ov_open(m_p_filePtr, &m_oggVorbis, 0, 0) != 0) {
		LogManager::LogError() << "ERROR: decoding " << m_fileName;
		fclose (m_p_filePtr);
		return false;
	}
	return true;
}

void VorbisStream::closeOGGVorbis()
{
	ov_clear(&m_oggVorbis);
}



}
}


#include "OALSound.hpp"

namespace bge
{
namespace snd
{

OALSound::OALSound()
{
}

OALSound::~OALSound()
{
	alSourceStop(m_sourceID);
	alDeleteSources(1, &m_sourceID);
	if (m_streaming) {
		alDeleteBuffers (2, m_bufferIDs);
	}
	else {
		alDeleteBuffers (1, m_bufferIDs);
	}
}

OALSound::OALSound(const String & name, OALSoundManager * p_oalSndMngr,
									 Stream * p_stream):
	Sound(name),
	m_p_oalSndMngr(p_oalSndMngr),
	m_p_stream(p_stream)
{
	m_maxBufferSize = 1048576; // 1 MB
	m_freq = m_p_stream->getFrequency();
	if(m_p_stream->getBitDepth() == 16) {
		if (m_p_stream->getNumChannels() == 1) {
			m_format = AL_FORMAT_MONO16;
		}
		else if (m_p_stream->getNumChannels() == 2) {
			m_format = AL_FORMAT_STEREO16;
		}
	}
	else if(m_p_stream->getBitDepth() == 8) {
		if (m_p_stream->getNumChannels() == 1) {
				m_format = AL_FORMAT_MONO8;
		}
		else if (m_p_stream->getNumChannels() == 2) {
				m_format = AL_FORMAT_STEREO8;
		}
	}
	ALsizei totalSize = m_p_stream->getSize();
	alGenSources(1, &m_sourceID);
	//TEST_ERROR0("generate source");
	m_currBuffer = 0;
	//sndDscr.offset = 0;
	m_looping = false;
	m_streaming = totalSize > m_maxBufferSize;
	if(m_streaming) {
		alGenBuffers(2, m_bufferIDs);
	}
	else {
		alGenBuffers(1, m_bufferIDs);
		std::vector<char> bufferData;
		bufferData.resize(totalSize);
		void * p_data = &bufferData[0];
		size_t size = m_p_stream->read(p_data, totalSize);
		alBufferData(m_bufferIDs[m_currBuffer], m_format, p_data, size, m_freq);
		alSourcei(m_sourceID, AL_BUFFER, m_bufferIDs[m_currBuffer]);
		alSourcei(m_sourceID, AL_LOOPING, m_looping ? AL_TRUE : AL_FALSE);
	}
	alSourcef(m_sourceID, AL_PITCH, 1);
	alSourcef(m_sourceID, AL_GAIN, 1);
	alSource3f(m_sourceID, AL_POSITION, 0, 0, 0);
	alSource3f(m_sourceID, AL_VELOCITY, 0, 0, 0);
}

void OALSound::play()
{
	ALint state;
	size_t size;
	alGetSourcei (m_sourceID, AL_SOURCE_STATE, &state);
	if (state == AL_PLAYING) {
		return;
	}
	if (state == AL_PAUSED) {
		return;
	}
	if(m_streaming) {
		std::vector<char> bufferData;
		bufferData.resize(m_maxBufferSize);
		void * p_data = &bufferData[0];
		size = m_p_stream->read(p_data, m_maxBufferSize);
		alBufferData(m_bufferIDs[0], m_format, p_data, size, m_freq);
		size = m_p_stream->read(p_data, m_maxBufferSize);
		alBufferData(m_bufferIDs[1], m_format, p_data, size, m_freq);
		alSourceQueueBuffers(m_sourceID, 2, m_bufferIDs);
	}
	m_p_oalSndMngr->played(this);
	alSourcePlay(m_sourceID);
}

void OALSound::stop()
{
	alSourceStop(m_sourceID);
	m_p_stream->seek(0.0);
	if (m_streaming) {
		ALint queued;
		alGetSourcei(m_sourceID, AL_BUFFERS_QUEUED,&queued);
		if (queued > 0) {
			alSourceUnqueueBuffers(m_sourceID, 2, m_bufferIDs);
		}
		m_currBuffer = 0;
	}
	m_p_oalSndMngr->stoped(this);
}

void OALSound::update()
{
	if (m_streaming) {
		ALint processed;
		alGetSourcei (m_sourceID, AL_BUFFERS_PROCESSED, &processed);
		if (processed == 0) {
			return;
		}
		if(processed == 1) {
			ALsizei freq = m_p_stream->getFrequency();
			alSourceUnqueueBuffers (m_sourceID, 1, &m_bufferIDs[m_currBuffer]);
			std::vector<char> bufferData;
			bufferData.resize(m_maxBufferSize);
			void * p_data = &bufferData[0];
			int size = m_p_stream->read(p_data, m_maxBufferSize);
			if (size > 0 || (size == 0 && m_looping) ) {
				alBufferData(m_bufferIDs[m_currBuffer], m_format, p_data, size, freq);
				alSourceQueueBuffers (m_sourceID, 1, &m_bufferIDs[m_currBuffer]);
				if (size < m_maxBufferSize && m_looping) {
					m_p_stream->seek(0.0);
				}
			}
			else {
				ALint queued;
				alGetSourcei(m_sourceID, AL_BUFFERS_QUEUED, &queued);
				if(queued == 0) {
					m_p_stream->seek(0.0);
				}
			}
			m_currBuffer = 1 - m_currBuffer;
		}
		else if (processed == 2) {
			alSourceUnqueueBuffers (m_sourceID, 2, m_bufferIDs);
			m_currBuffer = 0;
			play ();
		}
	}
}



}
}

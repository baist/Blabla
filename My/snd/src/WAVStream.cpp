
#include "LogManager.hpp"
#include "WAVStream.hpp"

namespace bge
{
namespace snd
{

WAVStream::WAVStream()
{
}

WAVStream::~WAVStream()
{
}

WAVStream::WAVStream(const String & fileName):
	m_fileName(fileName)
{
	m_absOffs = 0;
	m_ok = true;
	if(!openWAVFile()) {
		m_ok = false;
		return;
	}
	if(!readWAVParams()) {
		m_ok = false;
		return;
	}
	closeWAVFile();
}

size_t WAVStream::read(void * p_buf, size_t size)
{
	if(!m_ok) {
		return 0;
	}
	if(!openWAVFile()) {
		m_ok = false;
		return 0;
	}
	size_t res = readWAVFile(p_buf, size);
	if(res == 0) {
		m_ok = false;
	}
	LogManager::LogInfo() << "reading " << res << " bytes " <<
													 "from " << m_fileName;
	closeWAVFile();
	return res;
}

bool WAVStream::seek(double time)
{
	seekTimeWAVFile(time);
	return true; // !m_ifs.fail();;
}

bool WAVStream::openWAVFile()
{
	enum
	{
		RIFF = 0x46464952,
		WAVE = 0x45564157,
		FMT  = 0x20746D66,
		DATA = 0x61746164,
	};

	m_ifs.open(m_fileName.c_str());
	if(!m_ifs.good()) {
		return false;
	}
	int magic;
	m_ifs.read(reinterpret_cast<std::ifstream::char_type*>(&magic),
						 sizeof(magic));
	int length;
	m_ifs.read(reinterpret_cast<std::ifstream::char_type*>(&length),
						 sizeof(length));
	int magic2;
	m_ifs.read(reinterpret_cast<std::ifstream::char_type*>(&magic2),
						 sizeof(magic2));
	if (magic != RIFF || magic2 != WAVE) {
		return false;
	}
	return true;
}

bool WAVStream::readWAVParams()
{
	struct WAVFormat {
		unsigned short encoding;
		unsigned short channels;
		unsigned int   frequency;
		unsigned int   byterate;
		unsigned short blockAlign;
		unsigned short bitsPerSample;
	};

	enum
	{
		RIFF = 0x46464952,
		WAVE = 0x45564157,
		FMT  = 0x20746D66,
		DATA = 0x61746164,
	};

	unsigned int magic;
	unsigned int length;

	while(!m_ifs.eof()) {
		m_ifs.read(reinterpret_cast<std::ifstream::char_type*>(&magic),
							 sizeof(magic));
		m_ifs.read(reinterpret_cast<std::ifstream::char_type*>(&length),
							 sizeof(length));
		m_absOffs = m_ifs.tellg();
		if (magic == FMT) {
			WAVFormat	format;
			m_ifs.read(reinterpret_cast<std::ifstream::char_type*>(&format),
								 sizeof(WAVFormat));
			if(m_ifs.fail()) {
				return false;
			}
			if (format.encoding != 1) {
				return false;
			}
			m_absOffs = m_ifs.tellg();
			m_numChannels = format.channels;
			m_frequency = format.frequency;
			m_bitDepth = format.bitsPerSample;
		}
		else if (magic == DATA) {
			m_dataOffs = m_absOffs;
			m_size = length;
			break;
		}
		else {
			m_absOffs = m_absOffs + length;
			m_dataOffs = m_absOffs;
		}
	}
	if (m_numChannels < 1 || m_frequency == 0 ||
			m_dataOffs == 0 || m_size == 0) {
		return false;
	}
	return true;
}

void WAVStream::seekTimeWAVFile(double time)
{
	if(time < 0.0) {
		return;
	}
	if(time > m_duration) {
		return;
	}
	const unsigned int bytesPerSample = m_bitDepth / 2;
	const std::ifstream::pos_type off =
			time * m_numChannels * m_frequency * bytesPerSample;
	m_absOffs = m_dataOffs + off;
}

size_t WAVStream::readWAVFile(void * p_buf, size_t size)
{
	m_ifs.seekg(m_absOffs);
	std::ifstream::pos_type	bytesLeft = m_size - (m_absOffs - m_dataOffs);
	if (size < 0 || size > bytesLeft) {
		size = bytesLeft;
	}
	m_ifs.read(reinterpret_cast<std::ifstream::char_type*>(p_buf), size);
	if(m_ifs.fail()) {
		return 0;
	}
	m_absOffs = m_ifs.tellg();
	return m_ifs.gcount();
}

void WAVStream::closeWAVFile()
{
	m_ifs.close();
}



}
}


#include <algorithm>
#include "LogManager.hpp"
#include "OALSoundManager.hpp"

namespace bge
{
namespace snd
{

OALSoundManager::OALSoundManager()
{
	ALboolean enumeration =
			alcIsExtensionPresent(0, "ALC_ENUMERATION_EXT");
	if (enumeration == AL_FALSE) {
		LogManager::LogError() << "enumeration extension not available";
		return;
	}
	else {
		const ALCchar *devices = alcGetString(0, ALC_ALL_DEVICES_SPECIFIER);
		do {
			//log->write(String("Device: ") + devices);
			while(devices[0] != '\0') {
				++devices;
			}
			++devices;
		} while(devices[0] != '\0');
	}
	const ALCchar *defaultDeviceName =
			alcGetString(0, ALC_DEFAULT_DEVICE_SPECIFIER);
	m_p_device = alcOpenDevice(defaultDeviceName);
	if (!m_p_device) {
		LogManager::LogError() << "unable to open default device";
		return;
	}

	LogManager::LogInfo() << "Device: " <<
			alcGetString(m_p_device, ALC_DEVICE_SPECIFIER);
	LogManager::LogInfo() << "Device: " <<
			alcGetString(m_p_device, ALC_EXTENSIONS);

	alGetError();

	m_p_context = alcCreateContext(m_p_device, 0);
	if (!alcMakeContextCurrent(m_p_context)) {
		LogManager::LogError() << "failed to make default context";
		return;
	}

	LogManager::LogInfo() << "Vendor: " << alGetString(AL_VENDOR);
	LogManager::LogInfo() << "Renderer: " << alGetString(AL_RENDERER);
	LogManager::LogInfo() << "Version: " << alGetString(AL_VERSION);

	ALfloat listenerOri[] = { 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f };
	// set orientation
	alListener3f(AL_POSITION, 0, 0, 1.0f);
	//alListener3f(AL_VELOCITY, 0, 0, 0);
	alListenerfv(AL_ORIENTATION, listenerOri);
}

OALSoundManager::~OALSoundManager()
{
	deleteAll();
	alcMakeContextCurrent(0);
	alcDestroyContext(m_p_context);
	alcCloseDevice(m_p_device);
}

void OALSoundManager::deleteAll()
{
	ResourceManager::deleteAll();
	NamedStreams::iterator it = m_nmdStreams.begin();
	for(; it != m_nmdStreams.end(); ++it) {
		delete it->second;
	}
	m_nmdStreams.clear();
	m_streams.clear();
	//ResourceManager::deleteAll();
}

Sound * OALSoundManager::newSound(const String & name)
{
	const String newName = generateNewName(name);
	size_t pointPos = name.find_last_of('.');
	const String soundExt = name.substr(pointPos + 1);
	Stream * p_stream;
	if(!soundExt.compare("wav")) {
		p_stream = new WAVStream(name);
	}
	else if(!soundExt.compare("ogg")) {
		p_stream = new VorbisStream(name);
	}
	else {
		LogManager::LogError() << "unknown file extension \"" << soundExt << "\"";
		return 0;
	}
	m_streams.push_back(p_stream);
	NamedStreams::const_iterator it = m_nmdStreams.find(name);
	if(it == m_nmdStreams.end()) {
		m_nmdStreams.insert(NamedStreams::value_type(name, p_stream));
	}
	OALSound * p_oalSnd = new OALSound(newName, this, p_stream);
	addRes(p_oalSnd);
	return p_oalSnd;
}

void OALSoundManager::update()
{
	PlayedSounds::iterator it = m_playedSounds.begin();
	for(; it != m_playedSounds.end(); ++it) {
		(*it)->update();
	}
}

void OALSoundManager::played(OALSound * p_oalSnd)
{
	PlayedSounds::iterator it =
			std::find(m_playedSounds.begin(), m_playedSounds.end(), p_oalSnd);
	if(it != m_playedSounds.end()) {
		return;
	}
	m_playedSounds.push_back(p_oalSnd);
}

void OALSoundManager::stoped(OALSound * p_oalSnd)
{
	PlayedSounds::iterator it =
			std::find(m_playedSounds.begin(), m_playedSounds.end(), p_oalSnd);
	if(it == m_playedSounds.end()) {
		return;
	}
	m_playedSounds.erase(it);
}



}
}

#if !defined(VORBISSTREAM_HPP)
#define VORBISSTREAM_HPP

#include "Stream.hpp"
#include "String.hpp"
#include "vorbis/vorbisfile.h"

namespace bge
{
namespace snd
{


class VorbisStream: public Stream
{
public:
	VorbisStream();
	virtual ~VorbisStream();
	VorbisStream(const String & fileName);
	virtual size_t read(void * p_buf, size_t size);
	virtual bool seek(double time);
private:
	bool m_ok;
	String m_fileName;
	FILE * m_p_filePtr;
	OggVorbis_File m_oggVorbis;
	vorbis_info * m_p_vorbisInfo;
	size_t m_offs;
	double m_seekingTime;
private:
	bool openOGGVorbis();
	void closeOGGVorbis();
};

}
}

#endif // VORBISSTREAM_HPP

#if !defined(OALSOUND_HPP)
#define OALSOUND_HPP

#include <AL/al.h>
#include "Prerequisites.hpp"
#include "Sound.hpp"
#include "OALSoundManager.hpp"

namespace bge
{
namespace snd
{

class OALSoundManager;

class OALSound: public Sound
{
public:
	OALSound();
	virtual ~OALSound();
	OALSound(const String & name, OALSoundManager * p_oalSndMngr,
					 Stream * p_stream);
	virtual void play();
	virtual void stop();
	void update();
private:
	OALSoundManager * m_p_oalSndMngr;
	Stream * m_p_stream;
	size_t m_maxBufferSize;
	bool m_streaming;
	bool m_looping;
	ALuint m_sourceID;
	ALuint m_bufferIDs[2];
	ALenum m_format;
	ALsizei m_freq;
	unsigned int m_currBuffer;
};

}
}

#endif // OALSOUND_HPP

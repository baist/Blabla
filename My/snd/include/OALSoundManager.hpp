#if !defined(OALSOUNDMANAGER_HPP)
#define OALSOUNDMANAGER_HPP

#include <AL/al.h>
#include <AL/alc.h>
#include <list>
#include <map>
#include "Prerequisites.hpp"
#include "VorbisStream.hpp"
#include "WAVStream.hpp"
#include "SoundManager.hpp"
#include "OALSound.hpp"
#include "Stream.hpp"

namespace bge
{
namespace snd
{

class OALSound;

class OALSoundManager: public SoundManager
{
public:
	OALSoundManager();
	virtual ~OALSoundManager();
	virtual void deleteAll();
	virtual Sound * newSound(const String & name);
	virtual void update();
	void played(OALSound * p_oalSnd);
	void stoped(OALSound * p_oalSnd);
private:
	typedef std::list<Stream *> Streams;
	typedef std::map<String, Stream *> NamedStreams;
	typedef std::list<OALSound *> PlayedSounds;
private:
	ALCcontext * m_p_context;
	ALCdevice * m_p_device;
	Streams m_streams;
	NamedStreams m_nmdStreams;
	PlayedSounds m_playedSounds;
};

}
}

#endif // OALSOUNDMANAGER_HPP

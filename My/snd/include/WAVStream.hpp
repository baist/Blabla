#if !defined(WAVSTREAM_HPP)
#define WAVSTREAM_HPP

#include <fstream>
#include "Stream.hpp"
#include "String.hpp"

namespace bge
{
namespace snd
{


class WAVStream: public Stream
{
public:
	WAVStream();
	virtual ~WAVStream();
	WAVStream(const String & fileName);
	virtual size_t read(void * p_buf, size_t size);
	virtual bool seek(double time);
private:
	bool m_ok;
	String m_fileName;
	std::ifstream::pos_type m_dataOffs;
	std::ifstream::pos_type m_absOffs;
	std::ifstream m_ifs;
private:
	bool openWAVFile();
	bool readWAVParams();
	void seekTimeWAVFile(double time);
	size_t readWAVFile(void * p_buf, size_t size);
	void closeWAVFile();
};

}
}

#endif // WAVSTREAM_HPP

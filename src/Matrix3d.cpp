#include "Matrix3d.hpp"
#include <algorithm>
#include "Quaternion.hpp"
#include "Math.hpp"

namespace bge
{
const real Matrix3d::EPSILON = 1e-06;
const Matrix3d Matrix3d::ZERO(0, 0, 0, 0, 0, 0, 0, 0, 0);
const Matrix3d Matrix3d::IDENTITY(1, 0, 0, 0, 1, 0, 0, 0, 1);

Matrix3d::Matrix3d ()
{
	m[0][0] = 0.0f;
	m[0][1] = 0.0f;
	m[0][2] = 0.0f;
	m[1][0] = 0.0f;
	m[1][1] = 0.0f;
	m[1][2] = 0.0f;
	m[2][0] = 0.0f;
	m[2][1] = 0.0f;
	m[2][2] = 0.0f;
}
Matrix3d::Matrix3d (const real arr[3][3])
{
	std::copy(arr, arr + 9, m);
}
Matrix3d::Matrix3d (const Matrix3d &matr)
{
	std::copy(matr.m, matr.m + 9, m);
}
Matrix3d::Matrix3d(const Quaternion &rot)
{
	Matrix3d m3x3;
	rot.ToRotationMatrix(m3x3);
	operator=(m3x3);
}

Matrix3d::Matrix3d (real val00, real val01, real val02,
					real val10, real val11, real val12,
					real val20, real val21, real val22)
{
	m[0][0] = val00;
	m[0][1] = val01;
	m[0][2] = val02;
	m[1][0] = val10;
	m[1][1] = val11;
	m[1][2] = val12;
	m[2][0] = val20;
	m[2][1] = val21;
	m[2][2] = val22;
}

// member access, allows use of construct mat[r][c]
real *Matrix3d::operator[] (size_t row) const
{
	return (real *)m[row];
}

Matrix3d &Matrix3d::operator= (const Matrix3d &matr)
{
	std::copy(matr.m, matr.m + 9, m);
	return *this;
}

bool Matrix3d::operator== (const Matrix3d &matr) const
{
	for (size_t row = 0; row < 3; row++) {
		for (size_t col = 0; col < 3; col++) {
			if (m[row][col] != matr.m[row][col]) {
				return false;
			}
		}
	}
	return true;
}

bool Matrix3d::operator!= (const Matrix3d &matr) const
{
	return !operator==(matr);
}

Matrix3d Matrix3d::operator+ (const Matrix3d &matr) const
{
	Matrix3d res;
	for (size_t row = 0; row < 3; row++) {
		for (size_t col = 0; col < 3; col++) {
			res.m[row][col] = m[row][col] + matr.m[row][col];
		}
	}
	return res;
}

Matrix3d Matrix3d::operator- (const Matrix3d &matr) const
{
	Matrix3d res;
	for (size_t row = 0; row < 3; row++) {
		for (size_t col = 0; col < 3; col++) {
			res.m[row][col] = m[row][col] - matr.m[row][col];
		}
	}
	return res;
}

Matrix3d Matrix3d::operator* (const Matrix3d &matr) const
{
	Matrix3d res;
	for (size_t row = 0; row < 3; row++) {
		for (size_t col = 0; col < 3; col++) {
			res.m[row][col] =	m[row][0] * matr.m[0][col] +
					m[row][1] * matr.m[1][col] + m[row][2] * matr.m[2][col];
		}
	}
	return res;
}

Vector3d Matrix3d::operator* (const Vector3d &vec) const
{
	Vector3d res;
	for (size_t row = 0; row < 3; row++) {
		res[row] = m[row][0] * vec[0] + m[row][1] * vec[1] + m[row][2] * vec[2];
	}
	return res;
}

Matrix3d Matrix3d::operator- () const
{
	Matrix3d res;
	for (size_t row = 0; row < 3; row++) {
		for (size_t col = 0; col < 3; col++) {
			res[row][col] = -m[row][col];
		}
	}
	return res;
}

Matrix3d Matrix3d::operator* (real scalar) const
{
	Matrix3d res;
	for (size_t row = 0; row < 3; row++) {
		for (size_t col = 0; col < 3; col++) {
			res[row][col] = scalar * m[row][col];
		}
	}
	return res;
}

bool Matrix3d::hasScale() const
{
	// check magnitude of column vectors (==local axes)
	real t = Math::Sqr(m[0][0]) + Math::Sqr(m[1][0]) + Math::Sqr(m[2][0]);
	if (!Math::RealEqual(t, 1.0, (real)1e-04)) {
		return true;
	}
	t = Math::Sqr(m[0][1]) + Math::Sqr(m[1][1]) + Math::Sqr(m[2][1]);
	if (!Math::RealEqual(t, 1.0, (real)1e-04)) {
		return true;
	}
	t = Math::Sqr(m[0][2]) + Math::Sqr(m[1][2]) + Math::Sqr(m[2][2]);
	if (!Math::RealEqual(t, 1.0, (real)1e-04)) {
		return true;
	}
	return false;
}

Matrix3d Matrix3d::transpose () const
{
	Matrix3d res;
	for (size_t row = 0; row < 3; row++) {
		for (size_t col = 0; col < 3; col++) {
			res[row][col] = m[col][row];
		}
	}
	return res;
}

bool Matrix3d::inverse (Matrix3d &invMatr, real tolerance) const
{
	// Invert a 3x3 using cofactors.  This is about 8 times faster than
	// the Numerical Recipes code which uses Gaussian elimination.
	invMatr[0][0] = m[1][1] * m[2][2] - m[1][2] * m[2][1];
	invMatr[0][1] = m[0][2] * m[2][1] - m[0][1] * m[2][2];
	invMatr[0][2] = m[0][1] * m[1][2] - m[0][2] * m[1][1];
	invMatr[1][0] = m[1][2] * m[2][0] - m[1][0] * m[2][2];
	invMatr[1][1] = m[0][0] * m[2][2] - m[0][2] * m[2][0];
	invMatr[1][2] = m[0][2] * m[1][0] - m[0][0] * m[1][2];
	invMatr[2][0] = m[1][0] * m[2][1] - m[1][1] * m[2][0];
	invMatr[2][1] = m[0][1] * m[2][0] - m[0][0] * m[2][1];
	invMatr[2][2] = m[0][0] * m[1][1] - m[0][1] * m[1][0];
	real det = m[0][0] * invMatr[0][0] +
			m[0][1] * invMatr[1][0] + m[0][2] * invMatr[2][0];
	if (Math::Abs(det) <= tolerance) {
		return false;
	}
	real invDet = 1.0f / det;
	for (size_t row = 0; row < 3; row++) {
		for (size_t col = 0; col < 3; col++) {
			invMatr[row][col] *= invDet;
		}
	}
	return true;
}

Matrix3d Matrix3d::inverse (real tolerance) const
{
	Matrix3d res = Matrix3d::ZERO;
	inverse(res, tolerance);
	return res;
}

real Matrix3d::determinant () const
{
	real cofactor00 = m[1][1] * m[2][2] - m[1][2] * m[2][1];
	real cofactor10 = m[1][2] * m[2][0] - m[1][0] * m[2][2];
	real cofactor20 = m[1][0] * m[2][1] - m[1][1] * m[2][0];
	real det =
			m[0][0] * cofactor00 + m[0][1] * cofactor10 +	m[0][2] * cofactor20;
	return det;
}

void Matrix3d::toAngleAxis (Vector3d &axis, real &angle) const
{
	// Let (x,y,z) be the unit-length axis and let A be an angle of rotation.
	// The rotation matrix is R = I + sin(A)*P + (1-cos(A))*P^2 where
	// I is the identity and
	//
	//       +-        -+
	//   P = |  0 +z -y |
	//       | -z  0 +x |
	//       | +y -x  0 |
	//       +-        -+
	//
	// If A > 0, R represents a counterclockwise rotation about the axis in
	// the sense of looking from the tip of the axis vector towards the
	// origin.  Some algebra will show that
	//
	//   cos(A) = (trace(R)-1)/2  and  R - R^t = 2*sin(A)*P
	//
	// In the event that A = pi, R-R^t = 0 which prevents us from extracting
	// the axis through P.  Instead note that R = I+2*P^2 when A = pi, so
	// P^2 = (R-I)/2.  The diagonal entries of P^2 are x^2-1, y^2-1, and
	// z^2-1.  We can solve these for axis (x,y,z).  Because the angle is pi,
	// it does not matter which sign you choose on the square roots.
	real trace = m[0][0] + m[1][1] + m[2][2];
	real cosVal = 0.5f * (trace - 1.0f);
	angle = Math::ACos(cosVal);  // in [0,PI]
	if (angle > real(0.0)) {
		if (angle < real(Math::PI)) {
			axis.x = m[1][2] - m[2][1];
			axis.y = m[2][0] - m[0][2];
			axis.z = m[0][1] - m[1][0];
			axis.normalise();
		}
		else {
			// angle is PI
			float halfInv;
			if (m[0][0] >= m[1][1]) {
				// r00 >= r11
				if (m[0][0] >= m[2][2]) {
					// r00 is maximum diagonal term
					axis.x = 0.5f * Math::Sqrt(m[0][0] - m[1][1] - m[2][2] + 1.0f);
					halfInv = 0.5f / axis.x;
					axis.y = halfInv * m[1][0];
					axis.z = halfInv * m[2][0];
				}
				else {
					// r22 is maximum diagonal term
					axis.z = 0.5f * Math::Sqrt(m[2][2] - m[0][0] - m[1][1] + 1.0f);
					halfInv = 0.5f / axis.z;
					axis.x = halfInv * m[2][0];
					axis.y = halfInv * m[2][1];
				}
			}
			else {
				// r11 > r00
				if (m[1][1] >= m[2][2]) {
					// r11 is maximum diagonal term
					axis.y = 0.5f * Math::Sqrt(m[1][1] - m[0][0] - m[2][2] + 1.0f);
					halfInv  = 0.5f / axis.y;
					axis.x = halfInv * m[1][0];
					axis.z = halfInv * m[2][1];
				}
				else {
					// r22 is maximum diagonal term
					axis.z = 0.5f * Math::Sqrt(m[2][2] - m[0][0] - m[1][1] + 1.0f);
					halfInv = 0.5f / axis.z;
					axis.x = halfInv * m[2][0];
					axis.y = halfInv * m[2][1];
				}
			}
		}
	}
	else {
		// The angle is 0 and the matrix is the identity.  Any axis will
		// work, so just use the x-axis.
		axis.x = 1.0;
		axis.y = 0.0;
		axis.z = 0.0;
	}
}

void Matrix3d::fromAngleAxis (const Vector3d &axis, const real &angle)
{
	real cosVal = Math::Cos(angle);
	real sinVal = Math::Sin(angle);
	real oneMinusCos = 1.0f - cosVal;
	real xyM = axis.x * axis.y * oneMinusCos;
	real xzM = axis.x * axis.z * oneMinusCos;
	real yzM = axis.y * axis.z * oneMinusCos;
	real xSin = axis.x * sinVal;
	real ySin = axis.y * sinVal;
	real zSin = axis.z * sinVal;
	m[0][0] = Math::Sqr(axis.x) * oneMinusCos + cosVal;
	m[1][0] = xyM - zSin;
	m[2][0] = xzM + ySin;
	m[0][1] = xyM + zSin;
	m[1][1] = Math::Sqr(axis.y) * oneMinusCos + cosVal;
	m[2][1] = yzM - xSin;
	m[0][2] = xzM - ySin;
	m[1][2] = yzM + xSin;
	m[2][2] = Math::Sqr(axis.z) * oneMinusCos + cosVal;
}

bool Matrix3d::toEulerAnglesXYZ (real &yAngle, real &pAngle,
																real &rAngle) const
{
	// rot =  cy*cz   cz*sx*sy+cx*sz  -cx*cz*sy+sx*sz
	//       -cy*sz   cx*cz-sx*sy*sz   cz*sx+cx*sy*sz
	//        sy     -cy*sx            cx*cy
	pAngle = real(Math::ASin(m[2][0]));
	if (pAngle < real(Math::HALF_PI)) {
		if (pAngle > real(-Math::HALF_PI)) {
			yAngle = Math::ATan2(-m[2][1], m[2][2]);
			rAngle = Math::ATan2(-m[1][0], m[0][0]);
			return true;
		}
		else {
			// WARNING.  Not a unique solution.
			real rmY = Math::ATan2(m[0][1], m[1][1]);
			rAngle = real(0.0);  // any angle works
			yAngle = rAngle - rmY;
			return false;
		}
	}
	else {
		// WARNING.  Not a unique solution.
		real rpY = Math::ATan2(m[0][1], m[1][1]);
		rAngle = real(0.0);  // any angle works
		yAngle = rpY - rAngle;
		return false;
	}
}

bool Matrix3d::toEulerAnglesXZY (real &yAngle, real &pAngle,
																real &rAngle) const
{
	// rot =  cy*cz   sx*sy+cx*cy*sz  -cx*sy+cy*sx*sz
	//       -sz      cx*cz            cz*sx
	//        cz*sy  -cy*sx+cx*sy*sz   cx*cy+sx*sy*sz
	pAngle = Math::ASin(-m[1][0]);
	if (pAngle < real(Math::HALF_PI)) {
		if (pAngle > real(-Math::HALF_PI)) {
			yAngle = Math::ATan2(m[1][2], m[1][1]);
			rAngle = Math::ATan2(m[2][0], m[0][0]);
			return true;
		}
		else {
			// WARNING.  Not a unique solution.
			real rmY = Math::ATan2(-m[0][2], m[2][2]);
			rAngle = real(0.0);  // any angle works
			yAngle = rAngle - rmY;
			return false;
		}
	}
	else {
		// WARNING.  Not a unique solution.
		real rpY = Math::ATan2(-m[0][2], m[2][2]);
		rAngle = real(0.0);  // any angle works
		yAngle = rpY - rAngle;
		return false;
	}
}

bool Matrix3d::toEulerAnglesYXZ (real &yAngle, real &pAngle,
																real &rAngle) const
{
	// rot =  cy*cz+sx*sy*sz   cx*sz  -cz*sy+cy*sx*sz
	//        cz*sx*sy-cy*sz   cx*cz   cy*cz*sx+sy*sz
	//        cx*sy           -sx      cx*cy
	pAngle = Math::ASin(-m[2][1]);
	if (pAngle < real(Math::HALF_PI)) {
		if (pAngle > real(-Math::HALF_PI)) {
			yAngle = Math::ATan2(m[2][0], m[2][2]);
			rAngle = Math::ATan2(m[0][1], m[1][1]);
			return true;
		}
		else {
			// WARNING.  Not a unique solution.
			real rmY = Math::ATan2(-m[1][0], m[0][0]);
			rAngle = real(0.0);  // any angle works
			yAngle = rAngle - rmY;
			return false;
		}
	}
	else {
		// WARNING.  Not a unique solution.
		real rpY = Math::ATan2(-m[1][0], m[0][0]);
		rAngle = real(0.0);  // any angle works
		yAngle = rpY - rAngle;
		return false;
	}
}

bool Matrix3d::toEulerAnglesYZX (real &yAngle, real &pAngle,
																real &rAngle) const
{
	// rot =  cy*cz            sz     -cz*sy
	//        sx*sy-cx*cy*sz   cx*cz   cy*sx+cx*sy*sz
	//        cx*sy+cy*sx*sz  -cz*sx   cx*cy-sx*sy*sz
	pAngle = Math::ASin(m[0][1]);
	if (pAngle < real(Math::HALF_PI)) {
		if (pAngle > real(-Math::HALF_PI)) {
			yAngle = Math::ATan2(-m[0][2], m[0][0]);
			rAngle = Math::ATan2(-m[2][1], m[1][1]);
			return true;
		}
		else {
			// WARNING.  Not a unique solution.
			real rmY = Math::ATan2(m[1][2], m[2][2]);
			rAngle = real(0.0);  // any angle works
			yAngle = rAngle - rmY;
			return false;
		}
	}
	else {
		// WARNING.  Not a unique solution.
		real rpY = Math::ATan2(m[1][2], m[2][2]);
		rAngle = real(0.0);  // any angle works
		yAngle = rpY - rAngle;
		return false;
	}
}

bool Matrix3d::toEulerAnglesZXY (real &yAngle, real &pAngle,
																real &rAngle) const
{
	// rot =  cy*cz-sx*sy*sz   cz*sx*sy+cy*sz  -cx*sy
	//       -cx*sz            cx*cz            sx
	//        cz*sy+cy*sx*sz  -cy*cz*sx+sy*sz   cx*cy
	pAngle = Math::ASin(m[1][2]);
	if (pAngle < real(Math::HALF_PI)) {
		if (pAngle > real(-Math::HALF_PI)) {
			yAngle = Math::ATan2(-m[1][0], m[1][1]);
			rAngle = Math::ATan2(-m[0][2], m[2][2]);
			return true;
		}
		else {
			// WARNING.  Not a unique solution.
			real rmY = Math::ATan2(m[2][0], m[0][0]);
			rAngle = real(0.0);  // any angle works
			yAngle = rAngle - rmY;
			return false;
		}
	}
	else {
		// WARNING.  Not a unique solution.
		real rpY = Math::ATan2(m[2][0], m[0][0]);
		rAngle = real(0.0);  // any angle works
		yAngle = rpY - rAngle;
		return false;
	}
}

bool Matrix3d::foEulerAnglesZYX (real &yAngle, real &pAngle,
																real &rAngle) const
{
	// rot =  cy*cz            cy*sz           -sy
	//        cz*sx*sy-cx*sz   cx*cz+sx*sy*sz   cy*sx
	//        cx*cz*sy+sx*sz  -cz*sx+cx*sy*sz   cx*cy
	pAngle = Math::ASin(-m[0][2]);
	if (pAngle < real(Math::HALF_PI)) {
		if (pAngle > real(-Math::HALF_PI)) {
			yAngle = Math::ATan2(m[0][1], m[0][0]);
			rAngle = Math::ATan2(m[1][2], m[2][2]);
			return true;
		}
		else {
			// WARNING.  Not a unique solution.
			real rmY = Math::ATan2(-m[1][0], m[2][0]);
			rAngle = real(0.0);  // any angle works
			yAngle = rAngle - rmY;
			return false;
		}
	}
	else {
		// WARNING.  Not a unique solution.
		real rpY = Math::ATan2(-m[1][0], m[2][0]);
		rAngle = real(0.0);  // any angle works
		yAngle = rpY - rAngle;
		return false;
	}
}

void Matrix3d::fromEulerAnglesXYZ (const real &yAngle, const real &pAngle,
																	const real &rAngle)
{
	real cosVal, sinVal;
	cosVal = Math::Cos(yAngle);
	sinVal = Math::Sin(yAngle);
	Matrix3d xMatr(1.0, 0.0, 0.0, 0.0, cosVal, sinVal, 0.0, -sinVal, cosVal);
	cosVal = Math::Cos(pAngle);
	sinVal = Math::Sin(pAngle);
	Matrix3d yMatr(cosVal, 0.0, -sinVal, 0.0, 1.0, 0.0, sinVal, 0.0, cosVal);
	cosVal = Math::Cos(rAngle);
	sinVal = Math::Sin(rAngle);
	Matrix3d zMatr(cosVal, sinVal, 0.0, -sinVal, cosVal, 0.0, 0.0, 0.0, 1.0);
	*this = xMatr * (yMatr * zMatr);
}

void Matrix3d::fromEulerAnglesXZY (const real &yAngle, const real &pAngle,
																	const real &rAngle)
{
	real cosVal, sinVal;
	cosVal = Math::Cos(yAngle);
	sinVal = Math::Sin(yAngle);
	Matrix3d xMatr(1.0, 0.0, 0.0, 0.0, cosVal, sinVal, 0.0, -sinVal, cosVal);
	cosVal = Math::Cos(pAngle);
	sinVal = Math::Sin(pAngle);
	Matrix3d zMatr(cosVal, sinVal, 0.0, -sinVal, cosVal, 0.0, 0.0, 0.0, 1.0);
	cosVal = Math::Cos(rAngle);
	sinVal = Math::Sin(rAngle);
	Matrix3d yMatr(cosVal, 0.0, -sinVal, 0.0, 1.0, 0.0, sinVal, 0.0, cosVal);
	*this = xMatr * (zMatr * yMatr);
}

void Matrix3d::fromEulerAnglesYXZ (const real &yAngle, const real &pAngle,
																	const real &rAngle)
{
	real cosVal, sinVal;
	cosVal = Math::Cos(yAngle);
	sinVal = Math::Sin(yAngle);
	Matrix3d yMatr(cosVal, 0.0, -sinVal, 0.0, 1.0, 0.0, sinVal, 0.0, cosVal);
	cosVal = Math::Cos(pAngle);
	sinVal = Math::Sin(pAngle);
	Matrix3d xMatr(1.0, 0.0, 0.0, 0.0, cosVal, sinVal, 0.0, -sinVal, cosVal);
	cosVal = Math::Cos(rAngle);
	sinVal = Math::Sin(rAngle);
	Matrix3d zMatr(cosVal, sinVal, 0.0, -sinVal, cosVal, 0.0, 0.0, 0.0, 1.0);
	*this = yMatr * (xMatr * zMatr);
}

void Matrix3d::fromEulerAnglesYZX (const real &yAngle, const real &pAngle,
																	const real &rAngle)
{
	real cosVal, sinVal;
	cosVal = Math::Cos(yAngle);
	sinVal = Math::Sin(yAngle);
	Matrix3d yMatr(cosVal, 0.0, -sinVal, 0.0, 1.0, 0.0, sinVal, 0.0, cosVal);
	cosVal = Math::Cos(pAngle);
	sinVal = Math::Sin(pAngle);
	Matrix3d zMatr(cosVal, sinVal, 0.0, -sinVal, cosVal, 0.0, 0.0, 0.0, 1.0);
	cosVal = Math::Cos(rAngle);
	sinVal = Math::Sin(rAngle);
	Matrix3d xMatr(1.0, 0.0, 0.0, 0.0, cosVal, sinVal, 0.0, -sinVal, cosVal);
	*this = yMatr * (zMatr * xMatr);
}

void Matrix3d::fromEulerAnglesZXY (const real &yAngle, const real &pAngle,
																	const real &rAngle)
{
	real cosVal, sinVal;
	cosVal = Math::Cos(yAngle);
	sinVal = Math::Sin(yAngle);
	Matrix3d zMatr(cosVal, sinVal, 0.0, -sinVal, cosVal, 0.0, 0.0, 0.0, 1.0);
	cosVal = Math::Cos(pAngle);
	sinVal = Math::Sin(pAngle);
	Matrix3d xMatr(1.0, 0.0, 0.0, 0.0, cosVal, sinVal, 0.0, -sinVal, cosVal);
	cosVal = Math::Cos(rAngle);
	sinVal = Math::Sin(rAngle);
	Matrix3d yMatr(cosVal, 0.0, -sinVal, 0.0, 1.0, 0.0, sinVal, 0.0, cosVal);
	*this = zMatr * (xMatr * yMatr);
}

void Matrix3d::fromEulerAnglesZYX (const real &yAngle, const real &pAngle,
																	const real &rAngle)
{
	real cosVal, sinVal;
	cosVal = Math::Cos(yAngle);
	sinVal = Math::Sin(yAngle);
	Matrix3d zMatr(cosVal, sinVal, 0.0, -sinVal, cosVal, 0.0, 0.0, 0.0, 1.0);
	cosVal = Math::Cos(pAngle);
	sinVal = Math::Sin(pAngle);
	Matrix3d yMatr(cosVal, 0.0, -sinVal, 0.0, 1.0, 0.0, sinVal, 0.0, cosVal);
	cosVal = Math::Cos(rAngle);
	sinVal = Math::Sin(rAngle);
	Matrix3d xMatr(1.0, 0.0, 0.0, 0.0, cosVal, sinVal, 0.0, -sinVal, cosVal);
	*this = zMatr * (yMatr * xMatr);
}

}

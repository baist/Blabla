#include "Vector4d.hpp"
#include "Math.hpp"

namespace bge
{
Vector4d::Vector4d():
	x(0.0f),
	y(0.0f),
	z(0.0f),
	w(0.0f)
{
}

Vector4d::Vector4d(real valX, real valY, real valZ, real valW):
	x(valX),
	y(valY),
	z(valZ),
	w(valW)
{
}

Vector4d::Vector4d(const real val[4]):
	x(val[0]),
	y(val[1]),
	z(val[2]),
	w(val[3])
{
}

Vector4d::Vector4d(real *const val):
	x(val[0]),
	y(val[1]),
	z(val[2]),
	w(val[3])
{
}

Vector4d::Vector4d(real scalar):
	x(scalar),
	y(scalar),
	z(scalar),
	w(scalar)
{
}

Vector4d::Vector4d(const Vector3d &vec, real valW):
	x(vec.x),
	y(vec.y),
	z(vec.z),
	w(valW)
{
}

real Vector4d::operator [] (const size_t i) const
{
	// TODO: handle error i>3
	return *(&x + i);
}

real &Vector4d::operator [] (const size_t i)
{
	// TODO: handle error i>3
	return *(&x + i);
}

Vector4d &Vector4d::operator = (const Vector4d &vec)
{
	x = vec.x;
	y = vec.y;
	z = vec.z;
	w = vec.w;
	return *this;
}

Vector4d &Vector4d::operator = (real scalar)
{
	x = scalar;
	y = scalar;
	z = scalar;
	w = scalar;
	return *this;
}

bool Vector4d::operator == (const Vector4d &vec) const
{
	return ( x == vec.x &&
					 y == vec.y &&
					 z == vec.z &&
					 w == vec.w );
}

bool Vector4d::operator != (const Vector4d &vec) const
{
	return ( x != vec.x ||
					 y != vec.y ||
					 z != vec.z ||
					 w != vec.w );
}

Vector4d &Vector4d::operator = (const Vector3d &vec)
{
	x = vec.x;
	y = vec.y;
	z = vec.z;
	w = 1.0f;
	return *this;
}

// arithmetic operations
Vector4d Vector4d::operator + (const Vector4d &vec) const {
	return Vector4d(x + vec.x, y + vec.y, z + vec.z, w + vec.w);
}

Vector4d Vector4d::operator - (const Vector4d &vec) const {
	return Vector4d(x - vec.x, y - vec.y, z - vec.z, w - vec.w);
}

Vector4d Vector4d::operator * (real scalar) const {
	return Vector4d(x * scalar, y * scalar, z * scalar, w * scalar);
}

Vector4d Vector4d::operator * (const Vector4d &vec) const {
	return Vector4d(vec.x * x, vec.y * y, vec.z * z, vec.w * w);
}

Vector4d Vector4d::operator / (real scalar) const {
	// TODO: handle error scalar=0
	real invScalar = 1.0f / scalar;
	return Vector4d(x * invScalar,y * invScalar, z * invScalar, w * invScalar);
}

Vector4d Vector4d::operator / (const Vector4d &vec) const {
	return Vector4d(x / vec.x, y / vec.y, z / vec.z, w / vec.w);
}

const Vector4d &Vector4d::operator + () const {
	return *this;
}

Vector4d Vector4d::operator - () const {
	return Vector4d(-x, -y, -z, -w);
}
// arithmetic updates
Vector4d &Vector4d::operator += (const Vector4d &vec) {
	x += vec.x;
	y += vec.y;
	z += vec.z;
	w += vec.w;
	return *this;
}

Vector4d &Vector4d::operator -= (const Vector4d &vec) {
	x -= vec.x;
	y -= vec.y;
	z -= vec.z;
	w -= vec.w;
	return *this;
}

Vector4d &Vector4d::operator *= (real scalar) {
	x *= scalar;
	y *= scalar;
	z *= scalar;
	w *= scalar;
	return *this;
}

Vector4d &Vector4d::operator += (real scalar) {
	x += scalar;
	y += scalar;
	z += scalar;
	w += scalar;
	return *this;
}

Vector4d &Vector4d::operator -= (real scalar) {
	x -= scalar;
	y -= scalar;
	z -= scalar;
	w -= scalar;
	return *this;
}

Vector4d &Vector4d::operator *= (const Vector4d &vec) {
	x *= vec.x;
	y *= vec.y;
	z *= vec.z;
	w *= vec.w;
	return *this;
}

Vector4d &Vector4d::operator /= (real scalar) {
	// TODO: handle error scalar=0
	real invScalar = 1.0f / scalar;
	x *= invScalar;
	y *= invScalar;
	z *= invScalar;
	w *= invScalar;
	return *this;
}

Vector4d &Vector4d::operator /= (const Vector4d &vec) {
	x /= vec.x;
	y /= vec.y;
	z /= vec.z;
	w /= vec.w;
	return *this;
}

real Vector4d::dotProduct(const Vector4d &vec) const
{
	return x * vec.x + y * vec.y + z * vec.z + w * vec.w;
}

}

#include "Vector3d.hpp"
#include "Math.hpp"

namespace bge
{

Vector3d::Vector3d()
{
}

Vector3d::Vector3d(real valX, real valY, real valZ):
	x(valX),
	y(valY),
	z(valZ)
{
}

Vector3d::Vector3d(const real val[3]):
	x(val[0]),
	y(val[1]),
	z(val[2])
{
}

Vector3d::Vector3d(real * const val)
	: x(val[0]), y(val[1]), z(val[2])
{
}

Vector3d::Vector3d(real scalar):
	x(scalar),
	y(scalar),
	z(scalar)
{
}

real Vector3d::operator [] (size_t i ) const
{
	// TODO: handle error i<3
	return *(&x + i);
}

real & Vector3d::operator [] (size_t i )
{
	// TODO: handle error i<3
	return *(&x + i);
}

Vector3d & Vector3d::operator = (const Vector3d & vec)
{
	x = vec.x;
	y = vec.y;
	z = vec.z;
	return *this;
}

Vector3d & Vector3d::operator = (real scalar)
{
	x = scalar;
	y = scalar;
	z = scalar;
	return *this;
}

bool Vector3d::operator == (const Vector3d & vec) const
{
	return (x == vec.x && y == vec.y && z == vec.z);
}

bool Vector3d::operator != (const Vector3d & vec) const
{
	return (x != vec.x || y != vec.y || z != vec.z);
}

bool Vector3d::operator < (const Vector3d & vec) const
{
	if(x < vec.x && y < vec.y && z < vec.z) {
		return true;
	}
	return false;
}

bool Vector3d::operator > (const Vector3d & vec) const
{
	if(x > vec.x && y > vec.y && z > vec.z) {
		return true;
	}
	return false;
}

Vector3d Vector3d::operator + (const Vector3d & vec) const
{
	return Vector3d(x + vec.x, y + vec.y, z + vec.z);
}


Vector3d Vector3d::operator - (const Vector3d & vec) const
{
	return Vector3d(x - vec.x, y - vec.y, z - vec.z);
}

Vector3d Vector3d::operator * (real scalar) const
{
	return Vector3d(x * scalar, y * scalar, z * scalar);
}


Vector3d Vector3d::operator * (const Vector3d & vec) const
{
	return Vector3d(x * vec.x, y * vec.y, z * vec.z);
}

Vector3d Vector3d::operator / (real scalar) const
{
	// TODO: handle error scalar
	real invScalar = 1.0f / scalar;
	return Vector3d(x * invScalar, y * invScalar, z * invScalar);
}

Vector3d Vector3d::operator / (const Vector3d & vec) const
{
	return Vector3d(x / vec.x, y / vec.y, z / vec.z);
}

const Vector3d & Vector3d::operator + () const
{
	return *this;
}

Vector3d Vector3d::operator - () const
{
	return Vector3d(-x, -y, -z);
}

Vector3d & Vector3d::operator += (const Vector3d & vec)
{
	x += vec.x;
	y += vec.y;
	z += vec.z;
	return *this;
}

Vector3d & Vector3d::operator += (real scalar)
{
	x += scalar;
	y += scalar;
	z += scalar;
	return *this;
}

Vector3d & Vector3d::operator -= (const Vector3d & vec)
{
	x -= vec.x;
	y -= vec.y;
	z -= vec.z;
	return *this;
}

Vector3d & Vector3d::operator -= (real scalar)
{
	x -= scalar;
	y -= scalar;
	z -= scalar;
	return *this;
}

Vector3d & Vector3d::operator *= (real scalar)
{
	x *= scalar;
	y *= scalar;
	z *= scalar;
	return *this;
}

Vector3d & Vector3d::operator *= (const Vector3d & vec)
{
	x *= vec.x;
	y *= vec.y;
	z *= vec.z;
	return *this;
}

Vector3d & Vector3d::operator /= (real scalar)
{
	// TODO: handle error scalar=0
	real invScalar = 1.0f / scalar;
	x *= invScalar;
	y *= invScalar;
	z *= invScalar;
	return *this;
}

Vector3d & Vector3d::operator /= (const Vector3d & vec)
{
	x /= vec.x;
	y /= vec.y;
	z /= vec.z;
	return *this;
}

bool Vector3d::isZeroLength() const
{
	real sqlen = (x * x) + (y * y) + (z * z);
	return (sqlen < (1e-06 * 1e-06));
}

real Vector3d::length () const
{
	return Math::Sqrt(x * x + y * y + z * z);
}

real Vector3d::dotProduct(const Vector3d & vec) const
{
	return x * vec.x + y * vec.y + z * vec.z;
}

real Vector3d::normalise()
{
	real len = Math::Sqrt(x * x + y * y + z * z);
	if (len > real(0.0f) ) {
		real invLen = 1.0f / len;
		x *= invLen;
		y *= invLen;
		z *= invLen;
	}
	return len;
}

Vector3d Vector3d::crossProduct(const Vector3d & vec) const
{
	return Vector3d(y * vec.z - z * vec.y,
									z * vec.x - x * vec.z,
									x * vec.y - y * vec.x);
}

Vector3d Vector3d::getNormalised() const
{
	Vector3d ret = *this;
	ret.normalise();
	return ret;
}

}


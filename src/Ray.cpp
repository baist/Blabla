#include "Ray.hpp"

namespace bge
{

Ray::Ray():
	m_direction(Vector3d(0.0f, 0.0f, 1.0f))
{
}

Ray::Ray(const Vector3d &origin, const Vector3d &direction):
	m_origin(origin),
	m_direction(direction)
{
}

void Ray::setOrigin(const Vector3d &origin)
{
	m_origin = origin;
}

const Vector3d &Ray::getOrigin() const
{
	return m_origin;
}

void Ray::setDirection(const Vector3d &dir)
{
	m_direction = dir;
}

const Vector3d &Ray::getDirection() const
{
	return m_direction;
}

Vector3d Ray::getPoint(real t) const
{
	return Vector3d(m_origin + (m_direction * t));
}


Vector3d Ray::operator*(real t) const
{
	return getPoint(t);
}

std::pair<bool, real> Ray::intersects(const Plane &p) const
{
	return Math::Intersects(*this, p);
}

}

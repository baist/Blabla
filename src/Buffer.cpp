#include <stdexcept>
#include "Buffer.hpp"

namespace bge
{

Buffer::Buffer(size_t size):
	m_locked(false),
	m_size(size)
{
	if(m_size == 0) {
		throw std::runtime_error(std::string("Zero lenght of buffer"));
	}
	m_p_data = new unsigned char[size];
}

Buffer::~Buffer()
{
	delete[] m_p_data;
}

size_t Buffer::getSize() const
{
	return m_size;
}

void Buffer::map(const void **pp_data) const
{
	if(m_locked) {
		return;
	}
	*pp_data = m_p_data;
}

void Buffer::unmap() const
{
}

void Buffer::map(void **pp_data)
{
	if(m_locked) {
		return;
	}
	m_locked = true;
	*pp_data = m_p_data;
}

void Buffer::unmap()
{
	m_locked = false;
}

}

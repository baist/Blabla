#include "Vector2d.hpp"
#include "Math.hpp"

namespace bge
{

Vector2d::Vector2d():
	x(0.0f),
	y(0.0f)
{
}

Vector2d::Vector2d(real valX, real valY):
	x(valX),
	y(valY)
{
}

Vector2d::Vector2d(real scalar):
	x(scalar),
	y(scalar)
{
}

Vector2d::Vector2d(const real val[2]):
	x(val[0]),
	y(val[1])
{
}

Vector2d::Vector2d(real * const val):
	x(val[0]),
	y(val[1])
{
}

real Vector2d::Vector2d::operator [] (size_t i) const
{
	// TODO: handle error i>1
	return *(&x + i);
}

real & Vector2d::Vector2d::operator [] (size_t i)
{
	// TODO: handle error i>1
	return *(&x + i);
}

Vector2d & Vector2d::Vector2d::operator = (const Vector2d & vec)
{
	x = vec.x;
	y = vec.y;
	return *this;
}

Vector2d & Vector2d::operator = (real scalar)
{
	x = scalar;
	y = scalar;
	return *this;
}

bool Vector2d::operator == (const Vector2d & vec) const
{
	return (x == vec.x && y == vec.y);
}

bool Vector2d::operator != (const Vector2d & vec) const
{
	return (x != vec.x || y != vec.y );
}

bool Vector2d::operator < (const Vector2d & vec) const
{
	if(x < vec.x && y < vec.y) {
		return true;
	}
	return false;
}

bool Vector2d::operator > (const Vector2d & vec) const
{
	if(x > vec.x && y > vec.y) {
		return true;
	}
	return false;
}

Vector2d Vector2d::operator + (const Vector2d & vec) const
{
	return Vector2d(x + vec.x, y + vec.y);
}

Vector2d Vector2d::operator - (const Vector2d & vec) const
{
	return Vector2d(x - vec.x, y - vec.y);
}

Vector2d Vector2d::operator * (real scalar) const
{
	return Vector2d(x * scalar, y * scalar);
}

Vector2d Vector2d::operator * (const Vector2d & vec) const
{
	return Vector2d(x * vec.x, y * vec.y);
}

Vector2d Vector2d::operator / (real scalar) const
{
	// TODO: handle error scalar=0
	real invScalar = 1.0f / scalar;
	return Vector2d(x * invScalar, y * invScalar);
}

Vector2d Vector2d::operator / (const Vector2d & vec) const
{
	return Vector2d(x / vec.x, y / vec.y);
}

const Vector2d & Vector2d::operator + () const
{
	return *this;
}

Vector2d Vector2d::operator - () const
{
	return Vector2d(-x, -y);
}

Vector2d & Vector2d::operator += (const Vector2d & vec)
{
	x += vec.x;
	y += vec.y;
	return *this;
}

Vector2d & Vector2d::operator += (real scalar)
{
	x += scalar;
	y += scalar;
	return *this;
}

Vector2d & Vector2d::operator -= (const Vector2d & vec)
{
	x -= vec.x;
	y -= vec.y;
	return *this;
}

Vector2d & Vector2d::operator -= (real scalar)
{
	x -= scalar;
	y -= scalar;
	return *this;
}
Vector2d & Vector2d::operator *= (real scalar)
{
	x *= scalar;
	y *= scalar;
	return *this;
}
Vector2d & Vector2d::operator *= (const Vector2d & vec)
{
	x *= vec.x;
	y *= vec.y;
	return *this;
}
Vector2d & Vector2d::operator /= (real scalar)
{
	// TODO: handle error scalar=0
	real invScalar = 1.0f / scalar;
	x *= invScalar;
	y *= invScalar;
	return *this;
}
Vector2d & Vector2d::operator /= (const Vector2d & vec)
{
	x /= vec.x;
	y /= vec.y;
	return *this;
}
bool Vector2d::isZeroLength() const
{
	real sqlen = (x * x) + (y * y);
	return (sqlen < (1e-06 * 1e-06));
}
real Vector2d::length () const
{
	return Math::Sqrt(x * x + y * y);
}
real Vector2d::dotProduct(const Vector2d & vec) const
{
	return x * vec.x + y * vec.y;
}
real Vector2d::normalise()
{
	real len = Math::Sqrt( x * x + y * y);
	if (len > real(0.0f)) {
		real invLen = 1.0f / len;
		x *= invLen;
		y *= invLen;
	}
	return len;
}
real Vector2d::crossProduct(const Vector2d & vec) const
{
	return x * vec.y - y * vec.x;
}
Vector2d Vector2d::getNormalised() const
{
	Vector2d ret = *this;
	ret.normalise();
	return ret;
}

}

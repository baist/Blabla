#include "Quaternion.hpp"
#include "Math.hpp"
#include "Matrix3d.hpp"
#include "Vector3d.hpp"

namespace bge
{

const real Quaternion::EPSILON = 1e-03;
const Quaternion Quaternion::ZERO(0, 0, 0, 0);
const Quaternion Quaternion::IDENTITY(1, 0, 0, 0);

Quaternion Quaternion::Slerp (real fT, const Quaternion &rkP,
															const Quaternion &q, bool shortestPath)
{
	real fCos = rkP.Dot(q);
	Quaternion rkT;
	// Do we need to invert rotation?
	if (fCos < 0.0f && shortestPath) {
		fCos = -fCos;
		rkT = -q;
	}
	else {
		rkT = q;
	}
	if (Math::Abs(fCos) < 1 - EPSILON) {
		// Standard case (slerp)
		real fSin = Math::Sqrt(1 - Math::Sqr(fCos));
		real fAngle = Math::ATan2(fSin, fCos);
		real fInvSin = 1.0f / fSin;
		real fCoeff0 = Math::Sin((1.0f - fT) * fAngle) * fInvSin;
		real fCoeff1 = Math::Sin(fT * fAngle) * fInvSin;
		return fCoeff0 * rkP + fCoeff1 * rkT;
	}
	else {
		// There are two situations:
		// 1. "rkP" and "q" are very close (fCos ~= +1), so we can do a linear
		//    interpolation safely.
		// 2. "rkP" and "q" are almost inverse of each other (fCos ~= -1), there
		//    are an infinite number of possibilities interpolation. but we haven't
		//    have method to fix this case, so just use linear interpolation here.
		Quaternion t = (1.0f - fT) * rkP + fT * rkT;
		// taking the complement requires renormalisation
		t.normalise();
		return t;
	}
}

Quaternion Quaternion::SlerpExtraSpins (real fT,
																				const Quaternion &rkP,
																				const Quaternion &q, int iExtraSpins)
{
	real fCos = rkP.Dot(q);
	real fAngle ( Math::ACos(fCos) );
	if ( Math::Abs(fAngle) < EPSILON ) {
		return rkP;
	}
	real fSin = Math::Sin(fAngle);
	real fPhase ( Math::PI * iExtraSpins * fT );
	real fInvSin = 1.0f / fSin;
	real fCoeff0 = Math::Sin((1.0f - fT) * fAngle - fPhase) * fInvSin;
	real fCoeff1 = Math::Sin(fT * fAngle + fPhase) * fInvSin;
	return fCoeff0 * rkP + fCoeff1 * q;
}

void Quaternion::Intermediate (const Quaternion &q0,
															 const Quaternion &q1, const Quaternion &q2,
															 Quaternion &rkA, Quaternion &rkB)
{
	// assert:  q0, q1, q2 are unit quaternions
	Quaternion kQ0inv = q0.UnitInverse();
	Quaternion kQ1inv = q1.UnitInverse();
	Quaternion rkP0 = kQ0inv * q1;
	Quaternion rkP1 = kQ1inv * q2;
	Quaternion kArg = 0.25 * (rkP0.Log() - rkP1.Log());
	Quaternion kMinusArg = -kArg;
	rkA = q1 * kArg.Exp();
	rkB = q1 * kMinusArg.Exp();
}

Quaternion Quaternion::Squad (real fT,
															const Quaternion &rkP, const Quaternion &rkA,
															const Quaternion &rkB, const Quaternion &q, bool shortestPath)
{
	real fSlerpT = 2.0f * fT * (1.0f - fT);
	Quaternion kSlerpP = Slerp(fT, rkP, q, shortestPath);
	Quaternion kSlerpQ = Slerp(fT, rkA, rkB);
	return Slerp(fSlerpT, kSlerpP , kSlerpQ);
}

Quaternion Quaternion::nlerp(real fT, const Quaternion &rkP,
														 const Quaternion &q, bool shortestPath)
{
	Quaternion result;
	real fCos = rkP.Dot(q);
	if (fCos < 0.0f && shortestPath) {
		result = rkP + fT * ((-q) - rkP);
	}
	else {
		result = rkP + fT * (q - rkP);
	}
	result.normalise();
	return result;
}

Quaternion::Quaternion (): w(1), x(0), y(0), z(0)
{
}

Quaternion::Quaternion (real valW, real valX, real valY, real valZ):
	w(valW), x(valX), y(valY), z(valZ)
{
}

Quaternion::Quaternion(const Matrix3d &rot)
{
	this->FromRotationMatrix(rot);
}

Quaternion::Quaternion(const real &angle, const Vector3d &axis)
{
	this->FromAngleAxis(angle, axis);
}

Quaternion::Quaternion(const Vector3d &xaxis, const Vector3d &yaxis,
											 const Vector3d &zaxis)
{
	this->FromAxes(xaxis, yaxis, zaxis);
}

Quaternion::Quaternion(const Vector3d *axis)
{
	this->FromAxes(axis);
}

Quaternion::Quaternion(real *valptr)
{
	w = valptr[0];
	x = valptr[1];
	y = valptr[2];
	z = valptr[3];
}

real Quaternion::operator [] (const size_t i) const
{
	// TODO: handle error i>3
	return *(&w + i);
}

real &Quaternion::operator [] (const size_t i)
{
	// TODO: handle error i>3
	return *(&w + i);
}

bool Quaternion::operator== (const Quaternion &rhs) const
{
	return (rhs.x == x) && (rhs.y == y) &&
				 (rhs.z == z) && (rhs.w == w);
}
bool Quaternion::operator!= (const Quaternion &rhs) const
{
	return !operator==(rhs);
}

Quaternion &Quaternion::operator= (const Quaternion &q)
{
	w = q.w;
	x = q.x;
	y = q.y;
	z = q.z;
	return *this;
}

Quaternion Quaternion::operator+ (const Quaternion &q) const
{
	return Quaternion(w + q.w, x + q.x, y + q.y, z + q.z);
}

Quaternion Quaternion::operator- (const Quaternion &q) const
{
	return Quaternion(w - q.w, x - q.x, y - q.y, z - q.z);
}

Quaternion Quaternion::operator* (const Quaternion &q) const
{
	// NOTE:  Multiplication is not generally commutative, so in most
	// cases p*q != q*p.
	return Quaternion
				 (
						 w * q.w - x * q.x - y * q.y - z * q.z,
						 w * q.x + x * q.w + y * q.z - z * q.y,
						 w * q.y + y * q.w + z * q.x - x * q.z,
						 w * q.z + z * q.w + x * q.y - y * q.x
				 );
}

Quaternion Quaternion::operator* (real fScalar) const
{
	return Quaternion(fScalar * w, fScalar * x, fScalar * y, fScalar * z);
}

Quaternion Quaternion::operator- () const
{
	return Quaternion(-w, -x, -y, -z);
}

Vector3d Quaternion::operator* (const Vector3d &vec) const
{
	// nVidia SDK implementation
	Vector3d uv, uuv;
	Vector3d qvec(x, y, z);
	uv = qvec.crossProduct(vec);
	uuv = qvec.crossProduct(uv);
	uv *= (2.0f * w);
	uuv *= 2.0f;
	return vec + uv + uuv;
}

bool Quaternion::isNaN() const
{
	return Math::isNaN(x) || Math::isNaN(y) || Math::isNaN(z) || Math::isNaN(w);
}

real Quaternion::getRoll(bool reprojectAxis) const
{
	if (reprojectAxis) {
		// roll = atan2(localx.y, localx.x)
		// pick parts of xAxis() implementation that we need
		//      Real fTx  = 2.0*x;
		real fTy  = 2.0f * y;
		real fTz  = 2.0f * z;
		real fTwz = fTz * w;
		real fTxy = fTy * x;
		real fTyy = fTy * y;
		real fTzz = fTz * z;
		// Vector3(1.0-(fTyy+fTzz), fTxy+fTwz, fTxz-fTwy);
		return real(Math::ATan2(fTxy + fTwz, 1.0f - (fTyy + fTzz)));
	}
	else {
		return real(Math::ATan2(2 * (x * y + w * z), w * w + x * x - y * y - z * z));
	}
}

real Quaternion::getPitch(bool reprojectAxis) const
{
	if (reprojectAxis) {
		// pitch = atan2(localy.z, localy.y)
		// pick parts of yAxis() implementation that we need
		real fTx  = 2.0f * x;
		//      Real fTy  = 2.0f*y;
		real fTz  = 2.0f * z;
		real fTwx = fTx * w;
		real fTxx = fTx * x;
		real fTyz = fTz * y;
		real fTzz = fTz * z;
		// Vector3(fTxy-fTwz, 1.0-(fTxx+fTzz), fTyz+fTwx);
		return real(Math::ATan2(fTyz + fTwx, 1.0f - (fTxx + fTzz)));
	}
	else {
		// internal version
		return real(Math::ATan2(2 * (y * z + w * x), w * w - x * x - y * y + z * z));
	}
}

real Quaternion::getYaw(bool reprojectAxis) const
{
	if (reprojectAxis) {
		// yaw = atan2(localz.x, localz.z)
		// pick parts of zAxis() implementation that we need
		real fTx  = 2.0f * x;
		real fTy  = 2.0f * y;
		real fTz  = 2.0f * z;
		real fTwy = fTy * w;
		real fTxx = fTx * x;
		real fTxz = fTz * x;
		real fTyy = fTy * y;
		// Vector3(fTxz+fTwy, fTyz-fTwx, 1.0-(fTxx+fTyy));
		return real(Math::ATan2(fTxz + fTwy, 1.0f - (fTxx + fTyy)));
	}
	else {
		// internal version
		return real(Math::ASin(-2 * (x * z - w * y)));
	}
}

void Quaternion::FromRotationMatrix (const Matrix3d &rot)
{
	// Algorithm in Ken Shoemake's article in 1987 SIGGRAPH course notes
	// article "Quaternion Calculus and Fast Animation".
	real fTrace = rot[0][0] + rot[1][1] + rot[2][2];
	real fRoot;
	if ( fTrace > 0.0 ) {
		// |w| > 1/2, may as well choose w > 1/2
		fRoot = Math::Sqrt(fTrace + 1.0f);  // 2w
		w = 0.5f * fRoot;
		fRoot = 0.5f / fRoot; // 1/(4w)
		x = (rot[1][2] - rot[2][1]) * fRoot;
		y = (rot[2][0] - rot[0][2]) * fRoot;
		z = (rot[0][1] - rot[1][0]) * fRoot;
	}
	else {
		// |w| <= 1/2
		static size_t s_iNext[3] = { 1, 2, 0 };
		size_t i = 0;
		if ( rot[1][1] > rot[0][0] ) {
			i = 1;
		}
		if ( rot[2][2] > rot[i][i] ) {
			i = 2;
		}
		size_t j = s_iNext[i];
		size_t k = s_iNext[j];
		fRoot = Math::Sqrt(rot[i][i] - rot[j][j] - rot[k][k] + 1.0f);
		real *apkQuat[3] = { &x, &y, &z };
		*apkQuat[i] = 0.5f * fRoot;
		fRoot = 0.5f / fRoot;
		w = (rot[j][k] - rot[k][j]) * fRoot;
		*apkQuat[j] = (rot[i][j] + rot[j][i]) * fRoot;
		*apkQuat[k] = (rot[i][k] + rot[k][i]) * fRoot;
	}
}

void Quaternion::ToRotationMatrix (Matrix3d &rot) const
{
	real fTx  = x + x;
	real fTy  = y + y;
	real fTz  = z + z;
	real fTwx = fTx * w;
	real fTwy = fTy * w;
	real fTwz = fTz * w;
	real fTxx = fTx * x;
	real fTxy = fTy * x;
	real fTxz = fTz * x;
	real fTyy = fTy * y;
	real fTyz = fTz * y;
	real fTzz = fTz * z;
	rot[0][0] = 1.0f - (fTyy + fTzz);
	rot[1][0] = fTxy - fTwz;
	rot[2][0] = fTxz + fTwy;
	rot[0][1] = fTxy + fTwz;
	rot[1][1] = 1.0f - (fTxx + fTzz);
	rot[2][1] = fTyz - fTwx;
	rot[0][2] = fTxz - fTwy;
	rot[1][2] = fTyz + fTwx;
	rot[2][2] = 1.0f - (fTxx + fTyy);
}

void Quaternion::FromAngleAxis (const real &angle,
																const Vector3d &axis)
{
	// assert:  axis[] is unit length
	//
	// The quaternion representing the rotation is
	//   q = cos(A/2)+sin(A/2)*(x*i+y*j+z*k)
	real fHalfAngle ( 0.5 * angle );
	real fSin = Math::Sin(fHalfAngle);
	w = Math::Cos(fHalfAngle);
	x = fSin * axis.x;
	y = fSin * axis.y;
	z = fSin * axis.z;
}

void Quaternion::ToAngleAxis (real &angle, Vector3d &axis) const
{
	// The quaternion representing the rotation is
	//   q = cos(A/2)+sin(A/2)*(x*i+y*j+z*k)
	real fSqrLength = x * x + y * y + z * z;
	if ( fSqrLength > 0.0 ) {
		angle = 2.0 * Math::ACos(w);
		real fInvLength = Math::InvSqrt(fSqrLength);
		axis.x = x * fInvLength;
		axis.y = y * fInvLength;
		axis.z = z * fInvLength;
	}
	else {
		// angle is 0 (mod 2*pi), so any axis will do
		angle = real(0.0);
		axis.x = 1.0;
		axis.y = 0.0;
		axis.z = 0.0;
	}
}

void Quaternion::FromAxes (const Vector3d *axis)
{
	Matrix3d rot;
	for (size_t col = 0; col < 3; col++) {
		rot[0][col] = axis[col].x;
		rot[1][col] = axis[col].y;
		rot[2][col] = axis[col].z;
	}
	FromRotationMatrix(rot);
}

void Quaternion::FromAxes (const Vector3d &xaxis, const Vector3d &yaxis,
													 const Vector3d &zaxis)
{
	Matrix3d kRot;
	kRot[0][0] = xaxis.x;
	kRot[0][1] = xaxis.y;
	kRot[0][2] = xaxis.z;
	kRot[1][0] = yaxis.x;
	kRot[1][1] = yaxis.y;
	kRot[1][2] = yaxis.z;
	kRot[2][0] = zaxis.x;
	kRot[2][1] = zaxis.y;
	kRot[2][2] = zaxis.z;
	FromRotationMatrix(kRot);
}

void Quaternion::ToAxes (Vector3d *axis) const
{
	Matrix3d rot;
	ToRotationMatrix(rot);
	for (size_t col = 0; col < 3; col++) {
		axis[col].x = rot[0][col];
		axis[col].y = rot[1][col];
		axis[col].z = rot[2][col];
	}
}

Vector3d Quaternion::xAxis() const
{
	//Real fTx  = 2.0*x;
	real fTy  = 2.0f * y;
	real fTz  = 2.0f * z;
	real fTwy = fTy * w;
	real fTwz = fTz * w;
	real fTxy = fTy * x;
	real fTxz = fTz * x;
	real fTyy = fTy * y;
	real fTzz = fTz * z;
	return Vector3d(1.0f - (fTyy + fTzz), fTxy + fTwz, fTxz - fTwy);
}

Vector3d Quaternion::yAxis() const
{
	real fTx  = 2.0f * x;
	real fTy  = 2.0f * y;
	real fTz  = 2.0f * z;
	real fTwx = fTx * w;
	real fTwz = fTz * w;
	real fTxx = fTx * x;
	real fTxy = fTy * x;
	real fTyz = fTz * y;
	real fTzz = fTz * z;
	return Vector3d(fTxy - fTwz, 1.0f - (fTxx + fTzz), fTyz + fTwx);
}

Vector3d Quaternion::zAxis() const
{
	real fTx  = 2.0f * x;
	real fTy  = 2.0f * y;
	real fTz  = 2.0f * z;
	real fTwx = fTx * w;
	real fTwy = fTy * w;
	real fTxx = fTx * x;
	real fTxz = fTz * x;
	real fTyy = fTy * y;
	real fTyz = fTz * y;
	return Vector3d(fTxz + fTwy, fTyz - fTwx, 1.0f - (fTxx + fTyy));
}

void Quaternion::ToAxes (Vector3d &xaxis, Vector3d &yaxis, Vector3d &zaxis) const
{
	Matrix3d kRot;
	ToRotationMatrix(kRot);
	xaxis.x = kRot[0][0];
	xaxis.y = kRot[0][1];
	xaxis.z = kRot[0][2];
	yaxis.x = kRot[1][0];
	yaxis.y = kRot[1][1];
	yaxis.z = kRot[1][2];
	zaxis.x = kRot[2][0];
	zaxis.y = kRot[2][1];
	zaxis.z = kRot[2][2];
}

real Quaternion::Dot (const Quaternion &q) const
{
	return w * q.w + x * q.x + y * q.y + z * q.z;
}

real Quaternion::Norm () const
{
	return w * w + x * x + y * y + z * z;
}

real Quaternion::normalise()
{
	real len = Norm();
	real factor = 1.0f / Math::Sqrt(len);
	*this = *this * factor;
	return len;
}

Quaternion Quaternion::Inverse () const
{
	real fNorm = w * w + x * x + y * y + z * z;
	if ( fNorm > 0.0 ) {
		real fInvNorm = 1.0f / fNorm;
		return Quaternion(w * fInvNorm, -x * fInvNorm, -y * fInvNorm, -z * fInvNorm);
	}
	else {
		// return an invalid result to flag the error
		return ZERO;
	}
}

Quaternion Quaternion::UnitInverse () const
{
	// assert:  'this' is unit length
	return Quaternion(w, -x, -y, -z);
}

Quaternion Quaternion::Exp () const
{
	// If q = A*(x*i+y*j+z*k) where (x,y,z) is unit length, then
	// exp(q) = cos(A)+sin(A)*(x*i+y*j+z*k).  If sin(A) is near zero,
	// use exp(q) = cos(A)+A*(x*i+y*j+z*k) since A/sin(A) has limit 1.
	real fAngle ( Math::Sqrt(x * x + y * y + z * z) );
	real fSin = Math::Sin(fAngle);
	Quaternion kResult;
	kResult.w = Math::Cos(fAngle);
	if ( Math::Abs(fSin) >= EPSILON ) {
		real fCoeff = fSin / fAngle;
		kResult.x = fCoeff * x;
		kResult.y = fCoeff * y;
		kResult.z = fCoeff * z;
	}
	else {
		kResult.x = x;
		kResult.y = y;
		kResult.z = z;
	}
	return kResult;
}

Quaternion Quaternion::Log () const
{
	// If q = cos(A)+sin(A)*(x*i+y*j+z*k) where (x,y,z) is unit length, then
	// log(q) = A*(x*i+y*j+z*k).  If sin(A) is near zero, use log(q) =
	// sin(A)*(x*i+y*j+z*k) since sin(A)/A has limit 1.
	Quaternion kResult;
	kResult.w = 0.0;
	if ( Math::Abs(w) < 1.0 ) {
		real fAngle ( Math::ACos(w) );
		real fSin = Math::Sin(fAngle);
		if ( Math::Abs(fSin) >= EPSILON ) {
			real fCoeff = fAngle / fSin;
			kResult.x = fCoeff * x;
			kResult.y = fCoeff * y;
			kResult.z = fCoeff * z;
			return kResult;
		}
	}
	kResult.x = x;
	kResult.y = y;
	kResult.z = z;
	return kResult;
}

bool Quaternion::equals(const Quaternion &rhs, const real &tolerance) const
{
	real fCos = Dot(rhs);
	real angle = Math::ACos(fCos);
	return (Math::Abs(angle) <= tolerance)
				 || Math::RealEqual(angle, Math::PI, tolerance);
}



}

#include "Plane.hpp"
#include "Matrix3d.hpp"

namespace bge
{

Plane::Plane ():
	d(0.0f)
{
}

Plane::Plane (const Plane &val)
{
	normal = val.normal;
	d = val.d;
}

Plane::Plane (const Vector3d &norm, real scalar)
{
	normal = norm;
	d = -scalar;
}

Plane::Plane (real a, real b, real c, real _d):
	normal(a, b, c),
	d(_d)
{
}

Plane::Plane (const Vector3d &norm, const Vector3d &point)
{
	redefine(norm, point);
}

Plane::Plane (const Vector3d &point0, const Vector3d &point1,
							const Vector3d &point2)
{
	redefine(point0, point1, point2);
}

bool Plane::operator==(const Plane &pln) const
{
	return (pln.d == d && pln.normal == normal);
}

bool Plane::operator!=(const Plane &pln) const
{
	return (pln.d != d || pln.normal != normal);
}

real Plane::getDistance (const Vector3d &point) const
{
	return normal.dotProduct(point) + d;
}

Plane::Side Plane::getSide (const Vector3d &point) const
{
	real dist = getDistance(point);
	if (dist < 0.0) {
		return Plane::NEGATIVE_SIDE;
	}
	if (dist > 0.0) {
		return Plane::POSITIVE_SIDE;
	}
	return Plane::NO_SIDE;
}

void Plane::redefine(const Vector3d &point0, const Vector3d &point1,
										 const Vector3d &point2)
{
	Vector3d edge1 = point1 - point0;
	Vector3d edge2 = point2 - point0;
	normal = edge1.crossProduct(edge2);
	normal.normalise();
	d = -normal.dotProduct(point0);
}

void Plane::redefine(const Vector3d &norm, const Vector3d &point)
{
	normal = norm;
	d = -normal.dotProduct(point);
}

real Plane::normalise(void)
{
	real len = normal.length();
	if (len > real(0.0f)) {
		real invLen = 1.0f / len;
		normal *= invLen;
		d *= invLen;
	}
	return len;
}

}

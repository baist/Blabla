#include "Resource.hpp"

namespace bge
{

Resource::Resource()
{
	m_name = "";
}

Resource::Resource(const String &name):
	m_name(name)
{
}

Resource::~Resource()
{
}

const String &Resource::getName() const
{
	return m_name;
}

}

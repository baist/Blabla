#include "ResourceManager.hpp"
#include <sstream>
#include <sys/time.h>

namespace bge
{

ResourceManager::ResourceManager()
{
}

ResourceManager::~ResourceManager()
{
	deleteAll();
}

const String ResourceManager::generateNewName(const String &suggestedName)
{
	std::stringstream ss;
	if(suggestedName.empty()) {
		ss << "noname";
	}
	else {
		ss << suggestedName;
	}
	if(isExist(String(ss.str()))) {
		// TODO: create separate function
		struct timeval now;
		gettimeofday(&now, 0);
		ss << "_";
		ss.setf(std::ios::hex);
		ss << now.tv_usec;
	}
	return String(ss.str());
}


void ResourceManager::create()
{
}

void ResourceManager::deleteAll()
{
	Resources::iterator it = m_resources.begin();
	Resources::iterator end = m_resources.end();
	for(;it != end; ++it) {
		delete it->second;
	}
	m_resources.clear();
}

bool ResourceManager::isExist(const String &name)
{
	Resources::iterator it = m_resources.find(name);
	if(it != m_resources.end()) {
		return true;
	}
	return false;
}

Resource* ResourceManager::createRes(const String &name)
{
	return 0;
}

void ResourceManager::addRes(Resource *p_res)
{
	if(p_res == 0) {
		return;
	}
	const String &name = p_res->getName();
	if(isExist(name)) {
		// TODO: print error
	}
	m_resources.insert(std::pair<String, Resource*>(name, p_res));
}

void ResourceManager::removeRes(Resource *p_res)
{
	if(p_res == 0) {
		return;
	}
	Resources::iterator it = m_resources.find(p_res->getName());
	if(it != m_resources.end()) {
		m_resources.erase(it);
	}
	else {
		// TODO: print error
	}
	//Resources::iterator it = m_resources.begin();
	//Resources::iterator end = m_resources.end();
	//for(;it != end; ++it) {
	//	if(it->second == p_res) {
	//		delete p_res;
	//		m_resources.erase(it);
	//		break;
	//	}
	//}
}

void ResourceManager::removeRes(const String &name)
{
	if(name.empty()) {
		return;
	}
	Resources::iterator it = m_resources.find(name);
	if(it != m_resources.end()) {
		m_resources.erase(it);
	}
	else {
		// TODO: print error
	}
}

}

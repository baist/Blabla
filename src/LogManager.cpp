
#include <ctime>
#include <iostream>
#include <iomanip>
#include "LogManager.hpp"

namespace bge
{

template<> LogManager * Singleton<LogManager>::p_staticInstance = 0;

LogManager::Stream LogManager::LogInfo()
{
	return LogManager::getInstPtr()->getStream();
}

LogManager::Stream LogManager::LogError()
{
	return LogManager::getInstPtr()->getStream();
}

LogManager::LogManager()
{
	tm *p_time;
	time_t ctTime;
	time(&ctTime);
	p_time = localtime(&ctTime);
	std::cout << "~~~ " <<
			std::setw(2) << std::setfill('0') << p_time->tm_mday << "." <<
			std::setw(2) << std::setfill('0') << p_time->tm_mon + 1 << "." <<
			std::setw(2) << std::setfill('0') << p_time->tm_year + 1900 <<
			" ~~~" << std::endl;
}

LogManager::~LogManager()
{
}

void LogManager::write(const String & msg)
{
	tm *p_time;
	time_t ctTime;
	time(&ctTime);
	p_time = localtime(&ctTime);
	std::cout << "---[" <<
			std::setw(2) << std::setfill('0') << p_time->tm_hour << ":" <<
			std::setw(2) << std::setfill('0') << p_time->tm_min << ":" <<
			std::setw(2) << std::setfill('0') << p_time->tm_sec <<
			"] ";
	std::cout << msg.c_str() << std::endl;
}

LogManager::Stream LogManager::getStream()
{
	return Stream(this);
}



}

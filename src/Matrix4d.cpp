#include "Matrix4d.hpp"
#include "Vector3d.hpp"
#include "Matrix3d.hpp"
#include "Quaternion.hpp"

namespace bge
{

const Matrix4d Matrix4d::ZERO(
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0 );

const Matrix4d Matrix4d::ZEROAFFINE(
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 1 );

const Matrix4d Matrix4d::IDENTITY(
    1, 0, 0, 0,
    0, 1, 0, 0,
    0, 0, 1, 0,
    0, 0, 0, 1 );

const Matrix4d Matrix4d::CLIPSPACE2DTOIMAGESPACE(
    0.5,    0,  0, 0.5,
    0, -0.5,  0, 0.5,
    0,    0,  1,   0,
    0,    0,  0,   1);

Matrix4d Matrix4d::getTrans(const Vector3d &vec)
{
	Matrix4d r;
	r.m[0][0] = 1.0;
	r.m[0][1] = 0.0;
	r.m[0][2] = 0.0;
	r.m[0][3] = 0.0;
	r.m[1][0] = 0.0;
	r.m[1][1] = 1.0;
	r.m[1][2] = 0.0;
	r.m[1][3] = 0.0;
	r.m[2][0] = 0.0;
	r.m[2][1] = 0.0;
	r.m[2][2] = 1.0;
	r.m[2][3] = 0.0;
	r.m[3][0] = vec.x;
	r.m[3][1] = vec.y;
	r.m[3][2] = vec.z;
	r.m[3][3] = 1.0;
	return r;
}

Matrix4d Matrix4d::getTrans(real x, real y, real z)
{
	Matrix4d r;
	r.m[0][0] = 1.0;
	r.m[0][1] = 0.0;
	r.m[0][2] = 0.0;
	r.m[0][3] = 0.0;
	r.m[1][0] = 0.0;
	r.m[1][1] = 1.0;
	r.m[1][2] = 0.0;
	r.m[1][3] = 0.0;
	r.m[2][0] = 0.0;
	r.m[2][1] = 0.0;
	r.m[2][2] = 1.0;
	r.m[2][3] = 0.0;
	r.m[3][0] = x;
	r.m[3][1] = y;
	r.m[3][2] = z;
	r.m[3][3] = 1.0;
	return r;
}

Matrix4d Matrix4d::getScale(const Vector3d &vec)
{
	Matrix4d r;
	r.m[0][0] = vec.x;
	r.m[0][1] = 0.0;
	r.m[0][2] = 0.0;
	r.m[0][3] = 0.0;
	r.m[1][0] = 0.0;
	r.m[1][1] = vec.y;
	r.m[1][2] = 0.0;
	r.m[1][3] = 0.0;
	r.m[2][0] = 0.0;
	r.m[2][1] = 0.0;
	r.m[2][2] = vec.z;
	r.m[2][3] = 0.0;
	r.m[3][0] = 0.0;
	r.m[3][1] = 0.0;
	r.m[3][2] = 0.0;
	r.m[3][3] = 1.0;
	return r;
}

Matrix4d Matrix4d::getScale(real x, real y, real z)
{
	Matrix4d r;
	r.m[0][0] = x;
	r.m[0][1] = 0.0;
	r.m[0][2] = 0.0;
	r.m[0][3] = 0.0;
	r.m[1][0] = 0.0;
	r.m[1][1] = y;
	r.m[1][2] = 0.0;
	r.m[1][3] = 0.0;
	r.m[2][0] = 0.0;
	r.m[2][1] = 0.0;
	r.m[2][2] = z;
	r.m[2][3] = 0.0;
	r.m[3][0] = 0.0;
	r.m[3][1] = 0.0;
	r.m[3][2] = 0.0;
	r.m[3][3] = 1.0;
	return r;
}

Matrix4d::Matrix4d()
{
	m[0][0] = 0.0f;
	m[0][1] = 0.0f;
	m[0][2] = 0.0f;
	m[0][3] = 0.0f;
	m[1][0] = 0.0f;
	m[1][1] = 0.0f;
	m[1][2] = 0.0f;
	m[1][3] = 0.0f;
	m[2][0] = 0.0f;
	m[2][1] = 0.0f;
	m[2][2] = 0.0f;
	m[2][3] = 0.0f;
	m[3][0] = 0.0f;
	m[3][1] = 0.0f;
	m[3][2] = 0.0f;
	m[3][3] = 0.0f;
}

Matrix4d::Matrix4d(
		real m00, real m01, real m02, real m03,
		real m10, real m11, real m12, real m13,
		real m20, real m21, real m22, real m23,
		real m30, real m31, real m32, real m33 )
{
	m[0][0] = m00;
	m[0][1] = m01;
	m[0][2] = m02;
	m[0][3] = m03;
	m[1][0] = m10;
	m[1][1] = m11;
	m[1][2] = m12;
	m[1][3] = m13;
	m[2][0] = m20;
	m[2][1] = m21;
	m[2][2] = m22;
	m[2][3] = m23;
	m[3][0] = m30;
	m[3][1] = m31;
	m[3][2] = m32;
	m[3][3] = m33;
}

Matrix4d::Matrix4d(const Matrix3d &matr)
{
	operator=(IDENTITY);
	operator=(matr);
}

Matrix4d::Matrix4d(const Quaternion &rot)
{
	Matrix3d matr;
	rot.ToRotationMatrix(matr);
	operator=(IDENTITY);
	operator=(matr);
}

real *Matrix4d::operator [] (size_t row)
{
	// TODO: handle error row > 3
	return m[row];
}

const real *Matrix4d::operator [] (size_t row) const
{
	// TODO: handle error row > 3
	return m[row];
}

Matrix4d Matrix4d::concatenate(const Matrix4d &matr) const
{
	Matrix4d r;
	r.m[0][0] = m[0][0] * matr.m[0][0] + m[0][1] * matr.m[1][0] +
			m[0][2] * matr.m[2][0] + m[0][3] * matr.m[3][0];
	r.m[0][1] = m[0][0] * matr.m[0][1] + m[0][1] * matr.m[1][1] +
			m[0][2] * matr.m[2][1] + m[0][3] * matr.m[3][1];
	r.m[0][2] = m[0][0] * matr.m[0][2] + m[0][1] * matr.m[1][2] +
			m[0][2] * matr.m[2][2] + m[0][3] * matr.m[3][2];
	r.m[0][3] = m[0][0] * matr.m[0][3] + m[0][1] * matr.m[1][3] +
			m[0][2] * matr.m[2][3] + m[0][3] * matr.m[3][3];
	r.m[1][0] = m[1][0] * matr.m[0][0] + m[1][1] * matr.m[1][0] +
			m[1][2] * matr.m[2][0] + m[1][3] * matr.m[3][0];
	r.m[1][1] = m[1][0] * matr.m[0][1] + m[1][1] * matr.m[1][1] +
			m[1][2] * matr.m[2][1] + m[1][3] * matr.m[3][1];
	r.m[1][2] = m[1][0] * matr.m[0][2] + m[1][1] * matr.m[1][2] +
			m[1][2] * matr.m[2][2] + m[1][3] * matr.m[3][2];
	r.m[1][3] = m[1][0] * matr.m[0][3] + m[1][1] * matr.m[1][3] +
			m[1][2] * matr.m[2][3] + m[1][3] * matr.m[3][3];
	r.m[2][0] = m[2][0] * matr.m[0][0] + m[2][1] * matr.m[1][0] +
			m[2][2] * matr.m[2][0] + m[2][3] * matr.m[3][0];
	r.m[2][1] = m[2][0] * matr.m[0][1] + m[2][1] * matr.m[1][1] +
			m[2][2] * matr.m[2][1] + m[2][3] * matr.m[3][1];
	r.m[2][2] = m[2][0] * matr.m[0][2] + m[2][1] * matr.m[1][2] +
			m[2][2] * matr.m[2][2] + m[2][3] * matr.m[3][2];
	r.m[2][3] = m[2][0] * matr.m[0][3] + m[2][1] * matr.m[1][3] +
			m[2][2] * matr.m[2][3] + m[2][3] * matr.m[3][3];
	r.m[3][0] = m[3][0] * matr.m[0][0] + m[3][1] * matr.m[1][0] +
			m[3][2] * matr.m[2][0] + m[3][3] * matr.m[3][0];
	r.m[3][1] = m[3][0] * matr.m[0][1] + m[3][1] * matr.m[1][1] +
			m[3][2] * matr.m[2][1] + m[3][3] * matr.m[3][1];
	r.m[3][2] = m[3][0] * matr.m[0][2] + m[3][1] * matr.m[1][2] +
			m[3][2] * matr.m[2][2] + m[3][3] * matr.m[3][2];
	r.m[3][3] = m[3][0] * matr.m[0][3] + m[3][1] * matr.m[1][3] +
			m[3][2] * matr.m[2][3] + m[3][3] * matr.m[3][3];
	return r;
}

Matrix4d Matrix4d::operator * (const Matrix4d &matr) const
{
	return concatenate(matr);
}

Vector3d Matrix4d::operator * (const Vector3d &vec) const
{
	Vector3d r;
	real invW = 1.0f / (m[3][0] * vec.x + m[3][1] * vec.y +
			m[3][2] * vec.z + m[3][3]);
	r.x = (m[0][0] * vec.x + m[0][1] * vec.y + m[0][2] * vec.z + m[0][3]) * invW;
	r.y = (m[1][0] * vec.x + m[1][1] * vec.y + m[1][2] * vec.z + m[1][3]) * invW;
	r.z = (m[2][0] * vec.x + m[2][1] * vec.y + m[2][2] * vec.z + m[2][3]) * invW;
	return r;
}

Vector4d Matrix4d::operator*(const Vector4d &vec) const
{
	return Vector4d(
			m[0][0] * vec.x + m[0][1] * vec.y + m[0][2] * vec.z + m[0][3] * vec.w,
			m[1][0] * vec.x + m[1][1] * vec.y + m[1][2] * vec.z + m[1][3] * vec.w,
			m[2][0] * vec.x + m[2][1] * vec.y + m[2][2] * vec.z + m[2][3] * vec.w,
			m[3][0] * vec.x + m[3][1] * vec.y + m[3][2] * vec.z + m[3][3] * vec.w
		);
}

Matrix4d Matrix4d::operator + (const Matrix4d &matr) const
{
	Matrix4d r;
	r.m[0][0] = m[0][0] + matr.m[0][0];
	r.m[0][1] = m[0][1] + matr.m[0][1];
	r.m[0][2] = m[0][2] + matr.m[0][2];
	r.m[0][3] = m[0][3] + matr.m[0][3];
	r.m[1][0] = m[1][0] + matr.m[1][0];
	r.m[1][1] = m[1][1] + matr.m[1][1];
	r.m[1][2] = m[1][2] + matr.m[1][2];
	r.m[1][3] = m[1][3] + matr.m[1][3];
	r.m[2][0] = m[2][0] + matr.m[2][0];
	r.m[2][1] = m[2][1] + matr.m[2][1];
	r.m[2][2] = m[2][2] + matr.m[2][2];
	r.m[2][3] = m[2][3] + matr.m[2][3];
	r.m[3][0] = m[3][0] + matr.m[3][0];
	r.m[3][1] = m[3][1] + matr.m[3][1];
	r.m[3][2] = m[3][2] + matr.m[3][2];
	r.m[3][3] = m[3][3] + matr.m[3][3];
	return r;
}

Matrix4d Matrix4d::operator - (const Matrix4d &matr) const
{
	Matrix4d r;
	r.m[0][0] = m[0][0] - matr.m[0][0];
	r.m[0][1] = m[0][1] - matr.m[0][1];
	r.m[0][2] = m[0][2] - matr.m[0][2];
	r.m[0][3] = m[0][3] - matr.m[0][3];
	r.m[1][0] = m[1][0] - matr.m[1][0];
	r.m[1][1] = m[1][1] - matr.m[1][1];
	r.m[1][2] = m[1][2] - matr.m[1][2];
	r.m[1][3] = m[1][3] - matr.m[1][3];
	r.m[2][0] = m[2][0] - matr.m[2][0];
	r.m[2][1] = m[2][1] - matr.m[2][1];
	r.m[2][2] = m[2][2] - matr.m[2][2];
	r.m[2][3] = m[2][3] - matr.m[2][3];
	r.m[3][0] = m[3][0] - matr.m[3][0];
	r.m[3][1] = m[3][1] - matr.m[3][1];
	r.m[3][2] = m[3][2] - matr.m[3][2];
	r.m[3][3] = m[3][3] - matr.m[3][3];
	return r;
}

bool Matrix4d::operator == (const Matrix4d &matr) const
{
	if(
		 m[0][0] != matr.m[0][0] || m[0][1] != matr.m[0][1] ||
		 m[0][2] != matr.m[0][2] || m[0][3] != matr.m[0][3] ||
		 m[1][0] != matr.m[1][0] || m[1][1] != matr.m[1][1] ||
		 m[1][2] != matr.m[1][2] || m[1][3] != matr.m[1][3] ||
		 m[2][0] != matr.m[2][0] || m[2][1] != matr.m[2][1] ||
		 m[2][2] != matr.m[2][2] || m[2][3] != matr.m[2][3] ||
		 m[3][0] != matr.m[3][0] || m[3][1] != matr.m[3][1] ||
		 m[3][2] != matr.m[3][2] || m[3][3] != matr.m[3][3] ) {
		return false;
	}
	return true;
}

bool Matrix4d::operator != (const Matrix4d &matr) const
{
	if(
		 m[0][0] != matr.m[0][0] || m[0][1] != matr.m[0][1] ||
		 m[0][2] != matr.m[0][2] || m[0][3] != matr.m[0][3] ||
		 m[1][0] != matr.m[1][0] || m[1][1] != matr.m[1][1] ||
		 m[1][2] != matr.m[1][2] || m[1][3] != matr.m[1][3] ||
		 m[2][0] != matr.m[2][0] || m[2][1] != matr.m[2][1] ||
		 m[2][2] != matr.m[2][2] || m[2][3] != matr.m[2][3] ||
		 m[3][0] != matr.m[3][0] || m[3][1] != matr.m[3][1] ||
		 m[3][2] != matr.m[3][2] || m[3][3] != matr.m[3][3]) {
		return true;
	}
	return false;
}

void Matrix4d::operator = (const Matrix3d &matr)
{
	m[0][0] = matr.m[0][0];
	m[0][1] = matr.m[0][1];
	m[0][2] = matr.m[0][2];
	m[1][0] = matr.m[1][0];
	m[1][1] = matr.m[1][1];
	m[1][2] = matr.m[1][2];
	m[2][0] = matr.m[2][0];
	m[2][1] = matr.m[2][1];
	m[2][2] = matr.m[2][2];
}

Matrix4d Matrix4d::transpose(void) const
{
	return Matrix4d(
			m[0][0], m[1][0], m[2][0], m[3][0],
			m[0][1], m[1][1], m[2][1], m[3][1],
			m[0][2], m[1][2], m[2][2], m[3][2],
			m[0][3], m[1][3], m[2][3], m[3][3]
		);
}

void Matrix4d::setTrans(const Vector3d &vec)
{
	m[3][0] = vec.x;
	m[3][1] = vec.y;
	m[3][2] = vec.z;
}

Vector3d Matrix4d::getTrans() const
{
	return Vector3d(m[3][0], m[3][1], m[3][2]);
}

void Matrix4d::makeTrans(const Vector3d &vec)
{
	m[0][0] = 1.0;
	m[0][1] = 0.0;
	m[0][2] = 0.0;
	m[0][3] = 0.0;
	m[1][0] = 0.0;
	m[1][1] = 1.0;
	m[1][2] = 0.0;
	m[1][3] = 0.0;
	m[2][0] = 0.0;
	m[2][1] = 0.0;
	m[2][2] = 1.0;
	m[2][3] = 0.0;
	m[3][0] = vec.x;
	m[3][1] = vec.y;
	m[3][2] = vec.z;
	m[3][3] = 1.0;
}

void Matrix4d::makeTrans(real x, real y, real z)
{
	m[0][0] = 1.0;
	m[0][1] = 0.0;
	m[0][2] = 0.0;
	m[0][3] = 0.0;
	m[1][0] = 0.0;
	m[1][1] = 1.0;
	m[1][2] = 0.0;
	m[1][3] = 0.0;
	m[2][0] = 0.0;
	m[2][1] = 0.0;
	m[2][2] = 1.0;
	m[2][3] = 0.0;
	m[3][0] =  x;
	m[3][1] =  y;
	m[3][2] =  z;
	m[3][3] = 1.0;
}



void Matrix4d::setScale(const Vector3d &vec)
{
	m[0][0] = vec.x;
	m[1][1] = vec.y;
	m[2][2] = vec.z;
}

void Matrix4d::extract3x3Matrix(Matrix3d &m3x3) const
{
	m3x3.m[0][0] = m[0][0];
	m3x3.m[0][1] = m[0][1];
	m3x3.m[0][2] = m[0][2];
	m3x3.m[1][0] = m[1][0];
	m3x3.m[1][1] = m[1][1];
	m3x3.m[1][2] = m[1][2];
	m3x3.m[2][0] = m[2][0];
	m3x3.m[2][1] = m[2][1];
	m3x3.m[2][2] = m[2][2];
}

bool Matrix4d::hasScale() const
{
	// check magnitude of column vectors (==local axes)
	real t = m[0][0] * m[0][0] + m[1][0] * m[1][0] + m[2][0] * m[2][0];
	if (!Math::RealEqual(t, 1.0, (real)1e-04)) {
		return true;
	}
	t = m[0][1] * m[0][1] + m[1][1] * m[1][1] + m[2][1] * m[2][1];
	if (!Math::RealEqual(t, 1.0, (real)1e-04)) {
		return true;
	}
	t = m[0][2] * m[0][2] + m[1][2] * m[1][2] + m[2][2] * m[2][2];
	if (!Math::RealEqual(t, 1.0, (real)1e-04)) {
		return true;
	}
	return false;
}

Quaternion Matrix4d::extractQuaternion() const
{
	Matrix3d matr;
	extract3x3Matrix(matr);
	return Quaternion(matr);
}

Matrix4d Matrix4d::operator*(real scalar) const
{
	return Matrix4d(
			scalar * m[0][0], scalar * m[0][1], scalar * m[0][2], scalar * m[0][3],
			scalar * m[1][0], scalar * m[1][1], scalar * m[1][2], scalar * m[1][3],
			scalar * m[2][0], scalar * m[2][1], scalar * m[2][2], scalar * m[2][3],
			scalar * m[3][0], scalar * m[3][1], scalar * m[3][2], scalar * m[3][3]
		);
}

}

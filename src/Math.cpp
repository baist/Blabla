#include "Math.hpp"
#include <cmath>
#include "Vector2d.hpp"
#include "Vector3d.hpp"
#include "Vector4d.hpp"
#include "Matrix4d.hpp"
#include "Ray.hpp"
#include "Plane.hpp"

namespace bge
{

const real Math::POS_INFINITY = std::numeric_limits<real>::infinity();
const real Math::NEG_INFINITY = -std::numeric_limits<real>::infinity();
const real Math::PI = real(4.0 * atan(1.0));
const real Math::TWO_PI = real(2.0 * PI);
const real Math::HALF_PI = real(0.5 * PI);
const real Math::LOG2 = log(real(2.0));

size_t Math::staticTrigTableSize;
real  Math::staticTrigTableFactor;
real *Math::p_staticSinTable = NULL;
real *Math::p_staticTanTable = NULL;

int Math::AbsI (int val)
{
	return (val >= 0 ? val : -val);
}

int Math::CeilI (float val)
{
	return static_cast<int>(ceil(val));
}

int Math::FloorI (float val)
{
	return static_cast<int>(floor(val));
}

int Math::SignI (int val)
{
	return (val > 0 ? +1 : (val < 0 ? -1 : 0));
}

real Math::Abs (real val)
{
	return real(fabs(val));
}

template <typename T>
T Math::Clamp(T val, T minval, T maxval)
{
	// TOOD: handle error minval>maxval
	return std::max(std::min(val, maxval), minval);
}

Math::Math(unsigned int trigTableSize)
{
	staticTrigTableSize = trigTableSize;
	staticTrigTableFactor = staticTrigTableSize / Math::TWO_PI;
	p_staticSinTable = new real[staticTrigTableSize];
	p_staticTanTable = new real[staticTrigTableSize];
	buildTrigTables();
}


Math::~Math()
{
	delete p_staticSinTable;
	delete p_staticTanTable;
}


void Math::buildTrigTables()
{
	// Build trig lookup tables
	// Could get away with building only PI sized Sin table but simpler this
	// way. Who cares, it'll ony use an extra 8k of memory anyway and I like
	// simplicity.
	real angle;
	for (int i = 0; i < staticTrigTableSize; ++i) {
		angle = Math::TWO_PI * i / staticTrigTableSize;
		p_staticSinTable[i] = sin(angle);
		p_staticTanTable[i] = tan(angle);
	}
}

real Math::SinTable (real val)
{
	// Convert range to index values, wrap if required
	int indx;
	if (val >= 0) {
		indx = int(val * staticTrigTableFactor) % staticTrigTableSize;
	}
	else {
		indx = staticTrigTableSize -
				(int(-val * staticTrigTableFactor) % staticTrigTableSize) - 1;
	}
	return p_staticSinTable[indx];
}

real Math::TanTable (real val)
{
	// Convert range to index values, wrap if required
	int indx = int(val *= staticTrigTableFactor) % staticTrigTableSize;
	return p_staticTanTable[indx];
}

real Math::ACos (real val)
{
	if (-1.0 < val) {
		if (val < 1.0) {
			return real(acos(val));
		}
		else {
			return real(0.0);
		}
	}
	else {
		return real(PI);
	}
}

real Math::ASin (real val)
{
	if (-1.0 < val) {
		if (val < 1.0) {
			return real(asin(val));
		}
		else {
			return real(HALF_PI);
		}
	}
	else {
		return real(-HALF_PI);
	}
}

real Math::ATan (real val)
{
	return real(atan(val));
}

real Math::ATan2 (real y, real x)
{
	return real(atan2(y, x));
}

real Math::Ceil (real val)
{
	return real(ceil(val));
}

// Floor function (Floor(1.9) = 1)
real Math::Floor (real val)
{
	return real(floor(val));
}

bool Math::isNaN(real f)
{
	// std::isnan() is C99, not supported by all compilers
	// However NaN always fails this next test, no other number does.
	return f != f;
}

real Math::Cos (real val, bool useTables)
{
	return (!useTables) ? real(cos(val)) : SinTable(val + HALF_PI);
}

real Math::Exp (real val)
{
	return real(exp(val));
}

real Math::Log (real val)
{
	return real(log(val));
}

real Math::Log2 (real val)
{
	return real(log(val) / LOG2);
}

real Math::LogN (real base, real val)
{
	return real(log(val) / log(base));
}

real Math::Pow (real fBase, real fExponent)
{
	return real(pow(fBase, fExponent));
}

real Math::Sign (real val)
{
	if (val > 0.0) {
		return 1.0;
	}
	if (val < 0.0) {
		return -1.0;
	}
	return 0.0;
}

real Math::Sin (real val, bool useTables)
{
	return (!useTables) ? real(sin(val)) : SinTable(val);
}

real Math::Sqr (real val)
{
	return val * val;
}

// Square root function.
real Math::Sqrt (real val)
{
	return real(sqrt(val));
}

real Math::InvSqrt(real val)
{
	return 1.0f / sqrt(val);
}

real Math::Tan(real val, bool useTables)
{
	return (!useTables) ? real(tan(val)) : TanTable(val);
}

bool Math::pointInTri2D(const Vector2d &p, const Vector2d &a,
												const Vector2d &b, const Vector2d &c)
{
	// Winding must be consistent from all edges for point to be inside
	Vector2d v1, v2;
	real dot[3];
	bool zeroDot[3];
	v1 = b - a;
	v2 = p - a;
	// Note we don't care about normalisation here since sign is all we need
	// It means we don't have to worry about magnitude of cross products either
	dot[0] = v1.crossProduct(v2);
	zeroDot[0] = Math::RealEqual(dot[0], 0.0f, 1e-3);
	v1 = c - b;
	v2 = p - b;
	dot[1] = v1.crossProduct(v2);
	zeroDot[1] = Math::RealEqual(dot[1], 0.0f, 1e-3);
	// Compare signs (ignore colinear / coincident points)
	if(!zeroDot[0] && !zeroDot[1]
	   && Math::Sign(dot[0]) != Math::Sign(dot[1])) {
		return false;
	}
	v1 = a - c;
	v2 = p - c;
	dot[2] = v1.crossProduct(v2);
	zeroDot[2] = Math::RealEqual(dot[2], 0.0f, 1e-3);
	// Compare signs (ignore colinear / coincident points)
	if((!zeroDot[0] && !zeroDot[2]
	    && Math::Sign(dot[0]) != Math::Sign(dot[2])) ||
	   (!zeroDot[1] && !zeroDot[2]
	    && Math::Sign(dot[1]) != Math::Sign(dot[2]))) {
		return false;
	}
	return true;
}

bool Math::pointInTri3D(const Vector3d &p, const Vector3d &a,
												const Vector3d &b, const Vector3d &c,
												const Vector3d &normal)
{
	// Winding must be consistent from all edges for point to be inside
	Vector3d v1, v2;
	real dot[3];
	bool zeroDot[3];
	v1 = b - a;
	v2 = p - a;
	// Note we don't care about normalisation here since sign is all we need
	// It means we don't have to worry about magnitude of cross products either
	dot[0] = v1.crossProduct(v2).dotProduct(normal);
	zeroDot[0] = Math::RealEqual(dot[0], 0.0f, 1e-3);
	v1 = c - b;
	v2 = p - b;
	dot[1] = v1.crossProduct(v2).dotProduct(normal);
	zeroDot[1] = Math::RealEqual(dot[1], 0.0f, 1e-3);
	// Compare signs (ignore colinear / coincident points)
	if(!zeroDot[0] && !zeroDot[1]
	   && Math::Sign(dot[0]) != Math::Sign(dot[1])) {
		return false;
	}
	v1 = a - c;
	v2 = p - c;
	dot[2] = v1.crossProduct(v2).dotProduct(normal);
	zeroDot[2] = Math::RealEqual(dot[2], 0.0f, 1e-3);
	// Compare signs (ignore colinear / coincident points)
	if((!zeroDot[0] && !zeroDot[2]
	    && Math::Sign(dot[0]) != Math::Sign(dot[2])) ||
	   (!zeroDot[1] && !zeroDot[2]
	    && Math::Sign(dot[1]) != Math::Sign(dot[2]))) {
		return false;
	}
	return true;
}

bool Math::RealEqual(real a, real b, real tolerance)
{
	if (fabs(b - a) <= tolerance) {
		return true;
	}
	else {
		return false;
	}
}


std::pair<bool, real> Math::Intersects(const Ray &ray, const Plane &plane)
{
	real denom = plane.normal.dotProduct(ray.getDirection());
	if (Math::Abs(denom) < std::numeric_limits<real>::epsilon()) {
		// Parallel
		return std::pair<bool, real>(false, 0);
	}
	else {
		real nom = plane.normal.dotProduct(ray.getOrigin()) + plane.d;
		real t = -(nom / denom);
		return std::pair<bool, real>(t >= 0, t);
	}
}

std::pair<bool, real> Math::Intersects(const Ray &ray,
																			 const vector<Plane>::type &planes,
																			 bool normalIsOutside)
{
	list<Plane>::type planesList;
	for (vector<Plane>::type::const_iterator i = planes.begin();
			 i != planes.end(); ++i) {
		planesList.push_back(*i);
	}
	return Intersects(ray, planesList, normalIsOutside);
}

std::pair<bool, real> Math::Intersects(const Ray &ray,
																			 const list<Plane>::type &planes,
																			 bool normalIsOutside)
{
	list<Plane>::type::const_iterator planeit, planeitend;
	planeitend = planes.end();
	bool allInside = true;
	std::pair<bool, real> ret;
	std::pair<bool, real> end;
	ret.first = false;
	ret.second = 0.0f;
	end.first = false;
	end.second = 0;
	// derive side
	// NB we don't pass directly since that would require Plane::Side in
	// interface, which results in recursive includes since Math is so fundamental
	Plane::Side outside =
			normalIsOutside ? Plane::POSITIVE_SIDE : Plane::NEGATIVE_SIDE;
	for (planeit = planes.begin(); planeit != planeitend; ++planeit) {
		const Plane &plane = *planeit;
		// is origin outside?
		if (plane.getSide(ray.getOrigin()) == outside) {
			allInside = false;
			// Test single plane
			std::pair<bool, real> planeRes =
					ray.intersects(plane);
			if (planeRes.first) {
				// Ok, we intersected
				ret.first = true;
				// Use the most distant result since convex volume
				ret.second = std::max(ret.second, planeRes.second);
			}
			else {
				ret.first = false;
				ret.second = 0.0f;
				return ret;
			}
		}
		else {
			std::pair<bool, real> planeRes =
					ray.intersects(plane);
			if (planeRes.first) {
				if(!end.first) {
					end.first = true;
					end.second = planeRes.second;
				}
				else {
					end.second = std::min(planeRes.second, end.second);
				}
			}
		}
	}
	if (allInside) {
		// Intersecting at 0 distance since inside the volume!
		ret.first = true;
		ret.second = 0.0f;
		return ret;
	}
	if(end.first) {
		if(end.second < ret.second) {
			ret.first = false;
			return ret;
		}
	}
	return ret;
}

std::pair<bool, real> Math::Intersects(const Ray &ray, const Vector3d &a,
																			 const Vector3d &b, const Vector3d &c,
																			 const Vector3d &normal,
                                       bool positiveSide, bool negativeSide)
{
	//
	// Calculate intersection with plane.
	//
	real t;
	{
		real denom = normal.dotProduct(ray.getDirection());
		// Check intersect side
		if (denom > + std::numeric_limits<real>::epsilon()) {
			if (!negativeSide) {
				return std::pair<bool, real>(false, 0);
			}
		}
		else if (denom < - std::numeric_limits<real>::epsilon()) {
			if (!positiveSide) {
				return std::pair<bool, real>(false, 0);
			}
		}
		else {
			// Parallel or triangle area is close to zero when
			// the plane normal not normalised.
			return std::pair<bool, real>(false, 0);
		}
		t = normal.dotProduct(a - ray.getOrigin()) / denom;
		if (t < 0) {
			// Intersection is behind origin
			return std::pair<bool, real>(false, 0);
		}
	}
	//
	// Calculate the largest area projection plane in X, Y or Z.
	//
	size_t i0, i1;
	{
		real n0 = Math::Abs(normal[0]);
		real n1 = Math::Abs(normal[1]);
		real n2 = Math::Abs(normal[2]);
		i0 = 1;
		i1 = 2;
		if (n1 > n2) {
			if (n1 > n0) {
				i0 = 0;
			}
		}
		else {
			if (n2 > n0) {
				i1 = 0;
			}
		}
	}
	//
	// Check the intersection point is inside the triangle.
	//
	{
		real u1 = b[i0] - a[i0];
		real v1 = b[i1] - a[i1];
		real u2 = c[i0] - a[i0];
		real v2 = c[i1] - a[i1];
		real u0 = t * ray.getDirection()[i0] + ray.getOrigin()[i0] - a[i0];
		real v0 = t * ray.getDirection()[i1] + ray.getOrigin()[i1] - a[i1];
		real alpha = u0 * v2 - u2 * v0;
		real beta  = u1 * v0 - u0 * v1;
		real area  = u1 * v2 - u2 * v1;
		// epsilon to avoid float precision error
		const real EPSILON = 1e-6f;
		real tolerance = - EPSILON * area;
		if (area > 0) {
			if (alpha < tolerance || beta < tolerance ||
					alpha + beta > area - tolerance) {
				return std::pair<bool, real>(false, 0);
			}
		}
		else {
			if (alpha > tolerance || beta > tolerance ||
					alpha + beta < area - tolerance) {
				return std::pair<bool, real>(false, 0);
			}
		}
	}
	return std::pair<bool, real>(true, t);
}

std::pair<bool, real> Math::Intersects(const Ray &ray, const Vector3d &a,
																			 const Vector3d &b, const Vector3d &c,
                                       bool positiveSide, bool negativeSide)
{
	Vector3d normal = calcBasicFaceNormalWithoutNormalize(a, b, c);
	return Intersects(ray, a, b, c, normal, positiveSide, negativeSide);
}

Vector4d Math::calcFaceNormal(const Vector3d &v1, const Vector3d &v2,
																	const Vector3d &v3)
{
	Vector3d normal = calcBasicFaceNormal(v1, v2, v3);
	// Now set up the w (distance of tri from origin
	return Vector4d(normal.x, normal.y, normal.z, -(normal.dotProduct(v1)));
}

Vector3d Math::calcBasicFaceNormal(const Vector3d &v1, const Vector3d &v2,
																			 const Vector3d &v3)
{
	Vector3d normal = (v2 - v1).crossProduct(v3 - v1);
	normal.normalise();
	return normal;
}

Vector4d Math::calcFaceNormalWithoutNormalize(const Vector3d &v1,
																									const Vector3d &v2,
																									const Vector3d &v3)
{
	Vector3d normal = calcBasicFaceNormalWithoutNormalize(v1, v2, v3);
	// Now set up the w (distance of tri from origin)
	return Vector4d(normal.x, normal.y, normal.z, -(normal.dotProduct(v1)));
}

Vector3d Math::calcBasicFaceNormalWithoutNormalize(const Vector3d &v1,
																											 const Vector3d &v2,
																											 const Vector3d &v3)
{
	Vector3d normal = (v2 - v1).crossProduct(v3 - v1);
	return normal;
}

Vector3d Math::calcBernstein(const Vector3d * p_points, float t)
{
	// the t value inverted
	const float it = 1.0f - t;
	// calculate blending functions
	const float a = Pow(t, 3);
	const float b = 3 * Pow(t, 2) * it;
	const float c = 3 * Pow(it, 2) * t;
	const float d = Pow(it, 3);
	// sum the effects of the Points and their respective blending functions
	return p_points[0] * a + p_points[1] * b + p_points[2] * c + p_points[3] * d;
}

Vector3d Math::calcBernstein(const vector<Vector3d>::type & points, float t)
{
	// the t value inverted
	const float it = 1.0f - t;
	// calculate blending functions
	const float a = Pow(t, 3);
	const float b = 3 * Pow(t, 2) * it;
	const float c = 3 * Pow(it, 2) * t;
	const float d = Pow(it, 3);
	// sum the effects of the Points and their respective blending functions
	return points[0] * a + points[1] * b + points[2] * c + points[3] * d;
}

Vector3d Math::calcDerivBernstein(const Vector3d * p_points, float t)
{
	const float a = Math::Pow(t, 2);
	const float b = 2.0f * t - 3.0f * Math::Pow(t, 2);
	const float c = 1.0f - 4.0f * t + 3.0f * Math::Pow(t, 2);
	const float d = -1.0f + 2 * t - Math::Pow(t, 2);
	return p_points[0] * a + p_points[1] * b + p_points[2] * c + p_points[3] * d;
}

Vector3d Math::calcDerivBernstein(const vector<Vector3d>::type & points,
																			 float t)
{
	const float a = Math::Pow(t, 2);
	const float b = 2.0f * t - 3.0f * Math::Pow(t, 2);
	const float c = 1.0f - 4.0f * t + 3.0f * Math::Pow(t, 2);
	const float d = -1.0f + 2 * t - Math::Pow(t, 2);
	return points[0] * a + points[1] * b + points[2] * c + points[3] * d;
}

}

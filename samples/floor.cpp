#include <cstring>
#include "Buffer.hpp"
#include "Vector2d.hpp"
#include "Vector3d.hpp"
#include "Matrix4d.hpp"
#include "Quaternion.hpp"
#include "Texture.hpp"
#include "Render.hpp"
#include "TextureManager.hpp"
#include "ViewFrustum.hpp"
#include "VertexDescr.hpp"

#include "util.hpp"

class Sample0: public Sample
{
public:
protected:
	void GenerateObject(bge::rndr::VertexBuffer **p_vertBuffer,
											bge::rndr::IndexBuffer **p_indBuffer);
	virtual void onCreate();
	virtual void onDestroy();
	virtual void onRun(unsigned int elapsedTicks);
private:
	bge::rndr::VertexBuffer *m_p_vertBuffer;
	bge::rndr::IndexBuffer *m_p_indBuffer;
	bge::rndr::Texture *m_p_texture;
	bge::rndr::Render m_render;
	bge::real m_rad;
};

void Sample0::GenerateObject(bge::rndr::VertexBuffer **p_vertBuffer,
														 bge::rndr::IndexBuffer **p_indBuffer)
{
	using namespace bge;

	struct Vertex
	{
		Vector3d p;
		unsigned int c;
		Vector2d t;
	}
	exampleVertices[] = {
		Vector3d(-2.0f, 0.0f,  2.0f), 0x00FFFFFF, Vector2d(0.0f, 0.0f),
		Vector3d( 2.0f, 0.0f,  2.0f), 0x00FFFFFF, Vector2d(1.0f, 0.0f),
		Vector3d(-2.0f, 0.0f, -2.0f), 0x00FFFFFF, Vector2d(0.0f, 1.0f),
		Vector3d( 2.0f, 0.0f, -2.0f), 0x00FFFFFF, Vector2d(1.0f, 1.0f)
	};

	unsigned int exampleIndices[] = {
		0, 1, 2,
		1, 3, 2
	};
	rndr::VertexDescr vertDescr;
	vertDescr.add(rndr::VertexDescr::VT_UNKNOWN, DF_R32R32R32);
	vertDescr.add(rndr::VertexDescr::VT_UNKNOWN, DF_I8I8I8I8);
	vertDescr.add(rndr::VertexDescr::VT_UNKNOWN, DF_R32R32);
	*p_vertBuffer = new rndr::VertexBuffer(vertDescr, 4);
	*p_indBuffer = new rndr::IndexBuffer(sizeof(unsigned int), 6);
	void *p_temp;
	(*p_vertBuffer)->map(&p_temp);
	std::memcpy(p_temp, exampleVertices, sizeof(exampleVertices));
	(*p_vertBuffer)->unmap();
	(*p_indBuffer)->map(&p_temp);
	std::memcpy(p_temp, exampleIndices, sizeof(exampleIndices));
	(*p_indBuffer)->unmap();
}

void Sample0::onCreate()
{
	using namespace bge;
	using namespace rndr;
	ViewFrustum viewFrustum;
	GenerateObject(&m_p_vertBuffer, &m_p_indBuffer);
	m_p_texture = TextureManager::getInst().
			getTexture("/home/stas/My/3d-engine/leaves.tga");
	m_render.setDefaultColor(0x00FFFF00);
	m_render.setCullMode(CM_NONE);
	m_render.setFillMode(FM_SOLID);
	m_render.setProjMatrix(viewFrustum.getProjectionMatrix());
}

void Sample0::onDestroy()
{
	delete m_p_indBuffer;
	delete m_p_vertBuffer;
}

void Sample0::onRun(unsigned int elapsedTicks)
{
	using namespace bge;
	using namespace rndr;
	Matrix4d matr = Matrix4d::getTrans(0.0f, -1.0f, 4.0f);
	m_render.begin();
	m_render.setWorldMatrix(&matr);
	m_render.setVertexBuffer(m_p_vertBuffer);
	m_render.setIndexBuffer(m_p_indBuffer);
	m_render.setTexture(m_p_texture);
	m_render.drawTriMesh(2);
	m_render.end();
}

int main()
{
	Sample0 sample0;
	sample0.run();

	return 0;
}

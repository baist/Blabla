
#include <cstring>
#include <algorithm>
#include "Line2d.hpp"
#include "Vector3d.hpp"
#include "Vector4d.hpp"
#include "Matrix4d.hpp"
#include "Math.hpp"
#include "ColourValue.hpp"
#include "Render.hpp"
#include "ViewFrustum.hpp"

#include "util.hpp"

class Sample2: public Sample
{
public:
protected:
	void GenerateObject(bge::rndr::VertexBuffer ** p_vertBuffer,
											bge::rndr::IndexBuffer ** p_indBuffer);
	virtual void onCreate();
	virtual void onDestroy();
	virtual void onRun(unsigned int elapsedTicks);
private:
	bge::rndr::VertexBuffer *m_p_vertBuffer;
	bge::rndr::IndexBuffer *m_p_indBuffer;
	bge::rndr::Render m_render;
	bge::real m_rad;
};

void Sample2::GenerateObject(bge::rndr::VertexBuffer **p_vertBuffer,
														 bge::rndr::IndexBuffer **p_indBuffer)
{
	using namespace bge;
	struct Vertex
	{
		Vector3d p;
		unsigned int c;
	} exampleVertices[] = {
		Vector3d(-1.2f, -0.5f, 0.0f), 0x000000FF,
		Vector3d(-2.5f, -0.5f, 0.0f), 0x000000FF,
		Vector3d(-2.0f, 0.5f, 0.0f),  0x000000FF,

		Vector3d( 2.0f, 0.5f, 0.0f),  0x000000FF,
		Vector3d( 2.2f, -0.5f, 0.0f), 0x000000FF,
		Vector3d( 1.0f, 0.7f, 0.0f),  0x000000FF,

		Vector3d( 0.0f, -0.5f, 0.0f), 0x00FF0000,
		Vector3d(-1.0f, -0.5f, 0.0f), 0x00FF0000,
		Vector3d(-1.0f, 0.5f, 0.0f),  0x00FF0000,

		Vector3d( 0.0f, -2.5f, 0.0f), 0x00FFFF00,
		Vector3d(-1.0f, -2.0f, 0.0f), 0x00FFFF00,
		Vector3d(-1.0f, -1.0f, 0.0f), 0x00FFFF00,

		Vector3d(-0.5f, 0.5f, 0.0f),  0x00FF00FF,
		Vector3d( 0.5f, 0.5f, 0.0f),  0x00FF00FF,
		Vector3d( 0.5f, -0.5f, 0.0f), 0x00FF00FF,

		Vector3d( 1.0f, -2.0f, 0.0f), 0x0000FF00,
		Vector3d( 0.0f, -2.0f, 0.0f), 0x0000FF00,
		Vector3d( 0.5f, -1.0f, 0.0f), 0x0000FF00,

		Vector3d( 0.0f, 1.0f, 0.0f),  0x0000FFFF,
		Vector3d(-1.0f, 1.2f, 0.0f),  0x0000FFFF,
		Vector3d(-1.2f, 1.7f, 0.0f),  0x0000FFFF,

		Vector3d( 1.2f, 1.4f, 0.0f),  0x00FFFFFF,
		Vector3d( 0.2f, 1.0f, 0.0f),  0x00FFFFFF,
		Vector3d( 0.8f, 1.7f, 0.0f),  0x00FFFFFF
	};

	int exampleIndices[] = {
		 0,  1,  2,
		 3,  4,  5,
		 6,  7,  8,
		 9, 10, 11,
		12, 13, 14,
		15, 16, 17,
		18, 19, 20,
		21, 22, 23,
	};
	rndr::VertexDescr vertDescr;
	vertDescr.add(rndr::VertexDescr::VT_UNKNOWN, DF_R32R32R32);
	vertDescr.add(rndr::VertexDescr::VT_UNKNOWN, DF_I8I8I8I8);
	*p_vertBuffer = new rndr::VertexBuffer(vertDescr, 24);
	*p_indBuffer = new rndr::IndexBuffer(sizeof(unsigned int), 24);
	void *p_temp;
	(*p_vertBuffer)->map(&p_temp);
	std::memcpy(p_temp, exampleVertices, sizeof(exampleVertices));
	(*p_vertBuffer)->unmap();
	(*p_indBuffer)->map(&p_temp);
	std::memcpy(p_temp, exampleIndices, sizeof(exampleIndices));
	(*p_indBuffer)->unmap();
}

void Sample2::onCreate()
{
	using namespace bge;
	using namespace rndr;
	const int WIDTH = 640;
	const int HEIGHT = 480;
	ViewFrustum viewFrustum;
	GenerateObject(&m_p_vertBuffer, &m_p_indBuffer);
	m_render.setDefaultColor(0x00FFFF00);
	m_render.setCullMode(CM_CW);
	m_render.setFillMode(FM_SOLID);
	m_render.setProjMatrix(viewFrustum.getProjectionMatrix());
}

void Sample2::onDestroy()
{
	delete m_p_indBuffer;
	delete m_p_vertBuffer;
}

void Sample2::onRun(unsigned int elapsedTicks)
{
	using namespace bge;
	using namespace rndr;
	if(m_rad > Math::TWO_PI) {
		m_rad = 0.0f;
	}
	if(m_animation) {
		m_rad += static_cast<real>(elapsedTicks) * (Math::TWO_PI/ 60000.0f);
	}
	Quaternion q(m_rad, Vector3d(0.0f, 0.0f, 1.0f));
	Matrix4d matr = Matrix4d(q) *
			Matrix4d::getTrans(Vector3d(0.0f, 0.0f, 2.6f));
	m_render.begin();
	m_render.setWorldMatrix(&matr);
	m_render.setVertexBuffer(m_p_vertBuffer);
	m_render.setIndexBuffer(m_p_indBuffer);
	m_render.drawTriMesh(8);
	m_render.end();
}

int main()
{
	Sample2 sample2;
	sample2.run();

	return 0;
}

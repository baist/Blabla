#if !defined(UTIL_HPP)
#define UTIL_HPP

#include <sys/time.h>
#include "Prerequisites.hpp"
#include "LogManager.hpp"
#include "MyTextureManager.hpp"
#include "MyMeshManager.hpp"

class Sample
{
public:
	Sample();
	virtual ~Sample();
	void run();
protected:
	bool m_animation;
	bge::LogManager m_logManager;
	bge::rndr::MyTextureManager m_textureManager;
	bge::rndr::MyMeshManager m_meshManager;
protected:
	unsigned int getTicks();
	virtual void onCreate();
	virtual void onDestroy();
	virtual void onRun(unsigned int elapsedTicks);
private:
	struct timeval start;
};

#endif // UTIL_HPP

#include <cstring>
#include <algorithm>
#include "Line2d.hpp"
#include "Vector3d.hpp"
#include "Vector4d.hpp"
#include "Matrix4d.hpp"
#include "Math.hpp"
#include "ColourValue.hpp"
#include "Render.hpp"
#include "TextureManager.hpp"
#include "ViewFrustum.hpp"

#include "util.hpp"

class Sample1: public Sample
{
public:
protected:
	virtual void onCreate();
	virtual void onDestroy();
	virtual void onRun(unsigned int elapsedTicks);
private:
	bge::rndr::Mesh *m_p_mesh;
	bge::rndr::Texture *m_p_texture;
	bge::rndr::Render m_render;
	bge::real m_rad;
};

void Sample1::onCreate()
{
	using namespace bge;
	using namespace rndr;
	const int WIDTH = 640;
	const int HEIGHT = 480;
	ViewFrustum viewFrustum;
	m_p_mesh = MeshManager::getInst().getTeapot();
	m_p_texture = TextureManager::getInst().getLena();
	m_render.setDefaultColor(0x00FFFF00);
	m_render.setCullMode(CM_CW);
	m_render.setFillMode(FM_SOLID);
	m_render.setProjMatrix(viewFrustum.getProjectionMatrix());
}

void Sample1::onDestroy()
{
}

void Sample1::onRun(unsigned int elapsedTicks)
{
	using namespace bge;
	using namespace rndr;
	if(m_rad > Math::TWO_PI) {
		m_rad = 0.0f;
	}
	if(m_animation) {
		m_rad += static_cast<real>(elapsedTicks) * (Math::TWO_PI/ 6000.0f);
	}
	Quaternion q(m_rad, Vector3d(0.0f, 1.0f, 0.0f));
	Matrix4d matr = Matrix4d(q) *
			Matrix4d::getTrans(Vector3d(0.0f, -2.0f, 5.0f));
	m_render.begin();
	m_render.setWorldMatrix(&matr);
	m_render.setVertexBuffer(m_p_mesh->getVertexBuffer());
	m_render.setIndexBuffer(m_p_mesh->getIndexBuffer());
	m_render.setTexture(m_p_texture);
	m_render.drawTriMesh(m_p_mesh->getNumPrim());
	m_render.end();
}

int main()
{
	Sample1 sample1;
	sample1.run();

	return 0;
}

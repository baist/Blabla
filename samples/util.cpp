#include <SDL2/SDL.h>
#include "LogManager.hpp"
#include "util.hpp"

Sample::Sample():
	m_animation(true)
{
	using namespace bge;
	m_animation = true;
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0) {
		LogManager::LogError() << "Unable to initialize SDL: " <<
															SDL_GetError();
	}
	atexit(SDL_Quit);
}

Sample::~Sample()
{

}

void Sample::run()
{
	using namespace bge;
	onCreate();
	unsigned int ticks = getTicks();
	unsigned int fpsTicks = ticks;
	int numFrames = 0;
	bool infinite = true;
	while(infinite) {
		SDL_PumpEvents();
		Uint32 currentTicks = getTicks();
		onRun(currentTicks - ticks);
		numFrames += 1;
		if((currentTicks - fpsTicks) >= 1000) {
			LogManager::LogInfo() << "FPS: " << numFrames;
			//std::cout << ResourceManager::generateNewName("mesh.obj") << std::endl;
			numFrames = 0;
			fpsTicks = getTicks();
		}
		SDL_Event p_events[4];
		int numEvents = SDL_PeepEvents(p_events, 4, SDL_GETEVENT,
																	 SDL_FIRSTEVENT, SDL_LASTEVENT);
		if(numEvents < 0) {
			break;
		}
		for(unsigned int i = 0; i < numEvents; ++i) {
			SDL_Event &event = p_events[i];
			if(event.type == SDL_QUIT) {
				infinite = false;
			}
			else if(event.type == SDL_KEYUP) {
				m_animation = !m_animation;
			}
		}
		ticks = currentTicks;
	}
	onDestroy();
}

unsigned int Sample::getTicks()
{
	unsigned int ticks;
	struct timeval now;
	gettimeofday(&now, 0);
	ticks = (now.tv_sec - start.tv_sec) * 1000 +
			(now.tv_usec - start.tv_usec) / 1000;
	return ticks;
}

void Sample::onCreate()
{
}

void Sample::onDestroy()
{
}

void Sample::onRun(unsigned int elapsedTicks)
{
}
